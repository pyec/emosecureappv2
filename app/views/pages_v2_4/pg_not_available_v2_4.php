<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_not_available_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 *		Show the Category Details on its own.
  * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-06		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-categories";

?>
<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">


<center>
<div class="col-sm-12">

	
  				<h2>
 				  Under Construction.
                </h2>              
  				<h3>
 				  This page and feature is not currently available.
                </h3>              
                                
</div>
</center>


</div></div></div>
