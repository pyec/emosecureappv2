
<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_display_category_full_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 		This is also meant to show all the users records with Access
 * 		to this Category by CID.
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-04		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back						= "user-sigin";
$link_signin					= "user-signin";
$link_reqnew					= "user-requestnew";
$link_forgot_password			= "user-forgot-password";

$link_display_user				= "display-user";
$link_delete_user_entry			= "delthis-user";
$link_disable_user_entry		= "disable-user";
$link_enable_user_entry			= "enable-user";
$link_add_user_entry			= "add-user";
$link_update_user				= "display-user";
$link_modify_user_rec			= "change-user";

$link_add_doc_to_cat			= "add-document-to-cat";
$link_add_access_to_cat			= "add-access-to-cat";
$link_add_subcat_to_cat			= "add-subcat-to-cat";

$link_display_docs				= "display-document";
$link_delete_docs_entry			= "delthis-document";
$link_disable_docs_entry			= "disable-document";
$link_enable_docs_entry			= "enable-document";
$link_add_docs_entry			= "add-document";
$link_update_docs				= "display-document";
$link_modify_docs_rec			= "change-document";

$link_search					= "category-search";
$link_document					= "show-fulldoc";				


/*................FOR Debug Dump use...........PRSC */

$dbgYN		= FALSE;
if($dbgYN)
	{	
		$vmsg = "DISPLAY CATEGORIES";
		print "===================================----->>>> SO [" . $vmsg . "]  Data DMP)";
		print "<pre>";
		if(!empty($categoryREC))
			print_r($categoryREC);
		else 
			print "NO DATA IN Category REC<br><br>";	
		print "</pre>--------------<<<< EO [" . $vmsg . "]   Data DMP Ends)<br><br>";
		
	}	

/*...........Determine Category Record Key.........*/
		
if(!empty($categoryREC['CID']))
		$CID = $categoryREC['CID']; 	
	
?>


<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">
<!-- SO DETAIL CONTENT ############################################# -->


			<h1>Category Info</h1>

	    <hr>
   
 	<?php 			
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	<?php if($this->session->flashdata('success')): ?>
				<div class="alert alert-success">
					<?= $this->session->flashdata('success') ?>
				</div>
				
				
	<?php elseif($this->session->flashdata('danger')): ?>
	
				<div class="alert alert-error">
					<?= $this->session->flashdata('danger') ?>
				</div>
	<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.
                </div>'.validation_errors() : '' ?>
	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CID">Category Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CID']))
							print $categoryREC['CID'];
						else 
							print "--";	
							?>
					</div>
				</div>

	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CategoryShortName">Category Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['Name']))
							print $categoryREC['Name'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($categoryREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $accessREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>

<?php 
	if($roleREC['viewWhoYN'] == 'Y'):
?>
				
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModBy">Last Mod By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['LastModBy']))
							print $categoryREC['LastModBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModDate">Last Mod Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['LastModDate']))
							print $categoryREC['LastModDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
				
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created By  
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedBy">Created By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CreatedBy']))
							print $categoryREC['CreatedBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedDate">Created Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CreatedDate']))
							print $categoryREC['CreatedDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

<?php endif;											// EO if View Who enabled ?>					


<?php 
	if($roleREC['viewCatsPublicDescYN'] == 'Y'):
?>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="CategoryDesc">Category Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($categoryREC['CategoryDesc']))
					print $categoryREC['CategoryDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>
<?php endif;							// Show Desc YN?>


<?php 
	if($roleREC['viewCatsPrivDescYN'] == 'Y'):
?>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category PRIVATE Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="PrivateCategoryDesc">Category Private Info </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($categoryREC['CategoryPrivDesc']))
					print $categoryREC['CategoryPrivDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

<?php endif;									// EO Show Private Notes YN?>
				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open('show-categories');
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to List
			</button>
	
			</form>
 		 	
		</div>

<!-- EO DETAIL CONTENT ############################################# -->

		</div>
	</div>







<div role="tabpanel">



<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		
			<li role="presentation" >
			<a href="#generaldocs" 
						aria-controls="generaldocs" role="tab" data-toggle="tab">General Documents</a></li>
<?php endif ?>


<?php 
	if($roleREC['viewSubcatsYN'] == 'Y'):
?>

			<li role="presentation"  class="active">
			<a href="#subcats"
						 aria-controls="subcats" role="tab" data-toggle="tab">Sub Categories and Docs</a></li>
<?php endif ?>


<?php 
	if($roleREC['viewAccessYN'] == 'Y'):
?>
			<li role="presentation">
			<a href="#userswithaccess" aria-controls="userswithaccess" role="tab" data-toggle="tab">Users With Access</a></li>
<?php endif ?>
		</ul>



		<!-- Tab panes -->
		<div class="tab-content">
	
			
	
<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

<div role="tabpanel" class="tab-pane" id="generaldocs">


<?php 
/*===================================================================================
 * 	
 * 		SECTION:	Show Documents by Category
 * 		ADDED:		Stephen Chafe
 * 	
 *  This shows the core Documents that are not in Sub-categories
 *  
 * 	Note: This should only be for Admins
 * ==================================================================================
 */

$link_back					= "show-documents";
$recXX						= 0;

?>


<?php 
/*---------------------------------------------------------------
 * 			SHOW NON-SUB Category Documents assoc with category
 * --------------------------------------------------------------
 */
if(!empty($documentsARR)): ?>

<div class="row" style="height:40px;background:rgb(128,192,128);color:white">

	<div class="col-md-12">
		<h4>General Documents List</h4>
	</div>
</div>


	<div class="row">
			<div class="col-md-12">
			<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Document #</th>
				<th nowrap>Document Code</th>
				<th nowrap>Document Name</th>
				<th nowrap>Version</th>
				<th nowrap>View</th>
				
				<th nowrap>Last Mod</th>
				
				
<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
	
	
?>
				<th>Modify</th>
<?php endif ?>	

<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif ?>


<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif ?>


	</thead>
	<tbody class="tbody">
	
		<?php foreach ($documentsARR as $documentREC): ?>
		
		 <?php 
		 
 //		 echo "<pre>############################DOC REC ";   // DBG
 //		 print_r($documentREC);
//		 echo "</pre>";
		 
		 if($documentREC['SCID'] == '') : 
		 
		 /*...........List the Document as it is not in a Sub-Category... - PRSC */
		 ?>
		 
		 
		<tr>

<?php 
	if($roleREC['detailDocsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_update_docs; ?>/<?= $documentREC['DocID'] ?>">
		   	<?= $documentREC['DocID'] ?></a>
		</td>
<?php else: ?>							
		<td>
		   	<?= $documentREC['DocID'] ?>
		</td>
	<?php endif;					// EO Allow Detail or not Cell ?>
		   	

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Code
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocCD']  ?>
		</td>



		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortName']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Version
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['Version']  ?>
		</td>


		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document PDF Show
		 * -----------------------------------------------------PRSC 201603
		 */	?>

<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

	<?php if($documentREC['DocFileName']): ?>
					<?php 
					
//					echo form_open($link_document);
					$attributes = array('target' => '_blank', 'id' => 'myform');

					echo form_open($link_document, $attributes);					
					echo form_hidden('DocFileNM',$documentREC['DocFileName']);
					echo form_hidden('DocID',$documentREC['DocID']);
					echo form_hidden('CID',$documentREC['CID']);
					?>

					 
				<button type="submit" class="btn btn-sm btn-danger" 
							style="height:25px;vertical-align:middle;background:black">
					
			 		PDF
					</button>
				
					<?php 
					print '</form>';
					endif; 
					?>
			<?php endif;			// EO If document defined ?>		
					
		</td>


		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['LastModDate']  ?>
		</td>


<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_modify_docs_rec; ?>/<?= $documentREC['DocID'] ?>">
			   	Modify</a>
		</td>
	<?php endif;					// EO Modify Cell ?>


<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>

	<?php 
		if($documentREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle;background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle;background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							



<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
		<td>
			<form action="<?php echo base_url(); print $link_delete_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
				<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle">
				DEL
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
		</td>
			
	<?php endif;					// EO Del Cell ?>
	
</tr>
		<?php endif;				// EO if Doc has same Subcat ?>

		<?php endforeach;			// EO if for each document in list ?>

			</tbody>
		</table>
	</div>	
</div>		

	<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h5><i>No General Documents Associated with this Category. Found</i></h5>
				<center>
		</div>		
 	
		
<?php	endif;						// EO if There are Documents to Show empty ?>


<?php 
	if($roleREC['AddDocsYN'] == 'Y'):
?>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>
		<?php  print $link_add_doc_to_cat . '/' . $CID ?>" class="btn btn-success pull-right">Add New Document to this Category</a>
	</div>
</div>

<?php endif;					// EO If can Add a General Document or not?>

<br><br><br>
</div>



<?php	endif;						// EO if Allowed to view Docs ?>



<!-- EO PANE 1 ########################################################################3 -->
			
			

<!-- SO PANE 2 ########################################################################3 -->



<?php 
	if($roleREC['viewSubcatsYN'] == 'Y'):
?>
			
<div role="tabpanel" class="tab-pane active" id="subcats">

<?php 
/*===================================================================================
 * 	
 * 		SECTION:	Show Documents by Sub-Category
 * 		ADDED:		Stephen Chafe
 * 	
 *  This block is used to LIST ALL Applicable Users who hae access to this category.
 * 
 * 	Note: This should only be for Admins
 * ==================================================================================
 */
?>


<?php 
/*---------------------------------------------------------------
 * 			NEXT LOOP trhough all Sub-Category Docs associated
 * 			with this Category
 * --------------------------------------------------------------
 */


if(!Empty($subcatsARR)): ?>


    <?php 
		foreach ($subcatsARR as $subcatREC):
		// Show the Headers with the SubCategory	
		?>
	
	<!-- SO Show SubCAT BLOCK -->
		
	<div class="row" style="height:40px;background:rgb(192,192,255);color:white">
		<div class="col-md-12">
			<h4>&nbsp 
			<?php
			// Print the Subcat Header
//			echo '<pre>';						// DBG
//			print_r($subcatREC);
//			echo '</pre>';
			
			if(!empty($subcatREC['SubcatShortName']))
			{
				if($subcatREC['SubcatShortName'])
					print $subcatREC['SubcatShortName'];
			}	
			else 
				print "Unknown Sub-Category";	
			?>
			</h4>
		</div>
	</div>		
		
	<div class="row">
			<div class="col-md-12">
			<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Document #</th>
				<th nowrap>Document Code</th>
				<th nowrap>Document Name</th>
				<th nowrap>Version</th>
				<th nowrap>View</th>
				<th nowrap>Last Mod</th>
				
<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif ?>	

<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif ?>


<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif ?>
			</thead>
			
			
	<tbody class="tbody">
	
		<?php 
		
		$recXX		= 0;
		foreach ($documentsARR as $documentREC): ?>
		
		 <?php
		 $subcatYN	= FALSE;
		 $subcatID	= -1; 
		 if(!empty($documentREC['SCID']))
		 	{
		 		$subcatYN  = TRUE;
		 		$subcatID  = $documentREC['SCID'];
		 	}
		 if($subcatYN == TRUE && $subcatID == $subcatREC['SCID']) : 
		 /*...........List the Document............. - PRSC */
		 ?>

		<tr >


<?php 
	if($roleREC['detailDocsYN'] == 'Y'):
?>
		<td style="height:25px;vertical-align:middle">
		<a href="<?= base_url(); print $link_update_docs; ?>/<?= $documentREC['DocID'] ?>">
			   	<?= $documentREC['DocID'] ?></a>
		</td>
<?php else: ?>
		<td style="height:25px;vertical-align:middle">
			   	<?= $documentREC['DocID'] ?>
		</td>
<?php endif ?>							

		<td style="height:25px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Code
		 * -----------------------------------------------------PRSC 201603
		 */	
			$recXX++;					// Count number of docs shown
		
			?>
				<?= $documentREC['DocCD']  ?>
		</td>



		<td style="height:25px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortName']  ?>
		</td>

		<td style="height:25px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Version
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['Version']  ?>
		</td>


		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document PDF Show
		 * -----------------------------------------------------PRSC 201603
		 */	?>

<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

	<?php if($documentREC['DocFileName']): ?>
					<?php 
					
//					echo form_open($link_document);
					$attributes = array('target' => '_blank', 'id' => 'myform');

					echo form_open($link_document, $attributes);					
					echo form_hidden('DocFileNM',$documentREC['DocFileName']);
					echo form_hidden('DocID',$documentREC['DocID']);
					echo form_hidden('CID',$documentREC['CID']);
					?>

					 
				<button type="submit" class="btn btn-sm btn-danger"  style="height:25px;vertical-align:middle;background:black">
					<i class="icon-ok icon-white"></i>
			 		PDF
					</button>
				
					<?php 
					print '</form>';
					endif; 
					?>
			<?php endif;			// EO If document defined ?>		
					
		</td>


		<td style="height:25px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['LastModDate']  ?>
		</td>

<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>

		<td style="height:25px;vertical-align:middle">
			<a href="<?= base_url(); print $link_modify_docs_rec; ?>/<?= $documentREC['DocID'] ?>">
			   	Modify</a>
		</td>
<?php endif ?>

<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>

	<?php 
		if($documentREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							


<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>

		<form action="<?php echo base_url(); print $link_delete_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
		<td style="height:25px;vertical-align:middle">
				<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle">
				DEL
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
		</td>
		</form>
<?php endif ?>
	</tr>		
		

		<?php endif;				// EO if Doc has same Subcat ?>

		<?php endforeach;			// EO if for each document ?>
		
		
		</tbody>
		</table>
	</div>	
</div>		

		<?php 
		/*------------------------------------------------
		 * 	No Data for this Subcategory Displayed
		 *------------------------------------------PRSC-
		 */
		if ($recXX == 0): ?>
		<div class="row">
				<center>
				<h5><i>No Documents Associated with this Sub-Category</i></h5>
				</center>
				<br>
		</div>		
	<?php endif;?>

	<!-- EO BLOCK -->	
		
	<?php 	endforeach;					// EO if foreach subcatt ?>

<?php else: ?>

		<?php 
		/*------------------------------------------------
		 * 	No Data for this Subcategory Displayed
		 *------------------------------------------PRSC-
		 */?>
		 
		<div class="row">
				<center>
				<h5><i>No Sub-Categories to Display</i></h5>
				<center>
		</div>		

<?php endif;						// EO if Subcats are empty ?>

<br>
<?php 
	// This is a problem as the response page is wrong in create_subcat
	//if($roleREC['AddSubcatsYN'] == 'Y'):
	$todo = FALSE;
	if($todo):
?>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>
		<?php  print $link_add_subcat_to_cat . '/' . $CID ?>" class="btn btn-success pull-right">Add New Sub-Category to this Category</a>
	</div>
</div>

<?php endif;					// EO If can Add a Subcat or not?>

<br><br><br>

</div>
<?php endif;						// EO if Subcats can be viewed ?>

			
<!-- EO PANE 2 ########################################################################## -->			
			
			
			
		
<!-- SO PANE 3 ########################################################################## -->
			


<?php 
	if($roleREC['viewAccessYN'] == 'Y'):
?>

	<div role="tabpanel" class="tab-pane" id="userswithaccess">

<?php 
/*===================================================================================
 * 
 * 		MODULE: 	Show Users
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the EMO Users.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "show-users";
$link_delete_entry			= "delthis-user";
$link_disable_entry			= "disable-user";

$link_add_entry				= "add-user";
$link_update				= "display-user";
$link_modify_rec			= "change-user";
$link_search				= "search-user";
?>



<div class="row" style="height:40px;background:rgb(255,128,128);color:white">

	<div class="col-md-12">
		<h4>Users with Access</h4>
	</div>
</div>



<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="User"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>




<?php if (!empty($usersARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Record #</th>
				<th nowrap>First Name</th>
				<th nowrap>Last Name</th>
				<th nowrap>ActiveYN</th>
				<th nowrap>User Group</th>
				<th nowrap>Send Email</th>
<?php 
	if($roleREC['ModifyUsersYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>

<?php 
	if($roleREC['DisableUsersYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>

<?php 
	if($roleREC['DeleteUsersYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>
			</thead>
			<tbody class="tbody">
					<?php foreach ($usersARR as $workREC): ?>
						<tr>

<?php 
	if($roleREC['detailUsersYN'] == 'Y'):
?>
			<td>
			<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['UID'] ?>">
			   	<?= $workREC['UID'] ?></a>
			</td>
<?php else: ?>
			<td>
			   	<?php print $workREC['UID'] ?>
			</td>
<?php endif ?>
							
							<td>
								<?= $workREC['FirstName']  ?>
							</td>
							<td>
								<?= $workREC['LastName']  ?>
							</td>
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	ActiveYN Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($workREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
					print $wrkValue;
					?>
		</td>
							
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Group Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UserTypesARR as $nxREC)
					{ 
						if($workREC['UserGroup'] == $nxREC['UserTypesID'])
						{
						$wrkValue = $nxREC['UserTypesShortName'];
						}	
					}					
					print $wrkValue;
					?>
		</td>

			<td>
				<a href="mailto:<?php echo $workREC['Email'] ?>?Subject=EMONotice" target="_top">E-Mail</a>
			</td>



<?php 
	if($roleREC['ModifyUsersYN'] == 'Y'):
?>
			<td>
				<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['UID'] ?>">
			   	Modify</a>
			</td>
<?php endif ?>

<?php 
	if($roleREC['DisableUsersYN'] == 'Y'):
?>

	<?php 
		if($workREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_user_entry; print "/" . $workREC['UID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="UID" value="<?= $workREC['UID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_user_entry; print "/" . $workREC['UID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="UID" value="<?= $workREC['UID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							
			
			
<?php 
	if($roleREC['DeleteUsersYN'] == 'Y'):
?>
							
			<td>
				<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['UID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger">
						DEL
					</button>
					<input type="hidden" name="UID" value="<?= $workREC['UID'] ?>" />
				</form>
			</td>
<?php endif ?>			
			</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>



		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h5><i>No Users with Access to Display </i></h5>
				<center>
		</div>		

<?php endif ?>

			
<?php 
	if($roleREC['AddAccessYN'] == 'Y'):
?>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>
		<?php  print $link_add_access_to_cat . '/' . $CID ?>" class="btn btn-success pull-right">Add New Access to Category</a>
	</div>
</div>

<?php endif;					// EO If can Add a User or not?>
<br><br><br>

<?php endif;					// Are ViewUsersYN Allowed ?>

			</div>
			
			
<!-- EO PANE 3 ######################################################################### -->

		</div>

	</div>
</div>

