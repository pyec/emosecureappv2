<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($categoryREC);
// echo "</pre>";

// print $categoryREC['Name'];
 
$link_main_page 		= "show-categories"; 
?>

<?php 
	echo form_open('flipit-category');
	
	echo form_hidden('LastModBy',$this->session->userdata('UserName'));
	echo form_hidden('mod_state',$mod_state);
	if(!empty($categoryREC)) 
		    echo form_hidden('CID',$categoryREC['CID']);
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Category
                </h1>              
                 <b>[Change State Screen]</b>            
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CID">Category Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CID']))
							print $categoryREC['CID'];
						else 
							print "--";	
							?>
					</div>
				</div>

	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CategoryShortName">Category Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['Name']))
							print $categoryREC['Name'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($categoryREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $accessREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
			
			
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModBy">Last Mod By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['LastModBy']))
							print $categoryREC['LastModBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModDate">Last Mod Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['LastModDate']))
							print $categoryREC['LastModDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
				
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created By  
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedBy">Created By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CreatedBy']))
							print $categoryREC['CreatedBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedDate">Created Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CreatedDate']))
							print $categoryREC['CreatedDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

			

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="CategoryDesc">Category Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($categoryREC['CategoryDesc']))
					print $categoryREC['CategoryDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

		<div class="text-center" style="padding:15px 0;">

<?php 
	if($mod_state == 2):
?>
			<button type="submit" name="choice" value="ENABLE" class="btn btn-info" style="background:rgb(0,160,0)">
			Yes - Please Enable This
		 	</button>
			<button type="submit" name="choice" value="NO" class="btn btn-info">
			No
			</button>
<?php else: ?>
			<button type="submit" name="choice" value="DISABLE" class="btn btn-info" style="background:rgb(222,160,0)">
			Yes - Please Disable This
		 	</button>
			<button type="submit" name="choice" value="NO" class="btn btn-info">
			No
			</button>

<?php endif; 									//  The Disable Enable Question?>
			</form>
 		 	
		</div>
		
	 </div>
		
	</div>

</div>
</div>
</div>
</form>



