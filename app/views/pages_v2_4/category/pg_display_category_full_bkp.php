
<?php
/* ---------------------------------------------------------------------
 * 
 *		MODULE:			pg_permit_view_V2_4.php
 *		AUTHOR:			Unknown
 *		CREATED:		Unknown
 *
 *		This is used to display a Clients Permit List.  From this
 *		page they can also click on Fee and Inspectons. 
 * 
 * 		MODIFICATION HISTORY
 * 
 * 		20160620		PRSC	Fixed CLOB fields for display
 * 		20160906		PRSC	Trying to clean up logic back to controller	
 * 								File naming convention for versions added.
 * 
 * ----------------------------------------------------------------------
 */
?>

<div class="col-sm-10 hfx_content">
	<div class="form-group">
		<div class="col-sm-12 text-center">
			<h3>Building Permit for</h3>
		</div>
	</div>


	<div role="tabpanel">
		<ul class="nav nav-tabs" role="tablist">


<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

		<!-- Nav tabs -->
			<li role="presentation"  class="active">
			<a href="#documents" aria-controls="documents" role="tab" data-toggle="tab">
			Base Documents</a>
			</li>

<?php endif ?>

<?php 
	if($roleREC['viewSubcatsYN'] == 'Y'):
?>
			
			<li role="presentation">
			<a href="#subcats" aria-controls="subcats" role="tab" data-toggle="tab">
			Sub-Categories</a>
			</li>
<?php endif ?>

<?php 
	if($roleREC['viewUsersYN'] == 'Y'):
?>
			<li role="presentation">
			<a href="#users" aria-controls="users" role="tab" data-toggle="tab">
			Users</a>
			</li>
<?php endif ?>
			
		</ul>



		<!-- Tab panes -->
		<div class="tab-content">

<?php /*############################################################ SO BLOCK 1 */ ?>			
	
			<div role="tabpanel" class="tab-pane" id="documents">



<?php 
/*===================================================================================
 * 	
 * 		SECTION:	Show Documents by Category
 * 		ADDED:		Stephen Chafe
 * 	
 *  This shows the core Documents that are not in Sub-categories
 *  
 * 	Note: This should only be for Admins
 * ==================================================================================
 */

$link_back					= "show-documents";


$link_display_docs				= "display-document";
$link_delete_docs_entry			= "delthis-document";
$link_disable_docs_entry		= "disable-document";
$link_add_docs_entry			= "add-document";
$link_update_docs				= "display-document";
$link_modify_docs_rec			= "change-document";

?>


<?php 
/*---------------------------------------------------------------
 * 			SHOW NON-SUB Category Documents assoc with category
 * --------------------------------------------------------------
 */



if(!empty($documentsARR)): ?>

<div class="row" style="height:40px;background:rgb(192,192,192);color:white">

	<div class="col-md-12">
		<h4>Associated Documents List</h4>
	</div>
</div>
	<div class="row">
			<div class="col-md-12">
			<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Document #</th>
				<th nowrap>Document Code</th>
				<th nowrap>Document Name</th>
				<th nowrap>Version</th>
				<th nowrap>Short Desc</th>
				<th nowrap>Last Mod</th>
				
				
<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif ?>	

<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif ?>


<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif ?>
			</thead>
					<tbody class="tbody">
	
		<?php foreach ($documentsARR as $documentREC): ?>
		
		 <?php 
		 
 //		 echo "<pre>############################DOC REC ";   // DBG
 //		 print_r($documentREC);
//		 echo "</pre>";
		 
		 if($documentREC['SCID'] == '') : 
		 
		 /*...........List the Document as it is not in a Sub-Category... - PRSC */
		 ?>
		<tr>

<?php 
	if($roleREC['detailDocsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_update_docs; ?>/<?= $documentREC['DocID'] ?>">
		   	<?= $documentREC['DocID'] ?></a>
		</td>
<?php else: ?>							
		<td>
		   	<?= $documentREC['DocID'] ?>
		</td>
<?php endif ?>
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Code
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocCD']  ?>
		</td>



		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortName']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Version
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['Version']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document ShortDesc
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortDesc']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['LastModDate']  ?>
		</td>


<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_modify_docs_rec; ?>/<?= $documentREC['DocID'] ?>">
			   	Modify</a>
		</td>
<?php endif ?>


<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
		<td>
			<form action="<?php echo base_url(); print $link_disable_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
				<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
				DISABLE
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
		</td>
			
<?php endif ?>

<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
		<td>
			<form action="<?php echo base_url(); print $link_delete_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
				<button type="submit" class="btn btn-sm btn-danger">
				DEL
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
		</td>
			
<?php endif ?>
</tr>
		<?php 
		/*.....................................................*/
		endif;						// EO if Doc has same Subcat ?>

		<?php endforeach;			// EO if for each document ?>

			</tbody>
		</table>
	</div>	
</div>		

		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h5><i>No General Documents Associated with this Category. Found</i></h5>
				<center>
		</div>		
</div>


		
<?php
	/*.....................................................*/
	endif;						// EO if There are Documents to Show empty ?>



			</div>
<?php /*############################################################ EO BLOCK 1 */ ?>			
			
			
<?php /*############################################################ SO BLOCK 2 */ ?>			
			

<?php 
	if($roleREC['viewSubcatsYN'] == 'Y'):
?>

		
		<!-- Tab panes -->
		<div role="tabpanel" class="tab-pane" id="subcats">



<?php 
/*===================================================================================
 * 	
 * 		SECTION:	Show Documents by Sub-Category
 * 		ADDED:		Stephen Chafe
 * 	
 *  This block is used to LIST ALL Applicable Users who hae access to this category.
 * 
 * 	Note: This should only be for Admins
 * ==================================================================================
 */
?>



<?php 
/*---------------------------------------------------------------
 * 			NEXT LOOP trhough all Sub-Category Docs associated
 * 			with this Category
 * --------------------------------------------------------------
 */


if(!Empty($subcatsARR)): ?>


    <?php 
		foreach ($subcatsARR as $subcatREC):
		// Show the Headers with the SubCategory	
		?>
		
	<div class="row" style="height:40px;background:green;color:white">
		<div class="col-md-12">
			<h4>&nbsp 
			<?php
			// Print the Subcat Header
//			echo '<pre>';						// DBG
//			print_r($subcatREC);
//			echo '</pre>';
			
			if(!empty($subcatREC['SubcatShortName']))
			{
				if($subcatREC['SubcatShortName'])
					print $subcatREC['SubcatShortName'];
			}	
			else 
				print "Unknown Sub-Category";	
			?>
			</h4>
		</div>
	</div>		
		
		<div class="row">
			<div class="col-md-12">
			<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Document #</th>
				<th nowrap>Document Code</th>
				<th nowrap>Document Name</th>
				<th nowrap>Version</th>
				<th nowrap>Short Desc</th>
				<th nowrap>Last Mod</th>
				
<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif ?>	

<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif ?>


<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif ?>


</thead>
			
<tbody class="tbody">
	
		<?php 
		
		$recXX		= 0;
		foreach ($documentsARR as $documentREC): ?>
		
		 <?php
		 $subcatYN	= FALSE;
		 $subcatID	= -1; 
		 if(!empty($documentREC['SCID']))
		 	{
		 		$subcatYN  = TRUE;
		 		$subcatID  = $documentREC['SCID'];
		 	}
		 if($subcatYN == TRUE && $subcatID == $subcatREC['SCID']) : 
		 /*...........List the Document............. - PRSC */
		 ?>

		<tr >



<?php 
   //---------------------------------------------------------------------------
	if($roleREC['detailDocsYN'] == 'Y'):
?>
		<td style="height:20px;vertical-align:middle">
		<a href="<?= base_url(); print $link_update_docs; ?>/<?= $documentREC['DocID'] ?>">
			   	<?= $documentREC['DocID'] ?></a>
		</td>
<?php else: ?>
		<td style="height:20px;vertical-align:middle">
			   	<?= $documentREC['DocID']; print "[" . $roleREC['detailDocsYN'] . "]" ?>
		</td>
<?php endif;
   //------------------------------------------------------ EO If Details Doc ?>							


		<td style="height:20px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Code
		 * -----------------------------------------------------PRSC 201603
		 */	
			$recXX++;					// Count number of docs shown
		
			?>
				<?= $documentREC['DocCD']  ?>
		</td>



		<td style="height:20px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortName']  ?>
		</td>

		<td style="height:20px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Version
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['Version']  ?>
		</td>

		<td style="height:20px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document ShortDesc
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortDesc']  ?>
		</td>

		<td style="height:20px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['LastModDate']  ?>
		</td>

<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>

		<td style="height:20px;vertical-align:middle">
			<a href="<?= base_url(); print $link_modify_docs_rec; ?>/<?= $documentREC['DocID'] ?>">
			   	Modify</a>
		</td>
<?php endif 							// EO If Modify	?>


<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>

		<form action="<?php echo base_url(); print $link_disable_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
		<td style="height:20px;vertical-align:middle">
				<button type="submit" class="btn btn-sm btn-danger" 
							style="height:25px;vertical-align:middle;background:blue">
				DISABLE
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
		</td>
		</form>
<?php endif 							// EO Disable Docs ?>



<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>

		<form action="<?php echo base_url(); print $link_delete_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
		<td style="height:20px;vertical-align:middle">
				<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle">
				DEL
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
		</td>
		</form>
<?php endif 							// EO Delete Docs ?>


	</tr>		
		

		<?php 
		/*.....................................................*/
		endif;						// EO if Doc has same Subcat ?>

		<?php endforeach;			// EO if for each document ?>
		
		
		</tbody>
		</table>
	</div>	
</div>		

		<?php 
		/*------------------------------------------------
		 * 	No Data for this Subcategory Displayed
		 *------------------------------------------PRSC-
		 */
		if ($recXX == 0): ?>
		<div class="row">
				<center>
				<h5><i>No Documents Associated with this Sub-Category</i></h5>
				<center>
		</div>		
		<?php endif;?>

			
		<?php 
		/*.....................................................*/
		endforeach;					// EO if foreach subcatt ?>

		
		<?php
		/*.....................................................*/
		endif;						// EO if Subcats are empty ?>

		
<?php endif 						// If Show Subcats?>

</div>
<?php /*############################################################ EO BLOCK 2 */ ?>			
			
			
<?php /*############################################################ SO BLOCK 3 */ ?>			
			

<?php 
if($roleREC['viewUsersYN'] == 'Y'):
?>
		
<div role="tabpanel" class="tab-pane" id="users">



<?php 
/*===================================================================================
 * 	
 * 		SECTION:	List Users who have access to this Category
 * 		ADDED:		Stephen Chafe
 * 	
 *  This block is used for Admins or Ops to LIST ALL Applicable Users who 
 *  have access to this category.
 * 
 * ==================================================================================
 */

/* Internal Define overrides */

$link_display_user				= "display-user";
$link_delete_user_entry			= "delthis-user";
$link_disable_user_entry			= "disable-user";
$link_add_user_entry			= "add-user";
$link_update_user				= "display-user";
$link_modify_user_rec			= "change-user";

?>


<?php if (!empty($usersARR)): ?>

<div class="row" >
	<div class="col-sm-4">
		&nbsp  <h4>Associated Users</h4>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Record #</th>
				<th nowrap>First Name</th>
				<th nowrap>Last Name</th>
				<th nowrap>ActiveYN</th>
				<th nowrap>User Group</th>
	<?php 
	if($roleREC['ModifyUsersYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif ?>	

<?php 
	if($roleREC['DisableUsersYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif ?>


<?php 
	if($roleREC['DeleteUsersYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif ?>
			</thead>

	<tbody class="tbody">
					<?php foreach ($usersARR as $userREC): ?>
						<tr>

<?php 
	if($roleREC['detailUsersYN'] == 'Y'):
?>
				<td style="height:20px;vertical-align:middle">
				<a href="<?= base_url(); print $link_update_user; ?>/<?= $userREC['UID'] ?>">
			   	<?= $userREC['UID'] ?></a>
				</td>
<?php else: ?>
				<td style="height:20px;vertical-align:middle">
			   	<?= $userREC['UID'] ?>
				</td>
<?php endif ?>				
							
							<td style="height:20px;vertical-align:middle">
								<?= $userREC['FirstName']  ?>
							</td>
							<td style="height:20px;vertical-align:middle">
								<?= $userREC['LastName']  ?>
							</td>
							
		<td style="height:20px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	ActiveYN Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($userREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
					print $wrkValue;
					?>
			</div>
		</div>
		</td>
							
		<td style="height:20px;vertical-align:middle">
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Group Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UserTypesARR as $nxREC)
					{ 
						if($userREC['UserGroup'] == $nxREC['UserTypesID'])
						{
						$wrkValue = $nxREC['UserTypesShortName'];
						}	
					}					
					print $wrkValue;
					?>
			</div>
		</div>
		</td>

<?php 
	if($roleREC['ModifyUsersYN'] == 'Y'):
?>

		<td style="height:20px;vertical-align:middle">
				<a href="<?= base_url(); print $link_modify_user_rec; ?>/<?= $userREC['UID'] ?>">
			   	Modify</a>
		</td>
<?php endif ?>

<?php 
	if($roleREC['DisableUsersYN'] == 'Y'):
?>
		<form action="<?php echo base_url(); print $link_disable_user_entry; print "/" . $userREC['UID'] ?>" method="POST" >
			<td style="height:20px;vertical-align:middle">
			<button type="submit" class="btn btn-sm btn-danger" 
					style="height:25px;vertical-align:middle;background:rgb(222,160,0)">
			DISABLE
			</button>
			<input type="hidden" name="UID" value="<?= $userREC['UID'] ?>" />
			</td>
		</form>
<?php endif ?>


			
<?php 
	if($roleREC['DeleteUsersYN'] == 'Y'):
?>
		<form action="<?php echo base_url(); print $link_delete_user_entry; print "/" . $userREC['UID'] ?>" method="POST" >
			<td style="height:20px;vertical-align:middle">
						<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle">
						DEL
						</button>
						<input type="hidden" name="UID" value="<?= $userREC['UID'] ?>" />
						</td>
					</form>
<?php endif ?>


					</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>


<?php 
/*------------------------------------------------
 * 	No Data Screen Display to End User
 * ------------------------------------------PRSC-
 */
	else: ?>

		<div class="row">
				<center>
				<h5><i>No Users Associated with this Category were Found</i></h5>
				<center>
		</div>		

<?php endif;				// EO If Data to Show ?>			
			
			
			

</div>

<?php endif;				// EO If Show Users ?>			

<?php /*############################################################ EO BLOCK 3 */ ?>			



		</div>

	</div>
</div>
