<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_display_category_full_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 		This is also meant to show all the users records with Access
 * 		to this Category by CID.
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-04		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back						= "user-sigin";
$link_signin					= "user-signin";
$link_reqnew					= "user-requestnew";
$link_forgot_password			= "user-forgot-password";

$link_display_user				= "display-user";
$link_delete_user_entry			= "delthis-user";
$link_disable_user_entry			= "disable-user";
$link_add_user_entry			= "add-user";
$link_update_user				= "display-user";
$link_modify_user_rec			= "change-user";

$link_display_docs				= "display-docs";
$link_delete_docs_entry			= "delthis-docs";
$link_disable_docs_entry			= "disable-docs";
$link_add_docs_entry			= "add-docs";
$link_update_docs				= "display-docs";
$link_modify_docs_rec			= "change-docs";

$link_search					= "category-search";
?>

<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
 echo "<pre>";
 print_r($categoryREC);
 echo "</pre>                           HELLO";

// print $categoryREC['Name'];
 
 
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Category 
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CID">Category Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CID']))
							print $categoryREC['CID'];
						else 
							print "--";	
							?>
					</div>
				</div>

	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CategoryShortName">Category Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['Name']))
							print $categoryREC['Name'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
			

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="CategoryDesc">Category Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($categoryREC['CategoryDesc']))
					print $categoryREC['CategoryDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open('show-categories');
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to List
			</button>
	
			</form>
 		 	
		</div>
				
				
			
			</div>
		
	</div>

</div>
</div>
</div>


	<div role="tabpanel">
		<ul class="nav nav-tabs" role="tablist">


<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

		<!-- Nav tabs -->
			<li role="presentation"  class="active">
			<a href="#documents" aria-controls="documents" role="tab" data-toggle="tab">
			Base Documents</a>
			</li>

<?php endif ?>

<?php 
	if($roleREC['viewSubcatsYN'] == 'Y'):
?>
			
			<li role="presentation">
			<a href="#subcats" aria-controls="subcats" role="tab" data-toggle="tab">
			Sub-Categories</a>
			</li>
<?php endif ?>

<?php 
	if($roleREC['viewUsersYN'] == 'Y'):
?>
			<li role="presentation">
			<a href="#users" aria-controls="users" role="tab" data-toggle="tab">
			Users</a>
			</li>
<?php endif ?>
			
		</ul>



<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane" id="documents">



<?php 
/*===================================================================================
 * 	
 * 		SECTION:	Show Documents by Category
 * 		ADDED:		Stephen Chafe
 * 	
 *  This shows the core Documents that are not in Sub-categories
 *  
 * 	Note: This should only be for Admins
 * ==================================================================================
 */

$link_back					= "show-documents";

?>




<?php 
/*---------------------------------------------------------------
 * 			SHOW NON-SUB Category Documents assoc with category
 * --------------------------------------------------------------
 */



if(!empty($documentsARR)): ?>

<div class="row" style="height:40px;background:rgb(192,192,192);color:white">

	<div class="col-md-12">
		<h4>Associated Documents List</h4>
	</div>
</div>


	<div class="row">
			<div class="col-md-12">
			<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Document #</th>
				<th nowrap>Document Code</th>
				<th nowrap>Document Name</th>
				<th nowrap>Version</th>
				<th nowrap>Short Desc</th>
				<th nowrap>Last Mod</th>
				
				
<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif ?>	

<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif ?>


<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif ?>


	</thead>
	<tbody class="tbody">
	
		<?php foreach ($documentsARR as $documentREC): ?>
		
		 <?php 
		 
 //		 echo "<pre>############################DOC REC ";   // DBG
 //		 print_r($documentREC);
//		 echo "</pre>";
		 
		 if($documentREC['SCID'] == '') : 
		 
		 /*...........List the Document as it is not in a Sub-Category... - PRSC */
		 ?>
		 
		 
		<tr>

<?php 
	if($roleREC['detailDocsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_update_docs; ?>/<?= $documentREC['DocID'] ?>">
		   	<?= $documentREC['DocID'] ?></a>
		</td>
<?php else: ?>							
		<td>
		   	<?= $documentREC['DocID'] ?>
		</td>
	<?php endif;					// EO Allow Detail or not Cell ?>
		   	

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Code
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocCD']  ?>
		</td>



		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortName']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Version
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['Version']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document ShortDesc
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['DocShortDesc']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $documentREC['LastModDate']  ?>
		</td>


<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_modify_docs_rec; ?>/<?= $documentREC['DocID'] ?>">
			   	Modify</a>
		</td>
	<?php endif;					// EO Modify Cell ?>


<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
		<td>
			<form action="<?php echo base_url(); print $link_disable_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
				<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
				DISABLE
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
		</td>
			
	<?php endif;					// EO Disable Cell ?>

<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
		<td>
			<form action="<?php echo base_url(); print $link_delete_docs_entry; print "/" . $documentREC['DocID'] ?>" method="POST" >
				<button type="submit" class="btn btn-sm btn-danger">
				DEL
				</button>
				<input type="hidden" name="DocID" value="<?= $documentREC['DocID'] ?>" />
				</form>
		</td>
			
	<?php endif;					// EO Del Cell ?>
	
</tr>
		<?php endif;				// EO if Doc has same Subcat ?>

		<?php endforeach;			// EO if for each document in list ?>

			</tbody>
		</table>
	</div>	
</div>		

	
		
<?php
	/*.....................................................*/
	endif;						// EO if There are Documents to Show empty ?>

	<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h5><i>No General Documents Associated with this Category. Found</i></h5>
				<center>
		</div>		

</div>

<br><br><br>


