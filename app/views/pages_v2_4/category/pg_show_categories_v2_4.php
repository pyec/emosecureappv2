<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_show_categories_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the EMO Categories.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "show-categories";
$link_delete_entry			= "delthis-category";
$link_disable_entry			= "disable-category";
$link_enable_entry			= "enable-category";
$link_add_entry				= "add-category";
$link_update				= "display-category";
$link_modify_rec			= "change-category";
$link_search				= "search-category";

$link_adddoc				= "add-document-to-cat";


/*................FOR Debug Dump use...........PRSC */

$dbgYN		= FALSE;
if($dbgYN)
	{	
		$vmsg = "SHOW CATEGORIES";
		print "===================================----->>>> SO [" . $vmsg . "]  Data DMP)";
		print "<pre>";
		if(!empty($categoriesARR))
			print_r($categoriesARR);
		else 
			print "NO CELLS IN Category ARRAY<br><br>";	
		print "</pre>--------------<<<< EO [" . $vmsg . "]   Data DMP Ends)<br><br>";
		
	}	

?>



<div class="row">
	<div class="col-md-12">
	<div class="col-md-12">
		&nbsp <h2>Categories</h2>
	</div>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>


<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="Category"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>
	</div>
</div>



<?php if (!empty($categoriesARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Category ID</th>
				<th nowrap>Category Name</th>
				
<?php 
	if($roleREC['ModifyCatsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>



<?php 
	if($roleREC['AddDocsYN'] == 'Y'):
?>
				<th>Add Doc</th>
<?php endif; ?>
							

<?php 
	if($roleREC['DisableCatsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>



<?php 
	if($roleREC['DeleteCatsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>
			</thead>
			<tbody class="tbody">
					<?php foreach ($categoriesARR as $workREC): ?>
						<tr>


<?php 
	if($roleREC['detailCatsYN'] == 'Y'):
?>

		<td>
			<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['CID'] ?>">
		   	<?= $workREC['CID']; ?></a>
		</td>

<?php else: ?>
		<td>
		   	<?= $workREC['CID']; ?>
		</td>
<?php endif ?>		
		
		
							
							<td>
								<?= $workREC['Name']  ?>
							</td>
							

								
<?php 
	if($roleREC['ModifyCatsYN'] == 'Y'):
?>
			<td>
				<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['CID'] ?>">
			   	Modify</a>
			</td>
<?php endif ?>


<?php 
	if($roleREC['AddDocsYN'] == 'Y'):
?>
							
			<td>
				<form action="<?php echo base_url(); print $link_adddoc; print "/" . $workREC['CID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					NEW DOC
					</button>
					<input type="hidden" name="CID" value="<?= $workREC['CID'] ?>" />
				</form>
			</td>
<?php endif; ?>							






<?php 
	if($roleREC['DisableCatsYN'] == 'Y'):
?>

	<?php 
		if($workREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_entry; print "/" . $workREC['CID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="CID" value="<?= $workREC['CID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_entry; print "/" . $workREC['CID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="CID" value="<?= $workREC['CID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							


<?php 
	if($roleREC['DeleteCatsYN'] == 'Y'):
?>
							
							<td>
								<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['CID'] ?>" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger">
									DEL
									</button>
									<input type="hidden" name="CID" value="<?= $workREC['CID'] ?>" />
								</form>
							</td>
<?php endif; ?>							
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>
		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h4><i>No Categories Matching Search Criteria Found</i></h4>
				<center>
		</div>		

<?php endif ?>


<?php 
	if($roleREC['AddCatsYN'] == 'Y'):
?>

	<div class="row">
		<div class="col-md-12">
			<a href="<?php echo base_url(); ?><?php  print $link_add_entry ?>" class="btn btn-success pull-right">Add Category</a>
		</div>
	</div>

<?php endif ?>

