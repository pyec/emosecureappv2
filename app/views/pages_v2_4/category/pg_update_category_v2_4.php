<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($categoryREC);
// echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($categoryREC)) 
				{
				echo form_open('create-category');
				}
			else
				{
				echo form_open('update-category');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('Email'));
			// if this is an update to set the Category ID;
			if(!empty($categoryREC)) 
			         echo form_hidden('CategoryID',$categoryREC['CID']);
			?>
 
  				<h1>
 				 <?= (!empty($categoryREC)) ? 'Edit' : 'Create' ?> Category</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CID">Category Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CID']))
							print $categoryREC['CID'];
						else 
							print "--";	
							?>
					</div>
				</div>
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Name">Category Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="Name" class="form-control"
						 value="<?= set_value('Name', (!empty($categoryREC) ?
						  $categoryREC['Name'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($categoryREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $accessREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>

			

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


				<div class="row">
		<div class="col-sm-4">

					<label for="CategoryDesc">Category Description (500 digits)</label>

		</div>
		<div class="col-sm-8">

			<textarea name="CategoryDesc" rows="5" columns="80" style="width:600px; height: 120px;">
			<?= set_value('CategoryDesc',
			 (!empty($categoryREC) ? $categoryREC['CategoryDesc'] : '')) ?></textarea>
			<script>
			 CKEDITOR.replace( 'CategoryDesc', {
				toolbar: [
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
				]
				});
		</script>

		</div>
	</div>



		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($categoryREC))
					 ? 'Update' : 'Create' ?> Category</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




