<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($categoryREC);
// echo "</pre>";

// print $categoryREC['Name'];
 
 
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Category
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CID">Category Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['CID']))
							print $categoryREC['CID'];
						else 
							print "--";	
							?>
					</div>
				</div>

	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CategoryShortName">Category Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($categoryREC['Name']))
							print $categoryREC['Name'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
			

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Category Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="CategoryDesc">Category Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($categoryREC['CategoryDesc']))
					print $categoryREC['CategoryDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open('show-categories');
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to List
			</button>
	
			</form>
 		 	
		</div>
				
				
			
			</div>
		
	</div>

</div>
</div>
</div>




