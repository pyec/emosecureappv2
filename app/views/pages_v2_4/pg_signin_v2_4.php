<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_sigin_view_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-18		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "user-sigin";
$link_signin				= "user-signin";
$link_reqnew				= "user-requestnew";
$link_forgot_password		= "user-forgot-password";


/*  Add Assets translations here as well - PRSC TODO */

$asset_bootstrap 	= "_assets/plugins/bootstrap/css/bootstrap.min.css";
$asset_fonts		= "_assets/plugins/font-awesome/css/font-awesome.min.css";
$asset_app_css		= "_assets/app.css";

?>



<!DOCTYPE html>
<html lang="en">


    <head>
    
        <title>EMO Secure Login</title>
        
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/app.css">

        <!-- <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'> -->
        
        <script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!-- <script src="<?= base_url() ?>_assets/plugins/ckeditor/ckeditor.js"></script> -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>

            $(document).ready(function () {
                
            });

        </script>
        
    </head>
    
    <body>

        
        <div id="hfx_content_area" class="container">
                <div class="row">
                    <div class="col-sm-5 col-md-4 col-sm-offset-4">

                        <div class="text-center">
                            <img src="<?= base_url() ?>_assets/images/EMO_logo.jpg">
                            <h1 class="form-signin-heading">EMO Secure</h1>
                        </div>

                        <hr>

                        <h2 class="form-signin-heading">Sign in</h2>

                        <?php if($this->session->flashdata('confirmation')): ?>
                        <div class="alert alert-success text-center">
                            <?= $this->session->flashdata('confirmation') ?>
                        </div>
                        <?php endif ?>

                        <?php if($this->session->flashdata('error')): ?>
                        <div class="alert alert-danger text-center">
                            <?= $this->session->flashdata('error') ?>
                        </div>
                        <?php endif ?>

                        <?php if(validation_errors()): ?>
                        <div class="alert alert-danger text-center">
                            <?= validation_errors() ?>
                        </div>
                        <?php endif ?>

                        <form action="<?= base_url() ; echo $link_signin ?>" method="POST" class="form-signin">                         
                            
                            <label for="email_address" class="sr-only">Email Address</label>
                            <input type="email_address" class="form-control" placeholder="Email Address" name="email_address" maxlength="50" tabindex="1" autocomplete="off" autofocus />

                            <label for="password" class="sr-only">Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password" maxlength="128" tabindex="2" />
                            
                            <div style="padding-top:10px;">
                                <button class="btn btn-primary" type="submit">Sign in</button>
                                <a href="<?= base_url(); echo $link_forgot_password ?>" class="btn btn-link pull-right">Forgot password?</a>
                            </div>

                        </form>

                        <hr>

 
 
                    </div>
                </div>
            </div>
        </div>


    </body>

</html>