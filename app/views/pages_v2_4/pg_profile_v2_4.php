<?php //$this->session->set_flashdata('uri', uri_string()) ?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>

<!-- Page -->
<div id="page">
	<div class="container">

		<!-- <pre>
		<?php print_r($session_data) ?>
		</pre> -->

		<div class="row" style="background:#F8F8F8; padding:30px 0;">
			
			<div class="col-sm-2 text-center">
				<!-- <i class="fa fa-street-view" style="font-size:20em;color:#286090"></i> -->
				<!-- <i class="fa fa-female" style="font-size:20em;color:#286090"></i> -->
				<i class="fa fa-male" style="font-size:10em;color:#286090"></i>
				<p style="padding:10px 0 0;">Member since<br/>2011</p>
			</div>

			<div class="col-sm-5">


<?php 
/*-----------------------------------------------------------------
 * 			Welcome Screen Area
 * ----------------------------------------------------------------
 */

?>


	<div class="row">

	<h3>Welcome to the EMO Doc Share</h3>

			<p style="padding-bottom:7px;">
			This document share has been developed to keep people who need to know,
			in the know, by providing a centralized place to post updates, information, and documentation.</p>

			<p style="padding-bottom:7px;">
			This application and manage multiple users, and multiple events, 
			and users can be granted access to one or more events at a time.</p>

			<p style="padding-bottom:7px;">
			Users can access this document share via desktops or laptop computers
			or smart mobile devices such as the iPhones and Blackberries.</p>

		<p style="padding-bottom:7px;">
			If you have questions related to this application
			or the content herein, please contact: 
			<a href="mailto:HRM_EMO@halifax.ca">HRM_EMO@halifax.ca</a>.
		</p>

		</div>

<br><br><br>
		</div>
		</div>
	</div>
</div>

