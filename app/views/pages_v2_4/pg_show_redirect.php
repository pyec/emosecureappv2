<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_show_redirect.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-29
 * 
 * 		This is the template for PDF redirect view.
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
  * 
 *===================================================================================
 */

/* Internal Define overrides */


/*  Add Assets translations here as well - PRSC TODO */

$asset_bootstrap 	= "_assets/plugins/bootstrap/css/bootstrap.min.css";
$asset_fonts		= "_assets/plugins/font-awesome/css/font-awesome.min.css";
$asset_app_css		= "_assets/app.css";

//$destfile  = base_url() . 'EMODownloads/' . $destfilename;
$destfile  = 'https://apps.halifax.ca/EMODownloads/' . $destfilename;

//print "HERE [" . $destfile . "]";
//exit;

?>


<?php
	/*------------------------------------------------------------
	 *  GENERIC FILE FOR REDIRECT 
	 *  
	 *  This is designed to do a forced redirect with no fancies.
	 * -------------------------------------------------------------
	 *
	 */

//		header("Content-type: application/pdf");
//		header("Content-Length: " . filesize($destfile));

?>

<html>
<head>
<title>EMO Document Viewer - Redirecting to Document</title>

 	<link rel="shortcut icon" href="https://www.halifax.ca/_assets/images/favicon.png">
  	<link rel="stylesheet" href="https://www.halifax.ca/_assets/styles/custom.css"> 
	<!--  meta http-equiv="refresh" content="1800" -->
	<base target="_blank" />

	<meta http-equiv="refresh" content="0; url=<?php print $destfile; ?>">

</head>

<body>


<center>
If you are not redirected automatically please click on the link below:<br>

<a href="<?php print $destfile; ?>"> View Document by Clicking heree</a>


</center>


</body>

</html>




