<!DOCTYPE html>
<html lang="en">

    <head>
    
        <title>EMO Secure (Password Change)</title>
        
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/app.css">

        <!-- <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'> -->
        
        <script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!-- <script src="<?= base_url() ?>_assets/plugins/ckeditor/ckeditor.js"></script> -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>

            $(document).ready(function () {
                
            });

        </script>
        
    </head>
    
    <body>

        
        <div id="hfx_content_area" class="container">
                <div class="row">
                    <div class="col-sm-5 col-md-4 col-sm-offset-4">

                        <div class="text-center">
                            <img src="<?= base_url() ?>_assets/images/EMO_logo.jpg">
                            <h1 class="form-signin-heading">EMO Secure Document App</h1>
                        </div>

                        <hr>

                        <h2 class="form-signin-heading">Forgot Password Page</h2>

                        <?php if($this->session->flashdata('confirmation')): ?>
                        <div class="alert alert-success text-center">
                            <?= $this->session->flashdata('confirmation') ?>
                        </div>
                        <?php endif ?>

                        <?php if($this->session->flashdata('error')): ?>
                        <div class="alert alert-danger text-center">
                            <?= $this->session->flashdata('error') ?>
                        </div>
                        <?php endif ?>

                        <?php if(validation_errors()): ?>
                        <div class="alert alert-danger text-center">
                            <?= validation_errors() ?>
                        </div>
                        <?php endif ?>

                        <?php $this->session->keep_flashdata('email_address') ?>

                        <form action="<?= base_url() ?>user-forgot-password" method="POST" class="form-signin">                         
                            
                            <label for="email_address" class="sr-only">Email Address</label>
                            <input type="email_address" class="form-control" placeholder="Email Address" name="email_address" maxlength="100" tabindex="1" autocomplete="off" autofocus />

                            <!-- <label for="password" class="sr-only">Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password" maxlength="50" tabindex="2" /> -->
                            
                            <div style="padding-top:10px;">
                                <button class="btn btn-primary" type="submit">Reset</button>
                                <a href="<?= base_url() ?>user-signin" class="btn btn-link pull-right">Remember your password? Sign in</a>
                            </div>

                        </form>

                        <hr>

  
            </div>
        </div>


    </body>

</html>