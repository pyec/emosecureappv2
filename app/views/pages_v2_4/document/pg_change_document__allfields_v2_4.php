<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_change_document_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Enter a new Document for EMO Document.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-documents";
$link_search				= "search-documents";

?>

<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($ActiveStatesARR);
// echo "</pre>";
?>


<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($documentREC)) 
				{
				echo form_open('create-document');
				}
			else
				{
				echo form_open('update-document');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('DocumentName'));
			// if this is an update to set the Document ID;
			if(!empty($documentREC)) 
			         echo form_hidden('DocID',$documentREC['DocID']);
			?>
 
  				<h1>
 				 <?= (!empty($documentREC)) ? 'Edit' : 'Create' ?> Document</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="CID">Category</label>
			</div>
			<div class="col-sm-8">			
					<select class="form-control" id="CID" name="CID">
					<option value="">Category Choice</option>
					<?php
					//	Fill in the Options to choose from 
					foreach ($CategoriesARR as $nxREC)
					{
						
					 if($documentREC['CID'] == $nxREC['CID'])
					 	{  
					 		$sel_state  = true;
					 		$sel_stuff = ' selected="selected" ';
					 	 }
					 else
					 	{  
					 		$sel_state  = false;
					 		$sel_stuff = '';
					 	}
					 	
					 $baseST = '<option name="'. $nxREC['CID'] . '" value="' . $nxREC['CID'] . '"' . $sel_stuff . '>';
					 print $baseST;	
					 	
					 $optionST = $nxREC['Name'] . " (" . $nxREC['CID'] . ") " . '</option>';
					 print $optionST;

					 set_select('CID', $nxREC['CID'],$sel_state);
					 
					} 
					?>
					
					</select>
					
			</div>
		</div>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Sub-Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="SCID">Sub-Category</label>
			</div>
			<div class="col-sm-8">			
					<select class="form-control" id="SCID" name="SCID">
					<option value="">Sub-Category Choice</option>
					<?php
					//	Fill in the Options to choose from 
					foreach ($SubcatsARR as $nxREC)
					{
						
					 if($documentREC['SCID'] == $nxREC['SCID'])
					 	{  
					 		$sel_state  = true;
					 		$sel_stuff = ' selected="selected" ';
					 	 }
					 else
					 	{  
					 		$sel_state  = false;
					 		$sel_stuff = '';
					 	}
					 	
					 $baseST = '<option name="'. $nxREC['SCID'] . '" value="' . $nxREC['SCID'] . '"' . $sel_stuff . '>';
					 print $baseST;	
					 	
					 $optionST = $nxREC['SubcatShortName'] . " (" . $nxREC['SCID'] . ") " . '</option>';
					 print $optionST;

					 set_select('SCID', $nxREC['SCID'],$sel_state);
					 
					} 
					?>
					
					</select>
					
			</div>
		</div>



		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M DocCD 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="DocCD"> Document Code (10-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="DocCD" class="form-control"
						 value="<?= set_value('DocCD', (!empty($documentREC) ?
						  $documentREC['DocCD'] : ''))
						 ?>" maxlength="10">
					</div>
				</div>


	<?php
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M DocShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="DocShortName">Document Name (40-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="DocShortName" class="form-control"
						 value="<?= set_value('DocShortName', (!empty($documentREC) ?
						  $documentREC['DocShortName'] : ''))
						 ?>" maxlength="40">
					</div>
				</div>

	<?php
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Version 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Version">Version (5-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="Version" class="form-control"
						 value="<?= set_value('Version', (!empty($documentREC) ?
						  $documentREC['Version'] : ''))
						 ?>" maxlength="5">
					</div>
				</div>

	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active - YesNo
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Activity State</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="ActiveStatesID" name="ActiveStatesID">
					<?php
					//	Fill in the Options to choose from 
					foreach ($ActiveStatesARR as $nxREC)
					{
						
					 if($documentREC['Active'] == $nxREC['ActiveStatesID'])
					 	{  
					 		$sel_state  = true;
					 		$sel_stuff = ' selected="selected" ';
					 	 }
					 else
					 	{  
					 		$sel_state  = false;
					 		$sel_stuff = '';
					 	}
					
					
					 $baseST = '<option name="'. $nxREC['ActiveStatesID'] . '" value="' . $nxREC['ActiveStatesCD'] . '"' . $sel_stuff . '>';
					 print $baseST;	
					 	
					 $optionST = $nxREC['ActiveStatesShortName'] . " (" . $nxREC['ActiveStatesCD'] . ") " . '</option>';
					 print $optionST;

					 set_select('Active', $nxREC['ActiveStatesID'],$sel_state);
					}
					?>
					
					</select>
			</div>
		</div>

		 
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


				<div class="row">
		<div class="col-sm-4">

					<label for="DocDesc">Document Description (2500 digits)</label>

		</div>
		<div class="col-sm-8">

			<textarea name="DocDesc" rows="5" columns="80" style="width:600px; height: 120px;">
			<?= set_value('DocDesc',
			 (!empty($documentREC) ? $documentREC['DocDesc'] : '')) ?></textarea>
			<script>
			 CKEDITOR.replace( 'DocDesc', {
				toolbar: [
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
				]
				});
		</script>

		</div>
	</div>



		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($documentREC))
					 ? 'Update' : 'Create' ?> Document</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




