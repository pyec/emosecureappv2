<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_show_documents_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the EMO Documents.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "show-documents";
$link_delete_entry			= "delthis-document";
$link_disable_entry			= "disable-document";
$link_enable_entry			= "enable-document";

$link_add_entry				= "add-document";
$link_update				= "display-document";
$link_modify_rec			= "change-document";
$link_search				= "search-document";

$link_document				= "show-fulldoc";				

?>


<div class="row">
	<div class="col-md-12">
		<h2>Document Listings</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>


<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="Document"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>
	</div>
</div>



<?php if (!empty($documentsARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Record #</th>
				<th nowrap>Doc Code</th>
				<th nowrap>Document Name</th>
				<th nowrap>Version</th>
				<th nowrap>Category</th>
				<th nowrap>Sub-Category</th>
				<th nowrap>View</th>
				<th nowrap>Last Mod</th>
<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>

<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>

<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>

	</thead>
	<tbody class="tbody">
			
	<?php foreach ($documentsARR as $workREC): ?>
		<tr>
	
	
<?php 
	if($roleREC['detailDocsYN'] == 'Y'):
?>
		<td>
		<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['DocID'] ?>">
	   	<?= $workREC['DocID'] ?></a>
		</td>
<?php else: ?>
		<td>
	   	<?= $workREC['DocID'] ?>
		</td>
<?php endif ?>
							

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Code
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $workREC['DocCD']  ?>
		</td>



		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $workREC['DocShortName']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document Version
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $workREC['Version']  ?>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($workREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $workREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Sub-Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($SubcatsARR as $nxREC)
					{ 
						if($workREC['SCID'] == $nxREC['SCID'])
						{
						if(!empty($nxREC['SubcatShortName']))
							 $wrkValue = $nxREC['SubcatShortName'];
						else 
							 $wrkValue = "Undefined";		 
						}	
					}					
					if($wrkValue == '')
						{
						 $wrkValue = "--";
						}
					else 
					{			
					$wrkValue = $wrkValue . "(" . $workREC['SCID'] . ")";	 
					} 
					print $wrkValue;
					?>
			</div>
		</div>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Document PDF Show
		 * -----------------------------------------------------PRSC 201603
		 */	?>

<?php 
	if($roleREC['viewDocsYN'] == 'Y'):
?>

	<?php if($workREC['DocFileName']): ?>
					<?php 
					
//					echo form_open($link_document);
					$attributes = array('target' => '_blank', 'id' => 'myform');

					echo form_open($link_document, $attributes);					
					echo form_hidden('DocFileNM',$workREC['DocFileName']);
					echo form_hidden('DocID',$workREC['DocID']);
					echo form_hidden('CID',$workREC['CID']);
					?>

					 
					<button type="submit" name="choice" value="View Attachment" class="btn btn-info"
								style="background:black">
					<i class="icon-ok icon-white"></i>
			 		PDF
					</button>
				
					<?php 
					print '</form>';
					endif; 
					?>
			<?php endif;			// EO If document defined ?>		
					
			</div>
		</div>
		</td>




		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $workREC['LastModDate']  ?>
		</td>

<?php 
	if($roleREC['ModifyDocsYN'] == 'Y'):
?>

		<td>
			<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['DocID'] ?>">
			   	Modify</a>
		</td>
<?php endif ?>
			
<?php 
	if($roleREC['DisableDocsYN'] == 'Y'):
?>

	<?php 
		if($workREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_entry; print "/" . $workREC['DocID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle;background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="DocID" value="<?= $workREC['DocID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_entry; print "/" . $workREC['DocID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="height:25px;vertical-align:middle;background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="DocID" value="<?= $workREC['DocID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							


			
<?php 
	if($roleREC['DeleteDocsYN'] == 'Y'):
?>
							
		<td>
			<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['DocID'] ?>" method="POST" >
				<button type="submit" class="btn btn-sm btn-danger">
				DEL
				</button>
				<input type="hidden" name="DocID" value="<?= $workREC['DocID'] ?>" />
				</form>
		</td>
<?php endif ?>		
		
	</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>
		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End Document
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h4><i>No Documents Matching Search Criteria Found</i></h4>
				<center>
		</div>		

<?php endif ?>


<?php 
	if($roleREC['AddDocsYN'] == 'Y'):
?>

	<div class="row">
		<div class="col-md-12">
		<a href="<?php echo base_url(); ?><?php  print $link_add_entry ?>" class="btn btn-success pull-right">Add Document</a>
		</div>
	</div>

<?php endif ?>
