<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_display_document_full_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 		This is also meant to show all the users records with Document
 * 		to this Document by CID.
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-04		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-documents";

?>

<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($documentREC);
// echo "</pre>";

// print $documentREC['Name'];
 
 
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Document
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="DocID">Document Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['DocID']))
							print $documentREC['DocID'];
						else 
							print "--";	
							?>
					</div>
				</div>



		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<div class="row">
			<div class="col-sm-4">
						<label for="CategoryID">Category ID</label>
			</div>
			<div class="col-sm-8">			


					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($documentREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $documentREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document Code 
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
					<div class="col-sm-4">
						<label for="DocCD">Document Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['DocCD']))
							print $documentREC['DocCD'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document Name 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="DocShortName">Document Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['DocShortName']))
							print $documentREC['DocShortName'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document Version 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="DocCD">Version </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['Version']))
							print $documentREC['Version'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($documentREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $documentREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document FileName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="DocCD">Document Filename </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['DocFileName']))
							print $documentREC['DocFileName'];
						else 
							print "No Attachment";	
							?>
					</div>
				</div>



		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document Location 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="DocLoc">Document Location </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['DocLoc']))
							print $documentREC['DocLoc'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
		
		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModBy">Last Mod By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['LastModBy']))
							print $documentREC['LastModBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModDate">Last Mod Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['LastModDate']))
							print $documentREC['LastModDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Create By 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedBy">Created By</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['CreatedBy']))
							print $documentREC['CreatedBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Create Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedDate">Created Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($documentREC['CreatedDate']))
							print $documentREC['CreatedDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Document Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="DocDesc">Document Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($documentREC['DocDesc']))
					print $documentREC['DocDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open($link_back);
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to List
			</button>
	
			</form>
 		 	
		</div>
	</div>
		
	</div>

</div>
</div>
</div>




