<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_showPDF_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-18		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "user-sigin";
$link_signin				= "user-signin";
$link_reqnew				= "user-requestnew";
$link_forgot_password		= "user-forgot-password";


/*  Add Assets translations here as well - PRSC TODO */

$asset_bootstrap 	= "_assets/plugins/bootstrap/css/bootstrap.min.css";
$asset_fonts		= "_assets/plugins/font-awesome/css/font-awesome.min.css";
$asset_app_css		= "_assets/app.css";

?>



<!DOCTYPE html>
<html lang="en">


    <head>
    
        <title>EMO Secure Login</title>
        
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/app.css">

        <!-- <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'> -->
        
        <script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!-- <script src="<?= base_url() ?>_assets/plugins/ckeditor/ckeditor.js"></script> -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>

            $(document).ready(function () {
                
            });

        </script>
        
    </head>
    
    <body>

        
        <div id="hfx_content_area" class="container">
  
       echo $targfile;

    </body>

</html>