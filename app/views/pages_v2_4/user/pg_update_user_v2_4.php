<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
 //echo "<pre>";
// print_r($userREC);
//echo "</pre>";
 
// echo "<pre>";
// print_r($userREC);
// print_r($UserTypesARR);
// print_r($ActiveStatesARR);
// echo "</pre>";

// print $userREC['Name'];
 
 
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  User Info [Updated]
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="UID">User Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['UID']))
							print $userREC['UID'];
						else 
							print "--";	
							?>
					</div>
				</div>

	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User FirstName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="FirstName">User First Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['FirstName']))
							print $userREC['FirstName'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
			
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User LastName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastName">User Last Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['LastName']))
							print $userREC['LastName'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Department 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Department">Department </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['Department']))
							print $userREC['Department'];
						else 
							print "N/A";	
							?>
					</div>
				</div>




		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Landline 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Landline">User Landline # </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['Landline']))
							print $userREC['Landline'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Cellphone 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CellPhone">Cell Number </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['CellPhone']))
							print $userREC['CellPhone'];
						else 
							print "N/A";	
							?>
					</div>
				</div>



		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Email 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Email">User Email/Account </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['Email']))
							print $userREC['Email'];
						else 
							print "N/A";	
							?>
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Password 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Password">User Password </label>
					</div>
					<div class="col-sm-8">
						<?php 
//						if(!empty($userREC['LastName']))
//							print $userREC['LastName'];
//						else 
//							print "N/A";	
							print "************";
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($userREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
					print $wrkValue;
					?>
			</div>
		</div>

			

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Group Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="UserTypesID">User Type</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UserTypesARR as $nxREC)
					{ 
						if($userREC['UserGroup'] == $nxREC['UserTypesID'])
						{
						$wrkValue = $nxREC['UserTypesShortName'];
						}	
					}					
					print $wrkValue;
					?>
			</div>
		</div>



	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="UserDesc">User Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($userREC['UserDesc']))
					print $userREC['UserDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open('show-users');
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to User List
			</button>
	
			</form>
 		 	
		</div>
				
				
			
			</div>
		
	</div>

</div>
</div>
</div>




