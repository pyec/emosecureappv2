<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_show_users_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the EMO Users.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "show-users";
$link_delete_entry			= "delthis-user";
$link_disable_entry			= "disable-user";
$link_enable_entry			= "enable-user";

$link_add_entry				= "add-user";
$link_update				= "display-user";
$link_modify_rec			= "change-user";
$link_search				= "search-user";
?>


<div class="row">
	<div class="col-md-12">
	<div class="col-md-12">
		<h2>Users</h2>
	</div>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>


<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="User"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>
	</div>
</div>



<?php if (!empty($usersARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Record #</th>
				<th nowrap>First Name</th>
				<th nowrap>Last Name</th>
				<th nowrap>ActiveYN</th>
				<th nowrap>User Group</th>
				<th nowrap>Send Email</th>
<?php 
	if($roleREC['ModifyUsersYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>

<?php 
	if($roleREC['DisableUsersYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>

<?php 
	if($roleREC['DeleteUsersYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>
			</thead>
			<tbody class="tbody">
					<?php foreach ($usersARR as $workREC): ?>
						<tr>

<?php 
	if($roleREC['detailUsersYN'] == 'Y'):
?>
			<td>
			<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['UID'] ?>">
			   	<?= $workREC['UID'] ?></a>
			</td>
<?php else: ?>
			<td>
			   	<?php print $workREC['UID'] ?>
			</td>
<?php endif ?>
							
							<td>
								<?= $workREC['FirstName']  ?>
							</td>
							<td>
								<?= $workREC['LastName']  ?>
							</td>
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	ActiveYN Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($workREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
					print $wrkValue;
					?>
		</td>
							
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Group Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UserTypesARR as $nxREC)
					{ 
						if($workREC['UserGroup'] == $nxREC['UserTypesID'])
						{
						$wrkValue = $nxREC['UserTypesShortName'];
						}	
					}					
					print $wrkValue;
					?>
		</td>

			<td>
				<a href="mailto:<?php echo $workREC['Email'] ?>?Subject=EMONotice" target="_top">E-Mail</a>
			</td>



<?php 
	if($roleREC['ModifyUsersYN'] == 'Y'):
?>
			<td>
				<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['UID'] ?>">
			   	Modify</a>
			</td>
<?php endif ?>



<?php 
	if($roleREC['DisableUsersYN'] == 'Y'):
?>

	<?php 
		if($workREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_entry; print "/" . $workREC['UID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="UID" value="<?= $workREC['UID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_entry; print "/" . $workREC['UID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="UID" value="<?= $workREC['UID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							
			
			
<?php 
	if($roleREC['DeleteUsersYN'] == 'Y'):
?>
							
			<td>
				<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['UID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger">
						DEL
					</button>
					<input type="hidden" name="UID" value="<?= $workREC['UID'] ?>" />
				</form>
			</td>
<?php endif ?>			
			</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>



		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h4><i>No Users To Display</i></h4>
				<center>
		</div>		

<?php endif ?>

			
<?php 
	if($roleREC['AddUsersYN'] == 'Y'):
?>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>
		<?php  print $link_add_entry ?>" class="btn btn-success pull-right">Add User</a>
	</div>
</div>

<?php endif;					// EO If can Add a User or not?>