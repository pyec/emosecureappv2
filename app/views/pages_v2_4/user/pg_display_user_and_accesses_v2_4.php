<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_display_user_and_accesses_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 *		Show the User Details, and all the groups they are in.
 * 
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-06		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-users";

$link_add_access			= 'add-access';
$link_add_access_to_user	= 'add-access-to-user';

?>
<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
 //echo "<pre>";
// print_r($userREC);
//echo "</pre>";
 
// echo "<pre>";
// print_r($userREC);
// print_r($UserTypesARR);
// print_r($ActiveStatesARR);
// echo "</pre>";

// print $userREC['Name'];
 
if(!empty($userREC['UID']))
		$UID = $userREC['UID'];

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  User Info
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="UID">User Record # </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['UID']))
							print $userREC['UID'];
						else 
							print "--";	
							?>
					</div>
				</div>

	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User FirstName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="FirstName">User First Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['FirstName']))
							print $userREC['FirstName'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
			
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User LastName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastName">User Last Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['LastName']))
							print $userREC['LastName'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Department 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Department">Department </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['Department']))
							print $userREC['Department'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Landline 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Landline">User Landline # </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['Landline']))
							print $userREC['Landline'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Cellphone 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CellPhone">Cell Number </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['CellPhone']))
							print $userREC['CellPhone'];
						else 
							print "N/A";	
							?>
					</div>
				</div>



		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Email 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Email">User Email/Account </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['Email']))
							print $userREC['Email'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Password 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Password">User Password </label>
					</div>
					<div class="col-sm-8">
						<?php 
//						if(!empty($userREC['LastName']))
//							print $userREC['LastName'];
//						else 
//							print "N/A";	
							print "************";
							?>
					</div>
				</div>



		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($userREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
					print $wrkValue;
					?>
			</div>
		</div>

			

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Group Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="UserTypesID">User Type</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UserTypesARR as $nxREC)
					{ 
						if($userREC['UserGroup'] == $nxREC['UserTypesID'])
						{
						$wrkValue = $nxREC['UserTypesShortName'];
						}	
					}					
					print $wrkValue;
					?>
			</div>
		</div>



	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="UserDesc">User Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($userREC['UserDesc']))
					print $userREC['UserDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModBy">Last Mod By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['LastModBy']))
							print $userREC['LastModBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModDate">Last Mod Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['LastModDate']))
							print $userREC['LastModDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Create By 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedBy">Created By</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['CreatedBy']))
							print $userREC['CreatedBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Create Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedDate">Created Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($userREC['CreatedDate']))
							print $userREC['CreatedDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open('show-users');
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to User List
			</button>
	
			</form>
 		 	
		</div>
				
				
			
			</div>
		
	</div>

</div>
</div>
</div>


<?php 
	if($roleREC['viewAccessYN'] == 'Y'):
?>


<?php 
/*===================================================================================
 * 
 * 		SUB-MODULE: 	Show Users
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the Access for this user.  Their Name of course 
 * 		does not need to be shown.   
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */

/* Internal Define overrides */


$link_back					= "show-accesses";
$link_delete_entry			= "delthis-access";
$link_disable_entry			= "disable-access";
$link_enable_entry			= "enable-access";
$link_add_entry				= "add-access";
$link_update				= "display-access";
$link_modify_rec			= "change-access";
$link_search				= "search-access";



?>

<div class="row" style="height:40px;background:rgb(255,128,128);color:white">

	<div class="col-md-12">
		<h4> Other Access Listings for this User</h4>
	</div>
</div>


<
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>


<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="Access"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>
	</div>
</div>



<?php if (!empty($accessesARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Record #</th>
				<th nowrap>Category</th>
				<th nowrap>ActiveYN</th>
				
				<th nowrap>Last Mod</th>
<?php 
	if($roleREC['ModifyAccessYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>

<?php 
	if($roleREC['DisableAccessYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>

<?php 
	if($roleREC['DeleteAccessYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>			

	</thead>
	<tbody class="tbody">
	
	
	<?php foreach ($accessesARR as $workREC): ?>
						<tr>
	
<?php 
	if($roleREC['detailAccessYN'] == 'Y'):
?>
		<td>
				<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['AID'] ?>">
			   	<?= $workREC['AID'] ?></a>
		</td>
		
<?php else: ?>
		<td>
			   	<?= $workREC['AID'] ?>
		</td>
<?php endif; ?>							
							
							
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($workREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Category N/A";		
					$wrkValue = $wrkValue . "(" . $workREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		</td>




		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	ActiveYN Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($workREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
					print $wrkValue;
					?>
		</td>


		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $workREC['LastModDate']  ?>
		</td>


<?php 
	if($roleREC['ModifyAccessYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['AID'] ?>">
		   	Modify</a>
		</td>
<?php endif ?>


<?php 
	if($roleREC['DisableAccessYN'] == 'Y'):
?>
	<?php 
		if($workREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_entry; print "/" . $workREC['AID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="AID" value="<?= $workREC['AID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_entry; print "/" . $workREC['AID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="AID" value="<?= $workREC['AID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							


			
<?php 
	if($roleREC['DeleteAccessYN'] == 'Y'):
?>
		<td>
			<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['AID'] ?>" method="POST" >
			<button type="submit" class="btn btn-sm btn-danger">
			DEL
			</button>
			<input type="hidden" name="AID" value="<?= $workREC['AID'] ?>" />
			</form>
		</td>
<?php endif ?>		
		</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>
		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h5><i>This User has no Access Records to Display </i></h5>
				<center>
		</div>		

<?php endif ?>

			
<?php 
	if($roleREC['AddAccessYN'] == 'Y'):
?>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>
		<?php  print $link_add_access_to_user . '/' . $UID ?>" class="btn btn-success pull-right">Add a New Access For User</a>
	</div>
</div>

<?php endif;					// EO If can Add a User or not?>
<br><br><br>

<?php endif;					// Are ViewUsersYN Allowed ?>

			
			