<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_add_user_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Enter a new User for EMO Access.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-users";
$link_search				= "search-users";

?>

<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($ActiveStatesARR);
// echo "</pre>";
?>


<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($userREC)) 
				{
				echo form_open('create-user');
				}
			else
				{
				echo form_open('update-user');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the User ID;
			if(!empty($userREC)) 
			         echo form_hidden('UID',$userREC['UID']);
			?>
 
  				<h1>
 				 <?= (!empty($userREC)) ? 'Edit' : 'Create' ?> User</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User FirstName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="FirstName">First Name (35-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="FirstName" class="form-control"
						 value="<?= set_value('FirstName', (!empty($userREC) ?
						  $userREC['FirstName'] : ''))
						 ?>" maxlength="35">
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User LastName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastName">Last Name (35-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="LastName" class="form-control"
						 value="<?= set_value('LastName', (!empty($userREC) ?
						  $userREC['LastName'] : ''))
						 ?>" maxlength="35">
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Department 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Department">Department (35-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="Department" class="form-control"
						 value="<?= set_value('Department', (!empty($userREC) ?
						  $userREC['Department'] : ''))
						 ?>" maxlength="30">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Landline 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Landline">Landline # (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="Landline" class="form-control"
						 value="<?= set_value('Landline', (!empty($userREC) ?
						  $userREC['Landline'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Cellphone 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CellPhone">Cellphone Number # (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="CellPhone" class="form-control"
						 value="<?= set_value('CellPhone', (!empty($userREC) ?
						  $userREC['CellPhone'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Email
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Email">Email / Account (75-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="Email" class="form-control"
						 value="<?= set_value('Email', (!empty($userREC) ?
						  $userREC['Email'] : ''))
						 ?>" maxlength="75">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Password
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Password">Password (128-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="password" name="Password" class="form-control"
						 value="<?= set_value('Password', (!empty($userREC) ?
						  $userREC['Password'] : ''))
						 ?>" maxlength="128">
					</div>
				</div>
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	YesNo
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStateID">Activity State</label>
			</div>
			<div class="col-sm-8">			
					<select class="form-control" id="ActiveStateID" name="ActiveStateID">
					<option value="">Active State</option>
					<?php
					//	Fill in the Options to choose from 
					foreach ($ActiveStatesARR as $nxREC): 
					?>
					<option name="<?= $nxREC['ActiveStatesCD'] ?>" 
						value="<?= $nxREC['ActiveStatesID'] ?>" 
						<?= set_select('ActiveStatesID', 
								$nxREC['ActiveStatesID'],
								 ((!empty($userREC) &&
								  $userREC['Active'] == $nxREC['ActiveStatesID']) ? TRUE : '')) ?>>
								  <?= $nxREC['ActiveStatesShortName'] ?>
								  (<?= $nxREC['ActiveStatesCD'] ?>)
					 </option>
					<?php endforeach ?>
					</select>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Group Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="UserTypesID">User Type</label>
			</div>
			<div class="col-sm-8">			
					<select class="form-control" id="UserTypesID" name="UserTypesID">
					<option value="">GroupType ID</option>
					<?php
					//	Fill in the Options to choose from 
					foreach ($UserTypesARR as $nxREC): 
					?>
					<option name="<?= $nxREC['UserTypesCD'] ?>" 
						value="<?= $nxREC['UserTypesID'] ?>" 
						<?= set_select('UserTypesID', 
								$nxREC['UserTypesID'],
								 ((!empty($userREC) &&
								  $userREC['UserGroup'] == $nxREC['UserTypesID']) ? TRUE : '')) ?>>
								  <?= $nxREC['UserTypesShortName'] ?>
								  (<?= $nxREC['UserTypesCD'] ?>)
					 </option>
					<?php endforeach ?>
					</select>
			</div>
		</div>
		 


		 
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M User Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


				<div class="row">
		<div class="col-sm-4">

					<label for="UserDesc">User Description (500 digits)</label>

		</div>
		<div class="col-sm-8">

			<textarea name="UserDesc" rows="5" columns="80" style="width:600px; height: 120px;">
			<?= set_value('UserDesc',
			 (!empty($userREC) ? $userREC['UserDesc'] : '')) ?></textarea>
			<script>
			 CKEDITOR.replace( 'UserDesc', {
				toolbar: [
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
				]
				});
		</script>

		</div>
	</div>



		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($userREC))
					 ? 'Update' : 'Create' ?> User</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




