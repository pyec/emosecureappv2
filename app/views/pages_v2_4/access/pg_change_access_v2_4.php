<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_change_access_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-05
 * 
 * 		Cahnge Access for EMO Access.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-accesss";
$link_search				= "search-accesss";

?>
<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($ActiveStatesARR);
// echo "</pre>";


?>


<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($accessREC)) 
				{
				echo form_open('create-access');
				}
			else
				{
				echo form_open('update-access');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Access ID;
			if(!empty($accessREC)) 
			         echo form_hidden('AID',$accessREC['AID']);
			         ?>
 
  				<h1>
 				 <?= (!empty($accessREC)) ? 'Edit' : 'Create' ?> Access</button>
                </h1>              
                                
                <?php 
//				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="AID">Access Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['AID']))
							print $accessREC['AID'];
						else 
							print "--";	
							?>
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="CID">Category</label>
			</div>
			<div class="col-sm-8">			
					<select class="form-control" id="CID" name="CID">
					<option value="">Category Choice</option>
					<?php
					//	Fill in the Options to choose from 
					foreach ($CategoriesARR as $nxREC)
					{
						
					 if($accessREC['CID'] == $nxREC['CID'])
					 	{  
					 		$sel_state  = true;
					 		$sel_stuff = ' selected="selected" ';
					 	 }
					 else
					 	{  
					 		$sel_state  = false;
					 		$sel_stuff = '';
					 	}
					 	
					 $baseST = '<option name="'. $nxREC['CID'] . '" value="' . $nxREC['CID'] . '"' . $sel_stuff . '>';
					 print $baseST;	
					 	
					 $optionST = $nxREC['Name'] . " (" . $nxREC['CID'] . ") " . '</option>';
					 print $optionST;

					 set_select('CID', $nxREC['CID'],$sel_state);
					 
					} 
					?>
					
					</select>
					
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="UID">User</label>
			</div>
			<div class="col-sm-8">			
					<select class="form-control" id="UID" name="UID">
					<option value="">User Choice</option>
					<?php
					//	Fill in the Options to choose from 
					foreach ($UsersARR as $nxREC)
					{
						
					 if($accessREC['UID'] == $nxREC['UID'])
					 	{  
					 		$sel_state  = true;
					 		$sel_stuff = ' selected="selected" ';
					 	 }
					 else
					 	{  
					 		$sel_state  = false;
					 		$sel_stuff = '';
					 	}
					 	
					 $baseST = '<option name="'. $nxREC['UID'] . '" value="' . $nxREC['UID'] . '"' . $sel_stuff . '>';
					 print $baseST;	
					 	
					 $optionST = $nxREC['FirstName'] . " " . $nxREC['LastName'] .  " (" . $nxREC['UID'] . ") " . '</option>';
					 print $optionST;

					 set_select('UID', $nxREC['UID'],$sel_state);
					 
					} 
					?>
					
					</select>
					
			</div>
		</div>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	YesNo
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Activity State</label>
			</div>
			<div class="col-sm-8">			
					<select class="form-control" id="ActiveStatesID" name="ActiveStatesID">
					<option value="">Active State</option>
					<?php
					//	Fill in the Options to choose from 
					foreach ($ActiveStatesARR as $nxREC): 
					?>
					<option name="<?= $nxREC['ActiveStatesCD'] ?>" 
						value="<?= $nxREC['ActiveStatesID'] ?>" 
						<?= set_select('ActiveStatesID', 
								$nxREC['ActiveStatesID'],
								 ((!empty($accessREC) &&
								  $accessREC['Active'] == $nxREC['ActiveStatesID']) ? TRUE : '')) ?>>
								  <?= $nxREC['ActiveStatesShortName'] ?>
								  (<?= $nxREC['ActiveStatesCD'] ?>)
					 </option>
					<?php endforeach ?>
					</select>
			</div>
		</div>

		 
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


				<div class="row">
		<div class="col-sm-4">

					<label for="AccessDesc">Access Description (500 digits)</label>

		</div>
		<div class="col-sm-8">

			<textarea name="AccessDesc" rows="5" columns="80" style="width:600px; height: 120px;">
			<?= set_value('AccessDesc',
			 (!empty($accessREC) ? $accessREC['AccessDesc'] : '')) ?></textarea>
			<script>
			 CKEDITOR.replace( 'AccessDesc', {
				toolbar: [
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
				]
				});
		</script>

		</div>
	</div>

				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($accessREC))
					 ? 'Update' : 'Create' ?> Access</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




