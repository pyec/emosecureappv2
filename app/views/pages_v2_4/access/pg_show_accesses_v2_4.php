<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_show_accesses_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the EMO Accesss.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "show-accesses";
$link_delete_entry			= "delthis-access";
$link_disable_entry			= "disable-access";
$link_enable_entry			= "enable-access";
$link_add_entry				= "add-access";
$link_update				= "display-access";
$link_modify_rec			= "change-access";
$link_search				= "search-access";
?>


<div class="row">
	<div class="col-md-12">
	<div class="col-md-12">
		<h2>Access Listings</h2>
	</div>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>


<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="Access"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>
	</div>
</div>



<?php if (!empty($accessesARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Record #</th>
				<th nowrap>Category</th>
				<th nowrap>User Name</th>
				<th nowrap>ActiveYN</th>
				
				<th nowrap>Last Mod</th>
<?php 
	if($roleREC['ModifyAccessYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>

<?php 
	if($roleREC['DisableAccessYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>

<?php 
	if($roleREC['DeleteAccessYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>			

	</thead>
	<tbody class="tbody">
	
	
	<?php foreach ($accessesARR as $workREC): ?>
						<tr>
	
<?php 
	if($roleREC['detailAccessYN'] == 'Y'):
?>
		<td>
				<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['AID'] ?>">
			   	<?= $workREC['AID'] ?></a>
		</td>
		
<?php else: ?>
		<td>
			   	<?= $workREC['AID'] ?>
		</td>
<?php endif; ?>							
							
							
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($workREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Category N/A";		
					$wrkValue = $wrkValue . "(" . $workREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		</td>



		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UsersARR as $nxREC)
					{ 
						if($workREC['UID'] == $nxREC['UID'])
						{
						if(!empty($nxREC['FirstName']))
							$wrkValue = $nxREC['FirstName'] . " " . $nxREC['LastName'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}
					if($wrkValue == '')
						 $wrkValue = "User N/A";		 
					$wrkValue = $wrkValue . "(" . $workREC['UID'] . ")";	  
					 print $wrkValue;
					?>
			</div>
		</div>
		</td>

		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	ActiveYN Selection
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($workREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
					print $wrkValue;
					?>
		</td>


		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	LastMod
		 * -----------------------------------------------------PRSC 201603
		 */	?>
				<?= $workREC['LastModDate']  ?>
		</td>


<?php 
	if($roleREC['ModifyAccessYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['AID'] ?>">
		   	Modify</a>
		</td>
<?php endif ?>


<?php 
	if($roleREC['DisableAccessYN'] == 'Y'):
?>
	<?php 
		if($workREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_entry; print "/" . $workREC['AID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="AID" value="<?= $workREC['AID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_entry; print "/" . $workREC['AID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="AID" value="<?= $workREC['AID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							


			
<?php 
	if($roleREC['DeleteAccessYN'] == 'Y'):
?>
		<td>
			<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['AID'] ?>" method="POST" >
			<button type="submit" class="btn btn-sm btn-danger">
			DEL
			</button>
			<input type="hidden" name="AID" value="<?= $workREC['AID'] ?>" />
			</form>
		</td>
<?php endif ?>		
		</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>
		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End Access
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h4><i>No Data Matching Search Criteria Found</i></h4>
				<center>
		</div>		

<?php endif ?>


<?php 
	if($roleREC['AddAccessYN'] == 'Y'):
?>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?><?php  print $link_add_entry ?>" class="btn btn-success pull-right">Add Access</a>
	</div>
</div>

<?php endif ?>

