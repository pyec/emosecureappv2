<?php
/*===================================================================================
 * 
 * 		MODULE: 	pg_deletethis_access_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Delete the EMO Accesss for specifed record.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-accesses";

$link_main_page 			= "show-accesses"; 
?>
<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($accessREC);
// echo "</pre>";

// print $accessREC['Name'];
 

?>

<?php 
	echo form_open('deleteit-access');
	
	echo form_hidden('LastModBy',$this->session->userdata('AccessName'));
	if(!empty($accessREC)) 
		    echo form_hidden('AID',$accessREC['AID']);
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Access Info [Delete Screen]
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="AID">Access Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['AID']))
							print $accessREC['AID'];
						else 
							print "--";	
							?>
					</div>
				</div>



		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<div class="row">
			<div class="col-sm-4">
						<label for="CategoryID">Category ID</label>
			</div>
			<div class="col-sm-8">			


					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($accessREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $accessREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		


		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
		<div class="row">
			<div class="col-sm-4">
						<label for="UserID">User ID</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UsersARR as $nxREC)
					{ 
						if($accessREC['UID'] == $nxREC['UID'])
						{
						if(!empty($nxREC['FirstName']))
							$wrkValue = $nxREC['FirstName'] . " " . $nxREC['LastName'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}
					if($wrkValue == '')
						 $wrkValue = "Defunct";		 
					print $wrkValue;
					?>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($accessREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $accessREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		
		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By  
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModBy">Last Mod By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['LastModBy']))
							print $accessREC['LastModBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModDate">Last Mod Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['LastModDate']))
							print $accessREC['LastModDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
				
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created By  
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedBy">Created By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['CreatedBy']))
							print $accessREC['CreatedBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedDate">Created Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['CreatedDate']))
							print $accessREC['CreatedDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

				

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="AccessDesc">Access Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($accessREC['AccessDesc']))
					print $accessREC['AccessDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>



		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="YES" class="btn btn-info" style="background:red">
			Yes - Please Delete This
		 	</button>
			<button type="submit" name="choice" value="NO" class="btn btn-info">
			No
			</button>
	
			</form>
 		 	
		</div>
		
	 </div>
		
	</div>

</div>
</div>
</div>
</form>



