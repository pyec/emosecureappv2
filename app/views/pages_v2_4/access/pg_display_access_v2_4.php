<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_display_access_full_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 		This is also meant to show all the users records with Access
 * 		to this Access by CID.
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-04		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-accesses";

?>

<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($accessREC);
// echo "</pre>";

// print $accessREC['Name'];
 
 
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Access
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="AID">Access Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['AID']))
							print $accessREC['AID'];
						else 
							print "--";	
							?>
					</div>
				</div>



		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<div class="row">
			<div class="col-sm-4">
						<label for="CategoryID">Category </label>
			</div>
			<div class="col-sm-8">			


					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($accessREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Category N/A";		
					$wrkValue = $wrkValue . "(" . $accessREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		


		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
		<div class="row">
			<div class="col-sm-4">
						<label for="UserID">User Name</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UsersARR as $nxREC)
					{ 
						if($accessREC['UID'] == $nxREC['UID'])
						{
						if(!empty($nxREC['FirstName']))
							$wrkValue = $nxREC['FirstName'] . " " . $nxREC['LastName'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}
					if($wrkValue == '')
						 $wrkValue = "User N/A";		 
					$wrkValue = $wrkValue . "(" . $accessREC['UID'] . ")";	  
					print $wrkValue;
					
					?>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($accessREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $accessREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>


		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModBy">Last Mod By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['LastModBy']))
							print $accessREC['LastModBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Last Mod By Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="LastModDate">Last Mod Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['LastModDate']))
							print $accessREC['LastModDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>


	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created By  
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedBy">Created By </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['CreatedBy']))
							print $accessREC['CreatedBy'];
						else 
							print "N/A";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Created Date 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="CreatedDate">Created Date</label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['CreatedDate']))
							print $accessREC['CreatedDate'];
						else 
							print "N/A";	
							?>
					</div>
				</div>





	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="AccessDesc">Access Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($accessREC['AccessDesc']))
					print $accessREC['AccessDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

	if(!empty($response_page))
		{
		$link_back = $response_page;
		$user_choiceLIT		= "Go Back to User Page";	
		}
	else 
		$user_choiceLIT		= "Go Back To List";	
    echo form_open($link_back);
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 <?php print $user_choiceLIT ?>
			</button>
	
			</form>
 		 	
		</div>
	</div>
		
	</div>

</div>
</div>
</div>




