<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_change_access_limited_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-05
 * 
 * 		This allows limited changing of a created Access Record.
 *		Once a record is created for a Category and associated to a person,
 *		you cannot change the access who and where.  You should only be able
 *		to update the Desc and the Active YN.
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-accesss";
$link_search				= "search-accesss";

?>
<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($ActiveStatesARR);
// echo "</pre>";


?>


<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($accessREC)) 
				{
				echo form_open('create-access');
				}
			else
				{
				echo form_open('update-access');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Access ID;
			if(!empty($accessREC)) 
			         echo form_hidden('AID',$accessREC['AID']);
			         echo form_hidden('CID',$accessREC['CID']);
			         echo form_hidden('UID',$accessREC['UID']);
			         
			         ?>
 
  				<h1>
 				 <?= (!empty($accessREC)) ? 'Edit' : 'Create' ?> Access</button>
                </h1>              
                                
                <?php 
//				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="AID">Access Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($accessREC['AID']))
							print $accessREC['AID'];
						else 
							print "--";	
							?>
					</div>
				</div>



		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<div class="row">
			<div class="col-sm-4">
						<label for="CategoryID">Category ID</label>
			</div>
			<div class="col-sm-8">			


					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($accessREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $accessREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		


		
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	User Name
		 * -----------------------------------------------------PRSC 201603
		 */	?>
		<div class="row">
			<div class="col-sm-4">
						<label for="UserID">User ID</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($UsersARR as $nxREC)
					{ 
						if($accessREC['UID'] == $nxREC['UID'])
						{
						if(!empty($nxREC['FirstName']))
							$wrkValue = $nxREC['FirstName'] . " " . $nxREC['LastName'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}
					if($wrkValue == '')
						 $wrkValue = "Defunct";		 
					print $wrkValue;
					?>
			</div>
		</div>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active - YesNo
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStateID">Activity State</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="ActiveStatesID" name="ActiveStatesID">
					<?php
					//	Fill in the Options to choose from 
					foreach ($ActiveStatesARR as $nxREC)
					{
						
					 if($accessREC['Active'] == $nxREC['ActiveStatesID'])
					 	{  
					 		$sel_state  = true;
					 		$sel_stuff = ' selected="selected" ';
					 	 }
					 else
					 	{  
					 		$sel_state  = false;
					 		$sel_stuff = '';
					 	}
					
					
					 $baseST = '<option name="'. $nxREC['ActiveStatesID'] . '" value="' . $nxREC['ActiveStatesCD'] . '"' . $sel_stuff . '>';
					 print $baseST;	
					 	
					 $optionST = $nxREC['ActiveStatesShortName'] . " (" . $nxREC['ActiveStatesCD'] . ") " . '</option>';
					 print $optionST;

					 set_select('Active', $nxREC['ActiveStatesID'],$sel_state);
					}
					?>
					
					</select>
			</div>
		</div>

		 
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Access Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


				<div class="row">
		<div class="col-sm-4">

					<label for="AccessDesc">Access Description (500 digits)</label>

		</div>
		<div class="col-sm-8">

			<textarea name="AccessDesc" rows="5" columns="80" style="width:600px; height: 120px;">
			<?= set_value('AccessDesc',
			 (!empty($accessREC) ? $accessREC['AccessDesc'] : '')) ?></textarea>
			<script>
			 CKEDITOR.replace( 'AccessDesc', {
				toolbar: [
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
				]
				});
		</script>

		</div>
	</div>

				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($accessREC))
					 ? 'Update' : 'Create' ?> Access</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




