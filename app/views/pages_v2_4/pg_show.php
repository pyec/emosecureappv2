<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_showPDF_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-18		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "user-sigin";
$link_signin				= "user-signin";
$link_reqnew				= "user-requestnew";
$link_forgot_password		= "user-forgot-password";


/*  Add Assets translations here as well - PRSC TODO */

$asset_bootstrap 	= "_assets/plugins/bootstrap/css/bootstrap.min.css";
$asset_fonts		= "_assets/plugins/font-awesome/css/font-awesome.min.css";
$asset_app_css		= "_assets/app.css";

?>
 <?php 
//       print "TARGET FILE [" . $targfile . "]";
//       echo $targfile  . '<br>';
    
//		header("Content-type: application/pdf");
//		header("Content-Length: " . filesize($targfile));

//		readfile($targfile);
  
 		$fstuff = array();
 	 	$myfile = fopen($targfile,"rb");
//		$fstuff = $this->get_file_info($myfile,size);

		echo "Files Size = ".filesize($targfile) . "<br>";   	 	
 	 	

// 	 	echo fread($myfile,filesize($targfile));      
    	fclose($myfile);

		
//	copy($targfile, $destfile);		
	
?>
