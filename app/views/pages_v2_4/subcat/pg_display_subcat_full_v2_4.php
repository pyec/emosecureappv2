<<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_display_subcat_full_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 *		This displays subcat and its associated documents.
  * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-06		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-subcats";

?>

<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($subcatREC);
// echo "</pre>";

// print $subcatREC['SubcatShortName'];
 
 
?>


		<div class="col-sm-12">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Sub-Category
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SCID">Subcat Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SCID']))
							print $subcatREC['SCID'];
						else 
							print "--";	
							?>
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat CODE 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SubcatCD">Subcat Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SubcatCD']))
							print $subcatREC['SubcatCD'];
						else 
							print "N/A";	
							?>
					</div>
				</div>


	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SubcatShortName">Subcat Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SubcatShortName']))
							print $subcatREC['SubcatShortName'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
			
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<div class="row">
			<div class="col-sm-4">
						<label for="CID">Associated Category</label>
			</div>
			<div class="col-sm-8">			


					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($subcatREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $subcatREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($subcatREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $subcatREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>


	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="SubcatDesc">Subcat Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($subcatREC['SubcatDesc']))
					print $subcatREC['SubcatDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open('show-subcats');
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to Sub-Category List
			</button>
	
			</form>
 		 	
		</div>
				
				
			
			</div>
		
	</div>

</div>
</div>

<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_show_subcats_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the EMO Subcats.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "show-subcats";
$link_delete_entry			= "delthis-subcat";
$link_disable_entry			= "disable-subcat";

$link_add_entry				= "add-subcat";
$link_update				= "display-subcat";
$link_modify_rec			= "change-subcat";
$link_search				= "search-subcat";
?>


<div class="row">
	<div class="col-md-12">
	<div class="col-md-12">
		<h3>Other Sub-Categories in Parent Category</h3>
	</div>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>


<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="Subcat"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>
	</div>
</div>



<?php if (!empty($subcatsARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Record #</th>
				<th nowrap>Subcat Code</th>
				<th nowrap>Subcat Name</th>
				<th nowrap>Category #</th>
<?php 
	if($roleREC['ModifySubcatsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>

<?php 
	if($roleREC['DisableSubcatsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>

<?php 
	if($roleREC['DeleteSubcatsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>
			</thead>
			<tbody class="tbody">
					<?php foreach ($subcatsARR as $workREC): ?>
						<tr>
						
<?php 
	if($roleREC['detailSubcatsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['SCID'] ?>">
		   	<?= $workREC['SCID'] ?></a>
		</td>
<?php else: ?>
		<td>
		   	<?= $workREC['SCID'] ?>
		</td>
<?php  endif ?>							
							<td>
								<?= $workREC['SubcatCD']  ?>
							</td>
							<td>
								<?= $workREC['SubcatShortName']  ?>
							</td>
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($workREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $workREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		</td>
							
<?php 
	if($roleREC['ModifySubcatsYN'] == 'Y'):
?>
	
							<td>
								<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['SCID'] ?>">
							   	Modify</a>
							</td>
<?php endif; ?>

<?php 
	if($roleREC['DisableSubcatsYN'] == 'Y'):
?>
							<td>
								<form action="<?php echo base_url(); print $link_disable_entry; print "/" . $workREC['SCID'] ?>" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
									DISABLE
									</button>
									<input type="hidden" name="SCID" value="<?= $workREC['SCID'] ?>" />
								</form>
							</td>
<?php endif ?>							

			
<?php 
	if($roleREC['DeleteSubcatsYN'] == 'Y'):
?>
							<td>
								<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['SCID'] ?>" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger">
									DEL
									</button>
									<input type="hidden" name="SCID" value="<?= $workREC['SCID'] ?>" />
								</form>
							</td>
<?php endif ?>							
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>
		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h4><i>No Matching Sub-Categories Found</i></h4>
				<center>
		</div>		

<?php endif ?>


<?php 
	if($roleREC['AddSubcatsYN'] == 'Y'):
?>

	<div class="row">
		<div class="col-md-12">
		<a href="<?php echo base_url(); ?><?php  print $link_add_entry ?>" class="btn btn-success pull-right">Add Subcat</a>
		</div>
	</div>

<?php endif ?>

