<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_update_subcat_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 *		This allows modifying an existing subcat Record.
  * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-06		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-subcats";

?><!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($subcatREC);
// echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($subcatREC)) 
				{
				echo form_open('create-subcat');
				}
			else
				{
				echo form_open('update-subcat');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('Email'));
			// if this is an update to set the Subcat ID;
			if(!empty($subcatREC)) 
			         echo form_hidden('SCID',$subcatREC['SCID']);
			?>
 
  				<h1>
 				 <?= (!empty($subcatREC)) ? 'Edit' : 'Create' ?> Sub-Category</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SCID">Subcat Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SCID']))
							print $subcatREC['SCID'];
						else 
							print "--";	
							?>
					</div>
				</div>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat Code 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SubcatCD">Subcat Letter Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SubcatCD']))
							print $subcatREC['SubcatCD'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="Name">Subcat Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="Name" class="form-control"
						 value="<?= set_value('SubcatShortName', (!empty($subcatREC) ?
						  $subcatREC['SubcatShortName'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<div class="row">
			<div class="col-sm-4">
						<label for="CID">Associated Category</label>
			</div>
			<div class="col-sm-8">			


					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($subcatREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $subcatREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($subcatREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $subcatREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
			

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


				<div class="row">
		<div class="col-sm-4">

					<label for="SubcatDesc">Subcat Description (500 digits)</label>

		</div>
		<div class="col-sm-8">

			<textarea name="SubcatDesc" rows="5" columns="80" style="width:600px; height: 120px;">
			<?= set_value('SubcatDesc',(!empty($subcatREC) ? $subcatREC['SubcatDesc'] : '')) ?>
			</textarea>
			<script>
			 CKEDITOR.replace( 'SubcatDesc', {
				toolbar: [
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
				]
				});
		</script>

		</div>
	</div>



		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($subcatREC))
					 ? 'Update' : 'Create' ?> Sub-Category</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




