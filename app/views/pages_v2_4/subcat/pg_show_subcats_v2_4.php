<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_show_subcats_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-29
 * 
 * 		Display all the EMO Subcats.  
 * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 * 
 *===================================================================================
 */

/* Internal Define overrides */


$link_back					= "show-subcats";
$link_delete_entry			= "delthis-subcat";
$link_disable_entry			= "disable-subcat";
$link_enable_entry			= "enable-subcat";
$link_add_entry				= "add-subcat";
$link_update				= "display-subcat";
$link_modify_rec			= "change-subcat";
$link_search				= "search-subcat";
?>


<div class="row">
	<div class="col-md-12">
	<div class="col-md-12">
		<h3>Sub-Categories</h3>
	</div>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" 
					aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>


<?php 
/*---------------------------------------------------------------
 * 		Search Form when installed goes here.  -PRSC
 * --------------------------------------------------------------
 */

?>

	<input type="hidden" class="survey_name" value="Subcat"  />
	<form action="<?php echo base_url(); echo $link_search ?>" method="POST">


    </form>
	</div>
</div>



<?php if (!empty($subcatsARR)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th nowrap>Subcat ID</th>
				<th nowrap>Subcat Code</th>
				<th nowrap>Subcat Name</th>
				<th nowrap>Category Name</th>
<?php 
	if($roleREC['ModifySubcatsYN'] == 'Y'):
?>
				<th>Modify</th>
<?php endif; ?>

<?php 
	if($roleREC['DisableSubcatsYN'] == 'Y'):
?>
				<th>Disable</th>
<?php endif; ?>

<?php 
	if($roleREC['DeleteSubcatsYN'] == 'Y'):
?>
				<th>Delete</th>
<?php endif; ?>
			</thead>
			<tbody class="tbody">
					<?php foreach ($subcatsARR as $workREC): ?>
						<tr>
						
<?php 
	if($roleREC['detailSubcatsYN'] == 'Y'):
?>
		<td>
			<a href="<?= base_url(); print $link_update; ?>/<?= $workREC['SCID'] ?>">
		   	<?= $workREC['SCID'] ?></a>
		</td>
<?php else: ?>
		<td>
		   	<?= $workREC['SCID'] ?>
		</td>
<?php  endif ?>							
							<td>
								<?= $workREC['SubcatCD']  ?>
							</td>
							<td>
								<?= $workREC['SubcatShortName']  ?>
							</td>
		<td>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($workREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $workREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>
		</td>
							
<?php 
	if($roleREC['ModifySubcatsYN'] == 'Y'):
?>
	
							<td>
								<a href="<?= base_url(); print $link_modify_rec; ?>/<?= $workREC['SCID'] ?>">
							   	Modify</a>
							</td>
<?php endif; ?>

<?php 
	if($roleREC['DisableSubcatsYN'] == 'Y'):
?>

	<?php 
		if($workREC['Active'] == 2):
	?>
			<td>
				<form action="<?php echo base_url(); print $link_disable_entry; print "/" . $workREC['SCID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(222,160,0)">
					DISABLE
					</button>
					<input type="hidden" name="SCID" value="<?= $workREC['SCID'] ?>" />
				</form>
			</td>
	<?php else: ?>
			<td>
				<form action="<?php echo base_url(); print $link_enable_entry; print "/" . $workREC['SCID'] ?>" method="POST" >
					<button type="submit" class="btn btn-sm btn-danger" style="background:rgb(0,160,0)">
					ENABLE
					</button>
					<input type="hidden" name="SCID" value="<?= $workREC['SCID'] ?>" />
				</form>
			</td>
	
			
	<?php endif;						// EO Disable/Enable Type Display ?>		
<?php endif; 							// EO If allow Disable YN?>							
			
<?php 
	if($roleREC['DeleteSubcatsYN'] == 'Y'):
?>
							<td>
								<form action="<?php echo base_url(); print $link_delete_entry; print "/" . $workREC['SCID'] ?>" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger">
									DEL
									</button>
									<input type="hidden" name="SCID" value="<?= $workREC['SCID'] ?>" />
								</form>
							</td>
<?php endif ?>							
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>	
</div>
		<?php 
		/*------------------------------------------------
		 * 	No Data Screen Display to End User
		 * ------------------------------------------PRSC-
		 */
		else: ?>
		<div class="row">
				<center>
				<h4><i>No Matching Sub-Categories Found</i></h4>
				<center>
		</div>		

<?php endif ?>


<?php 
	if($roleREC['AddSubcatsYN'] == 'Y'):
?>

	<div class="row">
		<div class="col-md-12">
		<a href="<?php echo base_url(); ?><?php  print $link_add_entry ?>" class="btn btn-success pull-right">Add Subcat</a>
		</div>
	</div>

<?php endif ?>