<<?php 
/*===================================================================================
 * 
 * 		MODULE: 	pg_display_subcat_v2_4.php
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-04
 * 
 *		This displays subcat and its associated documents.
  * 
 * ---------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-06		PRSC	Revised from regular display to show users.
 * 
 *===================================================================================
 */

/* Internal Define overrides */

$link_back					= "show-subcats";

?>

<!-- Page -->
<div id="page" style="padding:5px 0 20px;">
	<div class="container">
	<div class="row">

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($subcatREC);
// echo "</pre>";

// print $subcatREC['SubcatShortName'];
 
 
?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			?>
 
  				<h1>
 				  Sub-Category
                </h1>              
                                
                <?php 
                if(!empty($data_state))
				 	print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
 
 				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat ID 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SCID">Subcat Number Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SCID']))
							print $subcatREC['SCID'];
						else 
							print "--";	
							?>
					</div>
				</div>
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat Code 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SubcatCD">Subcat Letter Code </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SubcatCD']))
							print $subcatREC['SubcatCD'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SubcatShortName">Subcat Name </label>
					</div>
					<div class="col-sm-8">
						<?php 
						if(!empty($subcatREC['SubcatShortName']))
							print $subcatREC['SubcatShortName'];
						else 
							print "N/A";	
							?>
					</div>
				</div>
			
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Category
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<div class="row">
			<div class="col-sm-4">
						<label for="CID">Associated Category</label>
			</div>
			<div class="col-sm-8">			


					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($CategoriesARR as $nxREC)
					{ 
						if($subcatREC['CID'] == $nxREC['CID'])
						{
						if(!empty($nxREC['Name']))
							 $wrkValue = $nxREC['Name'];
						else 
							 $wrkValue = "Unlisted";		 
						}	
					}					
					if($wrkValue == '')
						 $wrkValue = "Defunct";		
					$wrkValue = $wrkValue . "(" . $subcatREC['CID'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Active State
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		 
		<div class="row">
			<div class="col-sm-4">
						<label for="ActiveStatesID">Active State</label>
			</div>
			<div class="col-sm-8">			
					<?php
					//	Fill in the Options to choose from 
					$wrkValue		= '';
					
					foreach ($ActiveStatesARR as $nxREC)
					{ 
						if($subcatREC['Active'] == $nxREC['ActiveStatesCD'])
						{
						$wrkValue = $nxREC['ActiveStatesShortName'];
						}	
					}					
//					$wrkValue = $wrkValue . "(" . $subcatREC['Active'] . ")";	  
					print $wrkValue;
					?>
			</div>
		</div>


	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Subcat Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>


		<div class="row">
		<div class="col-sm-4">

					<label for="SubcatDesc">Subcat Description </label>

		</div>
		<div class="col-sm-8">
				<?php 
				if(!empty($subcatREC['SubcatDesc']))
					print $subcatREC['SubcatDesc'];
				else 
					print "N/A";	
				?>

		</div>
	</div>

				
<?php 
   // Poor mans way to put a controlled link back to the main page with security.
	// Add hidden field to confirm user ID	

    echo form_open('show-subcats');
?>
				
		<div class="text-center" style="padding:15px 0;">
			<button type="submit" name="choice" value="HOME" class="btn btn-info">
			<i class="icon-ok icon-white"></i>
			 Go Back to List
			</button>
	
			</form>
 		 	
		</div>
				
				
			
			</div>
		
	</div>

</div>
</div>
</div>

