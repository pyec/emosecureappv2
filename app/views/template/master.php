<?php
	// Display the page if a valid session exists and the requested page is not the login page.
	// Otherwise redirect to the login page.
	if($page != 'pages_v2_4/pg_signin_v2_4') 
	{
		// $this->load->view('template/header', $data);
		$this->load->view('template/header_v2_4');
		$this->load->view($page);
		$this->load->view('template/footer');
	}

	// elseif($this->session->userdata('UserName') && $page != 'pages/login') 
	// {
	// 	// $this->load->view('template/header', $data);
	// 	$this->load->view('template/header');
	// 	// $this->load->view('template/menu-top', $data);
	// 	$this->load->view('template/menu-top');
	// 	$this->load->view($page);
	// 	$this->load->view('template/footer');
	// }
	
	else
	{
		// $this->session->set_flashdata('error', 'You must be logged in to see this content.');
		// $this->load->view('template/header');
		$this->load->view('pages_v2_4/pg_signin_v2_4');
		// $this->load->view('template/footer');
	}
 ?>