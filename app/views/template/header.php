<!DOCTYPE html>
<html lang="en">

	<head>
	
		<title>EMO Secure Documents App</title>
		
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/datetimepicker/jquery.datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/app.css">

		<!-- <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'> -->
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

		<script>
			// $(document).ready(function () {
			// });	
		</script>
		
	</head>
	
	<body>
		<a name="back_to_top"></a>
		
		<!-- <div class="navbar navbar-inverse navbar-fixed-top"> -->
		<div class="navbar navbar-default">
	        <div class="container">
	         	<div class="navbar-header">
					<?php // if($this->session->userdata('UserName')): ?>
					<a type="button" class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars" style="color:white"></i></a>
					<!-- <a type="button" class="btn navbar-toggle back_to_top" href="#back_to_top" style="color:white"><i class="fa fa-arrow-up"></i></a> -->
					<?php // endif ?>
					<a class="navbar-brand" href="<?= base_url() ?>">
						<img class="pull-left" src="<?= base_url() ?>_assets/images/EMO_Logo-sm.jpg">
						<div class="pull-left" style="padding:0.45em 0 0 0.25em; color:#286090;font-size:1.25em; line-height:1.1em">
							<p>EMO Secure App</p>
						</div>
					</a>
				</div>
				<?php //if($this->session->userdata('UserName')): ?>
				<div>
					<div class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">
							<!-- <li class="hidden-xs"><a href="#back_to_top" class="back_to_top" title="Back to top">Back to Top</a></li> -->
							<li><a href="<?= base_url() ?>show-categories">Categories</a></li>

							<?php //if($this->session->userdata('UserAdminFlag')): ?>
							<li>
								<a href="<?= base_url() ?>show-subcats">Sub-Cats</a>
							</li>
							<?php //endif ?>


							<li><a href="<?= base_url() ?>show-documents">Documents</a></li>

							<?php //if($this->session->userdata('UserAdminFlag')): ?>
							<li>
								<a href="<?= base_url() ?>show-users">Users</a>
							</li>
								
							<?php //if($this->session->userdata('UserAdminFlag')): ?>
							<li>
								<a href="<?= base_url() ?>show-accesses">Access</a>
							</li>
							<?php //endif ?>

							<!-- <li><a href="<?= base_url() ?>logout">Logout <?= $this->session->userdata('UserNameShort') ?> (<?= $this->session->userdata('UserBusinessUnitCode') ?><?= $this->session->userdata('UserAdminFlag') ? '/Admin' : '' ?>)</a></li> -->
							<li><a href="<?php base_url() ?>">Logout</a></li>
							<!-- <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gian Pompilio <i class="fa fa-caret-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="<?php base_url() ?>">Profile</a></li>
									<li class="divider"></li>
									<li><a href="<?php base_url() ?>">Logout</a></li>
								</ul>									
							</li> -->
						</ul>

					</div>
				</div>
				<?php //endif ?>
				<!--/.nav-collapse -->
	        </div>
	    </div>