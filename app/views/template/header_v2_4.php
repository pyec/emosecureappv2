<?php 
/*===========================================================
 * 
 * 		MODULE:		header_v2_4.php
 * 		AUTHOR:		Stephen Chafe - Zen River Software
 * 
 * ----------------------------------------------------------
 * 
 *  	MODIFICATION HISTORY
 * 
 * 		20170612	PRSC	Added roleREC testing to base cd
 * 
 * 
 * ----------------------------------------------------------
 */

	// Set up Link References PRSC
	$link_logout			= "user-logout";
	$link_home				= "user-profile";


	if(empty($roleREC))
	{
		print "WARNING - role not found ";
			{				// TODO - This needs to be redirected to login PRSC
				return -1;
			}
	}		


?>

<!DOCTYPE html>
<html lang="en">

	<head>
	
		<title>EMO Secure Documents App</title>
		
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/datetimepicker/jquery.datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/app.css">

		<!-- <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'> -->

		<script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/jquery/jquery-ui-1.9.2.custom.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/ckeditor/ckeditor.js"></script>
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

		<script>
			// $(document).ready(function () {
			// });	
		</script>
		
	</head>
	
	
	
	
	<body>
		<a name="back_to_top"></a>
		
		<!-- <div class="navbar navbar-inverse navbar-fixed-top"> -->
		<div class="navbar navbar-default">
	        <div class="container">
	         	<div class="navbar-header">
					<?php // if($this->session->userdata('UserName')): ?>
					<a type="button" class="btn navbar-toggle" data-toggle="collapse" 
						data-target=".navbar-collapse"><i class="fa fa-bars" style="color:white"></i></a>
					<!-- <a type="button" class="btn navbar-toggle back_to_top" 
							href="#back_to_top" style="color:white"><i class="fa fa-arrow-up"></i></a> -->
					<?php // endif ?>
					<a class="navbar-brand" href="<?= base_url() ; print $link_home;?>">
						<img class="pull-left" src="<?= base_url() ?>_assets/images/EMO_Logo-sm.jpg">
						<div class="pull-left" style="padding:0.45em 0 0 0.25em; 
											color:#286090;font-size:1.25em; line-height:1.1em">
							<p>EMO Secure App</p>
						</div>
					</a>
				</div>
				
				<?php //if($this->session->userdata('UserName')): ?>
				<div>
					<div class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">
							<!-- <li class="hidden-xs">
							<a href="#back_to_top" class="back_to_top" 
										title="Back to top">Back to Top</a></li> -->
										

<?php if($roleREC['viewCatsMU'] == 'Y'): ?>
										
							<li><a href="<?= base_url() ?>show-categories">Categories</a></li>
<?php endif ?>

<?php if($roleREC['viewSubcatsMU'] == 'Y'): ?>
							<?php //if($this->session->userdata('UserAdminFlag')): ?>
							<li>
								<a href="<?= base_url() ?>show-subcats">Sub-Cats</a>
							</li>
							<?php //endif ?>
<?php endif?>

<?php if($roleREC['viewDocsMU'] == 'Y'): ?>

							<li><a href="<?= base_url() ?>show-documents">Documents</a></li>
<?php endif ?>

<?php if($roleREC['viewUsersMU'] == 'Y'): ?>
							<?php //if($this->session->userdata('UserAdminFlag')): ?>
							<li>
								<a href="<?= base_url() ?>show-users">Users</a>
							</li>
<?php endif ?>	

<?php if($roleREC['viewAccessMU'] == 'Y'): ?>
							
							<?php //if($this->session->userdata('UserAdminFlag')): ?>
							<li>
								<a href="<?= base_url() ?>show-accesses">Access</a>
							</li>
							<?php //endif ?>
<?php endif ?>


			<li>
				<a href="<?= base_url(); print $link_logout ?>">Logout - 
				<?php print $sessionUserREC["userNAME"]; ?> - ( <?php print $roleREC['name']; ?> )</a>
			
			</li>
							
							
			</ul>
			</div>
			</div>
			<?php //endif ?>
				<!--/.nav-collapse -->
	        </div>
	    
<?php 
/*------------------------------------------------------------------
 * 			Name and ID stuff
 * -----------------------------------------------------------------
 */
?>



	    
	    