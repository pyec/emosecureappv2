<?php //$this->session->set_flashdata('uri', uri_string()) ?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>

<!-- Page -->
<div id="page">
	<div class="container">
		<div class="row">
			<p><strong>Reporting</strong></p>
			<p>A combination of hours and shifts functionality from the older application.</p>
			<hr>
			<p><strong>Hours</strong></p>
			<p>Enables administrators to produce reports on volunteer hours by category.</p>
			<ul>
				<li>Abilty to filter by: Section, date/time range</li>
				<li>Ability to download results as CSV</li>
				<li></li>
				<li></li>
			</ul>
			<hr>
			<p><strong>Shifts</strong></p>
			<p>Enables administrators to produce shift reports.</p>
			<ul>
				<li>Ability to filter by one or more of the following simultaneously: Section, user, date/time range</li>
				<li>Ability to order results by: Date (default), volunteer, section</li>
				<li>If more than 1 month in results, group results by month</li>
				<li>Total hours by section and grand total</li>
				<li>Show total shift count by volunteer</li>
				<li>Ability to open shift details in modal</li>
				<li>Ability to download results as CSV</li>
			</ul>
		</div>
	</div>
</div>

