<?php //$this->session->set_flashdata('uri', uri_string()) ?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>

<!-- Page -->
<div id="page">
	<div class="container">

		<!-- <pre>
		<?php print_r($session_data) ?>
		</pre> -->

		<div class="row" style="background:#F8F8F8; padding:30px 0;">
			
			<div class="col-sm-2 text-center">
				<!-- <i class="fa fa-street-view" style="font-size:20em;color:#286090"></i> -->
				<!-- <i class="fa fa-female" style="font-size:20em;color:#286090"></i> -->
				<i class="fa fa-male" style="font-size:10em;color:#286090"></i>
				<p style="padding:10px 0 0;">Member since<br/>2011</p>
			</div>

			<div class="col-sm-5">

				<form action="<?= base_url() ?>" method="POST" class="form-signin" style="">

					<div class="row">

						<div class="col-sm-6">
			                <label for="first_name" class="sr-only">First Name</label>
			                <input type="first_name" class="form-control" placeholder="First name" name="first_name" maxlength="25" value="<?= $session_data['first_name'] ?>" />
		            	</div>

		                <div class="col-sm-6">
			                <label for="last_name" class="sr-only">Last Name</label>
			                <input type="last_name" class="form-control" placeholder="Last name" name="last_name" maxlength="25" value="<?= $session_data['last_name'] ?>" />
						</div>

					</div>

					<div class="row">

						<div class="col-sm-6">
			                <label for="home_phone" class="sr-only">Home Phone</label>
	                		<input type="home_phone" class="form-control" placeholder="Home Phone" name="home_phone" maxlength="100" value="<?= $session_data['home_phone'] ?>" />
		            	</div>

		                <div class="col-sm-6">
			                <label for="mobile_phone" class="sr-only">Mobile Phone</label>
	                		<input type="mobile_phone" class="form-control" placeholder="Mobile Phone" name="mobile_phone" maxlength="100" value="<?= $session_data['mobile_phone'] ?>" />
						</div>

					</div>

					<div class="row">

						<div class="col-sm-6">
			                <label for="city" class="sr-only">City</label>
	                		<input type="city" class="form-control" placeholder="City" name="city" maxlength="100" value="<?= $session_data['city'] ?>" />
		            	</div>

						<div class="col-sm-6">
			                <label for="postal_code" class="sr-only">Postal Code</label>
	                		<input type="postal_code" class="form-control" placeholder="Postal Code" name="postal_code" maxlength="100" value="<?= $session_data['postal_code'] ?>" />
						</div>

					</div>

					<div class="row">

						<div class="col-sm-6">
			                <label for="email_address" class="sr-only">Email Address</label>
	                		<input type="email_address" class="form-control" placeholder="Email Address" name="email_address" maxlength="100" value="<?= $session_data['email_address'] ?>" />
		            	</div>

						<div class="col-sm-6">
			                <label for="birthdate" class="sr-only">Date of Birth</label>
	                		<input type="birthdate" class="form-control date_picker_past" placeholder="Date of Birth" name="birthdate" maxlength="100" value="<?= date("d/m/Y",strtotime($session_data['birthdate'])) ?>" />
						</div>

					</div>
                
	                <p><button class="btn btn-primary" type="submit" style="margin-top: 10px;">Update</button></p>

	            </form>

			</div>

			<div class="col-sm-5">

				<div class="row">

					<div class="col-sm-6">
		                <p class="text-center"><strong>Total Hours</strong></p>
		                <p class="text-center text-muted" style="font-size:5em;">121</p>
	            	</div>

					<div class="col-sm-6">
		                <p class="text-center"><strong>Total Shifts</strong></p>
		                <p class="text-center text-muted" style="font-size:5em;">18</p>
	            	</div>

				</div>
				
			</div>

		</div>


		<div class="row" style="margin-top:20px;">
			
			<div class="col-sm-6">
				<div class="row" style="background:#F8F8F8;padding:30px 0; height:200px;">
	                <p class="text-center"><strong>Shift Calendar</strong></p>
	                <p class="text-center text-muted" style="font-size:5em;"><i class="fa fa-calendar"></i></p>
            	</div>
            	<div class="row" style="background:#F8F8F8;padding:30px 0; height:200px; margin-top:20px;">
	                <p class="text-center"><strong>Shift List</strong></p>
	                <p class="text-center text-muted" style="font-size:5em;"><i class="fa fa-calendar"></i></p>
            	</div> 
        	</div>

			<div class="col-sm-6">
				<div class="row" style="background:#F8F8F8;padding:30px 0; height:430px;">
	                <p class="text-center"><strong>Recent News</strong></p>
	                <p class="text-center text-muted" style="font-size:5em;"><i class="fa fa-newspaper-o"></i></p>
	            </div>
        	</div>

		</div>			


		
		<div class="row" style="padding-top:100px">
			<p><strong>The following data all related to the currenty logged in volunteer.</strong></p>
			<ul>
				<li>Personal Details - Updateable</li>
				<li>Recent News - past 30 days with link to all volunteer news</li>
				<li>Upcoming Shifts - next 7 days with link to all upcoming shifts</li>
			</ul>
		</div>


		</div>
	</div>
</div>

