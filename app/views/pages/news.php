<?php //$this->session->set_flashdata('uri', uri_string()) ?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>

<!-- Page -->
<div id="page">
	<div class="container">
		<div class="row">
			<p><strong>News</strong></p>
			<ul>
				<li>Sort by categories accessible to the currently logged in volunteer</li>
				<li>Volunteers can add news to categories they have access to and edit news posted by them</li>
				<li>Admins can add to and edit news in category</li>
			</ul>
		</div>
	</div>
</div>

