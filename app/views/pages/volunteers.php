<?php //$this->session->set_flashdata('uri', uri_string()) ?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>

<!-- Page -->
<div id="page">
	<div class="container">
		<div class="row">
			<p><strong>Volunteers</strong></p>
			<ul>
				<li>List volunteers, including: Name, sections, email, phone numbers, etc</li>
				<li>Ability to filter by: Name, section, email, how long they've been a volunteer (5,10,15,20,25)</li>
				<li>Clicking a volunteer opens their profile page (basic for volunteers, more detailed for admins)</li>
				<ul>
					<li>Personal info fields</li>
					<li>Admin comment</li>
					<li>Section and Admin access grid</li>
					<li>Enable/Disable options</li>
					<li>Access Card ID (5 numbers)</li>
					<li>Volunteer ID (5 alphanumeric)</li>
					<li>Member Since Date</li>
					<li>Discharge Date</li>
					<li>Property Status, for:</li>
					<ul>
						<li>Access Card</li>
						<li>ID Card</li>
						<li>Notebook</li>
						<li>Clothing</li>
					</ul>
					<li>Property Status, radio options for each of the above:</li>
					<ul>
						<li>Not Applicable</li>
						<li>Issued</li>
						<li>Return Requested</li>
						<li>Received</li>
						<li>Deactivated</li>
					</ul>
				</ul>
			</ul>
		</div>
	</div>
</div>

