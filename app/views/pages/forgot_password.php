<!DOCTYPE html>
<html lang="en">

    <head>
    
        <title>HRP Volunteer Calendar</title>
        
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/app.css">

        <!-- <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'> -->
        
        <script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <!-- <script src="<?= base_url() ?>_assets/plugins/ckeditor/ckeditor.js"></script> -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>

            $(document).ready(function () {
                
            });

        </script>
        
    </head>
    
    <body>

        
        <div id="hfx_content_area" class="container">
                <div class="row">
                    <div class="col-sm-5 col-md-4 col-sm-offset-4">

                        <div class="text-center">
                            <img src="<?= base_url() ?>_assets/images/logo-sm.jpg">
                            <h1 class="form-signin-heading">Volunteer Calendar</h1>
                        </div>

                        <hr>

                        <h2 class="form-signin-heading">Forgot Password</h2>

                        <?php if($this->session->flashdata('confirmation')): ?>
                        <div class="alert alert-success text-center">
                            <?= $this->session->flashdata('confirmation') ?>
                        </div>
                        <?php endif ?>

                        <?php if($this->session->flashdata('error')): ?>
                        <div class="alert alert-danger text-center">
                            <?= $this->session->flashdata('error') ?>
                        </div>
                        <?php endif ?>

                        <?php if(validation_errors()): ?>
                        <div class="alert alert-danger text-center">
                            <?= validation_errors() ?>
                        </div>
                        <?php endif ?>

                        <?php $this->session->keep_flashdata('email_address') ?>

                        <form action="<?= base_url() ?>user/forgot_password" method="POST" class="form-signin">                         
                            
                            <label for="email_address" class="sr-only">Email Address</label>
                            <input type="email_address" class="form-control" placeholder="Email Address" name="email_address" maxlength="100" tabindex="1" autocomplete="off" autofocus />

                            <!-- <label for="password" class="sr-only">Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password" maxlength="50" tabindex="2" /> -->
                            
                            <div style="padding-top:10px;">
                                <button class="btn btn-primary" type="submit">Reset</button>
                                <a href="<?= base_url() ?>user/signin" class="btn btn-link pull-right">Remember your password? Sign in</a>
                            </div>

                        </form>

                        <hr>

                        <h2 class="form-signin-heading">Register</h2>
                        <p>If you are interested in becoming a volunteer, please visit our <a href="https://www.halifax.ca/Police/Programs/volunteering.php" target="_blank">volunteer opportunities page</a> and complete the <a href="">pre-registration form</a>.</p>

                        <!-- 
                        <div class="alert alert-info">
                            <p><strong>Our application has changed.</strong><br>You now sign in with your email address and password.</p>
                        </div>
                        -->

                        <form action="<?= base_url() ?>" method="POST" class="form-signin" style="display:none">
                           
                            <label for="first_name" class="sr-only">First Name</label>
                            <input type="first_name" class="form-control" placeholder="First name" name="first_name" maxlength="25" />

                            <label for="last_name" class="sr-only">Last Name</label>
                            <input type="last_name" class="form-control" placeholder="Last name" name="last_name" maxlength="25" />

                            <label for="email_address" class="sr-only">Email Address</label>
                            <input type="email_address" class="form-control" placeholder="Email Address" name="email_address" maxlength="100" />

                            <label for="home_phone" class="sr-only">Home Phone</label>
                            <input type="home_phone" class="form-control" placeholder="Home Phone" name="home_phone" maxlength="100" />

                            <label for="mobile_phone" class="sr-only">Mobile Phone</label>
                            <input type="mobile_phone" class="form-control" placeholder="Mobile Phone" name="mobile_phone" maxlength="100" />

                            <label for="city" class="sr-only">City</label>
                            <input type="city" class="form-control" placeholder="City" name="city" maxlength="100" />

                            <label for="province" class="sr-only">Province</label>
                            <input type="province" class="form-control" placeholder="Province" name="province" maxlength="100" value="Nova Scotia" readonly />

                            <label for="postal_code" class="sr-only">Postal Code</label>
                            <input type="postal_code" class="form-control" placeholder="Postal Code" name="postal_code" maxlength="100" />

                            <label for="dob" class="sr-only">Date of Birth</label>
                            <input type="dob" class="form-control" placeholder="Date of Birth" name="dob" maxlength="100" />
                            
                            <p><button class="btn btn-primary" type="submit" style="margin-top: 10px;">Submit</button></p>

                        </form>

                    </div>
                </div>
            </div>
        </div>


    </body>

</html>