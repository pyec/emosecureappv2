<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*           
============================================================================
 Class: 		Deliverables
 Description:	Main application controller. 
 				Interacts with the deliverables_model and application views.
============================================================================
*/
class Volunteers extends CI_Controller {

/*           
============================================================================
 index
----------------------------------------------------------------------------
 Load volunteers page
============================================================================
*/
	public function index()
	{
		
		$data['page'] = "pages/volunteers";
		$this->load->view('template/master', $data);

	}


}