<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User_v2_4 extends CI_Controller {


	

// Here I define my constant and use the se	
	
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('main_model_v2_4');

}


public function inner_get_security_levels()
{

       $securityARR = array(
			"1" => array( 'name' 		=> 'Civilian',
						'Code' 			=> 'CIV',
						'Level' 		=> '1',
						'Active' 		=> 'Y'
						),
			"2" => array( 'name' 		=> 'Staff',
						'Code' 			=> 'STF',
						'Level' 		=> '2',
						'Active' 		=> 'Y'
						),
			"5" => array( 'name' 		=> 'Confidential',
						'Code' 			=> 'CONF',
						'Level' 		=> '2',
						'Active' 		=> 'Y'
						),
			"15" => array( 'name' 		=> 'Restricted',
						'Code' 			=> 'REST',
						'Level' 		=> '15',
						'Active' 		=> 'Y'
						),
			"20" => array( 'name' 		=> 'Classified',
						'Code' 			=> 'CLSF',
						'Level' 		=> '2',
						'Active' 		=> 'Y'
						),
			"25" => array( 'name' 		=> 'Top Secret',
						'Code' 			=> 'TSC',
						'Level' 		=> '25',
						'Active' 		=> 'Y'
						),
	);
												
return $securityARR;
}


public function inner_get_roles()
{
        $rolesARR = array(
			"1" => array( 'name' 		=> 'Admin',
						'uploadYN' 		=> 'Y',
						'viewLVL' 		=> '1',
						'viewWhoYN' 		=> 'Y',
						'viewDisabledYN'=> 'Y',
						'viewUsersYN' 	=> 'Y',
						'viewUsersMU' 	=> 'Y',
						'viewUsersPublicDescYN' 		=> 'Y',
						'viewUsersPrivDescYN' 			=> 'N',
						'AddUsersYN'	=> 'Y',
        				'detailUsersYN'	=> 'Y',
        				'ModifyUsersYN'	=> 'Y',
        				'DisableUsersYN'=> 'Y',
						'DeleteUsersYN'	=> 'N',
						'KillUsersYN' 	=> 'Y',
						'viewCatsYN' 	=> 'Y',
						'viewCatsMU' 	=> 'Y',
						'viewCatsPublicDescYN' 			=> 'Y',
						'viewCatsPrivDescYN' 			=> 'Y',
        				'detailCatsYN'	=> 'Y',
        				'AddCatsYN'		=> 'Y',
        				'ModifyCatsYN'	=> 'Y',
        				'DisableCatsYN'	=> 'Y',
						'DeleteCatsYN'	=> 'N',
						'KillCatsYN'	=> 'Y',
						'viewSubcatsYN' => 'Y',
						'viewSubcatsMU'	=> 'Y',
						'viewSubcatsPublicDescYN' 		=> 'Y',
						'viewSubcatsPrivDescYN' 		=> 'N',
        				'detailSubcatsYN'=> 'Y',
        				'AddSubcatsYN'	=> 'Y',
        				'ModifySubcatsYN'=> 'Y',
        				'DisableSubcatsYN'=> 'Y',
						'DeleteSubcatsYN'=> 'N',
						'KillSubcatsYN'	=> 'Y',
        				'viewDocsYN' 	=> 'Y',
						'viewDocsMU' 	=> 'Y',
						'viewDocsPublicDescYN' 		=> 'Y',
						'viewDocsPrivDescYN' 		=> 'Y',
        				'detailDocsYN'	=> 'Y',
        				'AddDocsYN'		=> 'Y',
        				'ModifyDocsYN'	=> 'Y',
        				'DisableDocsYN'	=> 'Y',
						'DeleteDocsYN'	=> 'N',
						'KillDocsYN' 	=> 'Y',
						'viewAccessYN' 	=> 'Y',
						'viewAccessMU' 	=> 'Y',
						'viewAccessPublicDescYN' 	=> 'Y',
						'viewAccessPrivDescYN' 		=> 'N',
        				'detailAccessYN'=> 'Y',
						'AddAccessYN'	=> 'Y',
        				'ModifyAccessYN'=> 'Y',
        				'DisableAccessYN'=> 'Y',
        				'RemoveAccessYN'=> 'N',
        				'DeleteAccessYN'=> 'N',
						'KillAccessYN' 	=> 'Y',
        				        'year' => '2010'
						),
			"2" => array( 'name' 		=> 'Operations',
						'uploadYN' 		=> 'Y',
						'viewLVL' 		=> '1',
						'viewWhoYN' 		=> 'Y',
						'viewDisabledYN'=> 'N',
						'viewUsersYN' 	=> 'N',
						'viewUsersMU' 	=> 'N',
						'viewUsersPublicDescYN' 		=> 'Y',
						'viewUsersPrivDescYN' 			=> 'N',
						'AddUsersYN'	=> 'N',
        				'detailUsersYN'	=> 'N',
        				'ModifyUsersYN'	=> 'N',
        				'DisableUsersYN'	=> 'N',
						'DeleteUsersYN'	=> 'N',
						'KillUsersYN' 	=> 'N',
						'viewCatsYN' 	=> 'Y',
						'viewCatsMU' 	=> 'Y',
						'viewCatsPublicDescYN' 		=> 'Y',
						'viewCatsPrivDescYN' 		=> 'N',
						'detailCatsYN'	=> 'Y',
        				'AddCatsYN'		=> 'N',
        				'ModifyCatsYN'	=> 'N',
        				'DisableCatsYN'	=> 'N',
						'DeleteCatsYN'	=> 'N',
						'KillCatsYN'	=> 'N',
						'viewSubcatsYN' => 'Y',
						'viewSubcatsMU'	=> 'N',
						'viewSubcatsPublicDescYN' 		=> 'Y',
						'viewSubcatsPrivDescYN' 		=> 'N',
						'detailSubcatsYN'=> 'N',
        				'AddSubcatsYN'	=> 'N',
        				'ModifySubcatsYN'=> 'N',
        				'DisableSubcatsYN'=> 'N',
						'DeleteSubcatsYN'=> 'N',
						'KillSubcatsYN'	=> 'N',
        				'viewDocsYN' 	=> 'Y',
						'viewDocsMU' 	=> 'N',
						'viewDocsPublicDescYN' 		=> 'Y',
						'viewDocsPrivDescYN' 		=> 'Y',
						'detailDocsYN'	=> 'Y',
        				'AddDocsYN'		=> 'N',
        				'ModifyDocsYN'	=> 'N',
        				'DisableDocsYN'	=> 'Y',
						'DeleteDocsYN'	=> 'N',
						'KillDocsYN' 	=> 'N',
						'viewAccessYN' 	=> 'N',
						'viewAccessMU' 	=> 'N',
						'viewAccessPublicDescYN' 	=> 'Y',
						'viewAccessPrivDescYN' 		=> 'N',
        				'detailAccessYN'=> 'N',
						'AddAccessYN'	=> 'N',
        				'ModifyAccessYN'=> 'N',
        				'DisableAccessYN'=> 'N',
        				'RemoveAccessYN'=> 'N',
						'DeleteAccessYN'=> 'N',
						'KillAccessYN' 	=> 'N',
						
				        'year' => '2010'
						),
			"3" => array( 'name' 		=> 'General User',
						'uploadYN' 		=> 'N',
						'viewLVL' 		=> '1',
						'viewWhoYN' 		=> 'N',
						'viewDisabledYN'=> 'N',
						'viewUsersYN' 	=> 'N',
						'viewUsersMU' 	=> 'N',
						'viewUsersPublicDescYN' 		=> 'N',
						'viewUsersPrivDescYN' 			=> 'N',
						'AddUsersYN'	=> 'N',
        				'detailUsersYN'	=> 'N',
        				'ModifyUsersYN'	=> 'N',
        				'DisableUsersYN'=> 'N',
						'DeleteUsersYN'	=> 'N',
						'KillUsersYN' 	=> 'N',
						'viewCatsYN' 	=> 'Y',
						'viewCatsMU' 	=> 'Y',
						'viewCatsPublicDescYN' 		=> 'N',
						'viewCatsPrivDescYN' 		=> 'N',
						'detailCatsYN'	=> 'Y',
        				'AddCatsYN'		=> 'N',
        				'ModifyCatsYN'	=> 'N',
        				'DisableCatsYN'	=> 'N',
						'DeleteCatsYN'	=> 'N',
						'KillCatsYN'	=> 'N',
						'viewSubcatsYN' => 'N',
						'viewSubcatsMU'	=> 'N',
						'viewSubcatsPublicDescYN' 		=> 'N',
						'viewSubcatsPrivDescYN' 		=> 'N',
						'detailSubcatsYN'=> 'N',
        				'AddSubcatsYN'	=> 'N',
        				'ModifySubcatsYN'=> 'N',
        				'DisableSubcatsYN'=> 'N',
						'DeleteSubcatsYN'=> 'N',
						'KillSubcatsYN'	=> 'N',
        				'viewDocsYN' 	=> 'Y',
						'viewDocsMU' 	=> 'N',
						'viewDocsPublicDescYN' 		=> 'Y',
						'viewDocsPrivDescYN' 		=> 'N',
						'detailDocsYN'	=> 'N',
        				'AddDocsYN'		=> 'N',
        				'ModifyDocsYN'	=> 'N',
        				'DisableDocsYN'	=> 'N',
						'DeleteDocsYN'	=> 'N',
						'KillDocsYN' 	=> 'N',
						'viewAccessYN' 	=> 'N',
						'viewAccessMU' 	=> 'N',
						'viewAccessPublicDescYN' 	=> 'N',
						'viewAccessPrivDescYN' 		=> 'N',
						'detailAccessYN'=> 'N',
						'AddAccessYN'	=> 'N',
        				'ModifyAccessYN'=> 'N',
        				'DisableAccessYN'=> 'N',
        				'RemoveAccessYN'=> 'N',
						'DeleteAccessYN'=> 'N',
						'KillAccessYN' 	=> 'N',
				        'year' => '2010'
						),
			"4" => array( 'name' 		=> 'SuperAdmin',
						'uploadYN' 		=> 'Y',
						'viewLVL' 		=> '1',
						'viewWhoYN' 		=> 'Y',
						'viewDisabledYN'=> 'Y',
						'viewUsersYN' 	=> 'Y',
						'viewUsersMU' 	=> 'Y',
						'viewUsersPublicDescYN' 		=> 'Y',
						'viewUsersPrivDescYN' 			=> 'Y',
						'AddUsersYN'	=> 'Y',
        				'detailUsersYN'	=> 'Y',
        				'ModifyUsersYN'	=> 'Y',
        				'DisableUsersYN'=> 'Y',
						'DeleteUsersYN'	=> 'Y',
						'KillUsersYN' 	=> 'Y',
						'viewCatsYN' 	=> 'Y',
						'viewCatsMU' 	=> 'Y',
						'viewCatsPublicDescYN' 	=> 'Y',
						'viewCatsPrivDescYN' 	=> 'Y',
        				'detailCatsYN'	=> 'Y',
        				'AddCatsYN'		=> 'Y',
        				'ModifyCatsYN'	=> 'Y',
        				'DisableCatsYN'	=> 'Y',
						'DeleteCatsYN'	=> 'Y',
						'KillCatsYN'	=> 'Y',
						'viewSubcatsYN' => 'Y',
						'viewSubcatsMU'	=> 'Y',
						'viewSubcatsPublicDescYN' 		=> 'N',
						'viewSubcatsPrivDescYN' 		=> 'N',
        				'detailSubcatsYN'=> 'Y',
        				'AddSubcatsYN'	=> 'Y',
        				'ModifySubcatsYN'=> 'Y',
        				'DisableSubcatsYN'=> 'Y',
						'DeleteSubcatsYN'=> 'Y',
						'KillSubcatsYN'	=> 'Y',
        				'viewDocsYN' 	=> 'Y',
						'viewDocsMU' 	=> 'Y',
						'viewDocsPublicDescYN' 		=> 'Y',
						'viewDocsPrivDescYN' 		=> 'Y',
        				'detailDocsYN'	=> 'Y',
        				'AddDocsYN'		=> 'Y',
        				'ModifyDocsYN'	=> 'Y',
        				'DisableDocsYN'	=> 'Y',
						'DeleteDocsYN'	=> 'Y',
						'KillDocsYN' 	=> 'Y',
						'viewAccessYN' 	=> 'Y',
						'viewAccessMU' 	=> 'Y',
						'viewAccessPublicDescYN' 	=> 'Y',
						'viewAccessPrivDescYN' 		=> 'Y',
						'detailAccessYN'=> 'Y',
						'AddAccessYN'	=> 'Y',
        				'ModifyAccessYN'=> 'Y',
        				'DisableAccessYN'=> 'Y',
        				'RemoveAccessYN'=> 'Y',
						'DeleteAccessYN'=> 'Y',
						'KillAccessYN' 	=> 'Y',
						'year' => '2010'
						)
      );
      
	return $rolesARR;					
	
}

/*           
============================================================================
 index
----------------------------------------------------------------------------
 Load  view
============================================================================
*/
	public function index()
	{
		$this->yprint("Loading Signin","user->signin()",0);
		$this->ctr_start_login_v2_4();
	}

/* 	
=============================================================================================
 () - Attempts to log a user in using email address and password.
---------------------------------------------------------------------------------------------
 Requires a valid email_address and password from post data. Validates those credentials 
 against records in the database. If valid, an array of user data is returned and used to 
 create a session. If invalid, the user is returned to the  page with error message.
=============================================================================================
*/
	public function ctr_signin()
	{
		$data = $this->input->post(null, true);

		// if credentials were supplied, attempt to authenticate
		// if($this->input->post('email_address', true) && $this->input->post('password', true))
		if($data)
		{
			$this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email|min_length[6]|max_length[100]|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[5]|max_length[15]|required');

			// validation failed
			if ($this->form_validation->run() == false)
			{
				$this->load->view('pages_v2_4/pg_signin_v2_4');
			}
			// validation successful
			else
			{
				// get data  data and authenticate user
				$this->yprint("Attempting to Authenticate","user->authenticate()",0);
				$result = $this->user_model->authenticate($data);
				$this->yprintARR($result, "LOGIN REC Dump", 0);    // DBG
				
				// if successful, load user data into session and goto user profile
				if($result)
				{
					// echo "<pre>";
					// print_r($result);
					// echo "</pre>";
					// die();

					$this->session->set_userdata($result);
					$this->yprint("Authenticate Success","user->authenticate()",0);
										
					redirect('user-profile');
				}
				// if failed, goto  with error
				else
				{
					$this->my_set_flash_msg('',0,'error', 'Invalid credentials.<br>Try again or <a href="'.base_url().'user-forgot-password">reset your password</a>');
					$this->my_set_flash_msg('',0,'email_address', $data['email_address']);
					$this->yprint("Authenticate Failed","user->authenticate()",0);
					redirect('user-signin');
				} 
			}
		}
		// if no credentials, goto  with error
		else
		{
			// $data['page'] = 'pages_v2_4/content/';
			// $this->load->view('pages_v2_4/general', $data);
			
			 $data['page'] = "pages_v2_4/pg_signin_v2_4";
			 $this->load->view('template/master', $data);
		}
	}													// EO ctr_login FUNC


/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_signin_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This replaces the old controller that had little security.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-09		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_signin_v2_4()
	{
		$post = $this->input->post(null, true);

		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles SIGNIN V2_4", 0);    // DBG

		$this->yprintARR($post, "POST FOund", 0);    // DBG
		exit;
			
		// if credentials were supplied, attempt to authenticate
		// if($this->input->post('email_address', true) && $this->input->post('password', true))
		if($post)
		{
			$this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email|min_length[6]|max_length[100]|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[5]|max_length[15]|required');

			// validation failed
			if ($this->form_validation->run() == false)
			{
				$this->load->view('pages_v2_4/pg_signin_v2_4');
				$this->yprint("Authenticate Form failure","user->authenticate()",0);
				
exit;			
			}
			// validation successful
			else
			{
				// get data  data and authenticate user
				$this->yprint("Attempting to Authenticate","user->authenticate()",0);

				// get post variables

				$w_username = $data['email_address'];
				$w_password = $data['password'];
				$resp = $this->user_authenticate_v2_4($w_username, $w_password, 0);
				$result = $this->user_model->authenticate($data);
				$this->yprintARR($result, "LOGIN REC Dump", 0);    // DBG
				
				// if successful, load user data into session and goto user profile
exit;
				switch($resp)
				{
					case -2:
					case -3:
						$this->yprint("Major Authenticate Failed","user->authenticate()",0);
						redirect('new-login');
						break; 
						
					default:
						$this->yprint("Authenticate Failed","user->authenticate()",0);
						redirect('new-login');
			 			$data['page'] = "pages_v2_4/pg_signin_v2_4";
			 			$this->load->view('template/master', $data);
						break;
					
					case 1:
//						$this->session->set_userdata($result);
						$this->yprint("Authenticate Success","user->authenticate()",0);
						redirect('user-profile');
				} 										// EO Switch resp
			
		}												// EO If form Validation
	}													// EO If data to check


// If it gets here, there are other issues - PRSC

	$this->my_set_flash_msg('',0,'error', "Credentials Error. Please Contact Support or your App Admin.");
	$this->yprint("Serious Authentication Failure","user->authenticate()",0);
	$data['page'] = "pages_v2_4/pg_signin_v2_4";
	$this->load->view('template/master', $data);
	
}														// EO ctr_login FUNC
	
	

/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_start_login_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This replaces the old login controller that had little security.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-09		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_start_login_v2_4()
	{
		// No Input Data so force simple view

		$this->yprint("First Login Requested","user->start_login()",0);
		$data['page'] = "pages_v2_4/pg_signin_v2_4";
		$this->load->view('template/master', $data);
   	
}														// EO ctr_login FUNC
	


/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_new_login_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This replaces the old login controller that had little security.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-09		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_new_login_v2_4()
	{
		
//		$postARR = array();
		$postARR = $this->input->post(null, TRUE);

//		$this->yprint("New Login Attempt To Approve","user->authenticate()",0);
//		$this->yprintARR($postARR, "Login Post values ", 0);    // DBG
		
		
		
		// if credentials were supplied, attempt to authenticate
		// if($this->input->post('email_address', true) && $this->input->post('password', true))
		if($postARR)
		{
			$this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email|min_length[6]|max_length[100]|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[5]|max_length[15]|required');

			$this->yprint("Attempt Form Validation Failure","user->authenticate()",0);
			
			// validation failed
			if ($this->form_validation->run() == false)
			{
				$this->yprint("Form Validation Failure","user->authenticate()",0);
				
				$this->load->view('pages_v2_4/pg_signin_v2_4');
			}
			// validation successful
			else
			{
				// get data  data and authenticate user
				$this->yprint("NWLOG Attempting to Authenticate","user->authenticate()",0);

				// get post variables

				$w_username = $postARR['email_address'];
				$w_password = $postARR['password'];
				
				$this->yprintARR($postARR, "VNL Login Post values ", 0);    // DBG

				$resp = $this->user_authenticate_v2_4($w_username, $w_password, 0);
//				$result = $this->user_model->authenticate($data);
//				$this->yprintARR($result, "LOGIN REC Dump", 0);    // DBG
				
				// if successful, load user data into session and goto user profile

				$this->yprint("Authenticate Results [ " . $resp . "]","user->authenticate()",0);
				
				
				switch($resp)
				{
					case -2:
					case -3:
						$this->yprint("Major Authenticate Failed","user->authenticate()",0);
						redirect('new-login');
						break; 
						
					default:
						$this->yprint("Authenticate Failed","user->authenticate()",0);
						redirect('new-login');
			 			$data['page'] = "pages_v2_4/pg_signin_v2_4";
			 			$this->load->view('template/master', $data);
						break;
					
					case 1:
//						$this->session->set_userdata($result);
						$this->yprint("Authenticate Success","user->authenticate()",0);
						redirect('user-profile');
				} 										// EO Switch resp
			
		}												// EO If form Validation
	}													// EO If data to check
   else 
   {						// No Input Data so force simple view

		$this->yprint("New Login Requested","user->authenticate()",0);
		$exit;
		$data['page'] = "pages_v2_4/pg_signin_v2_4";
		$this->load->view('template/master', $data);
   }

$this->yprint("MAJOR FAIL","user->authenticate()",0);
   
   
}														// EO ctr_login FUNC
	



/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_user_authenticate_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This is the controller that handles log in validateion.
 * 
 * 		RESPONSE
 * 
 * 		Returns OKAY or an Error Code below zero if something specific.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-18		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function user_authenticate_v2_4($i_userACCT = NULL, $i_userPASS = NULL,$xcond = NULL)
	{
		$this->yprint("Testing User Data [" . $i_userACCT . "]",'user->ev_user_authenticate',0);		// DBG

		// load model
		$this->load->model('main_model_v2_4'); 

		$w_userACCT = $i_userACCT;
		$w_userPASS = $i_userPASS;
		
		
		if (!empty($w_userACCT) && !empty($w_userPASS))
		{
			$user_data = $this->main_model_v2_4->md_get_user_by_email($w_userACCT);

			$this->yprintARR($user_data, "USER DATA Returned", 0);    // DBG
			
			if (!empty($user_data))
			{
				if ($w_userPASS == $user_data['Password'])
				{
					if ($user_data['Active'])
					{
						$array = array( 
							'UID' 		=> $user_data['UID'],
							'ACCTID' 	=> $w_userACCT,
							'Email' 	=> $w_userACCT,
						
//							'secLVL'	=> $user_data['SecurityLevel'],
							'userNAME'	=> $user_data['FirstName'] . " " . $user_data['LastName'],
//							'userDEPT'	=> $user_data['Department'],
							'UserGroup'	=> $user_data['UserGroup'],
//							'trainingYN'=> $user_data['TrainingYN'],
							'loggedIn' 	=> TRUE
							);
						
						$this->session->set_userdata( $array );
						$this->yprintARR($array, "Successful SESSION values", 0);    // DBG
						
						
						return 1;
						
//						$this->main_login();
//						$this->index();
					}
					else
					{
						$this->my_set_flash_msg('',0,'error', 'Your account has been disabled. Pleae contact your Admin.');
						return -1;
					}
				}
				else
				{		//TODO Failed Credentials - TODO this should send warning to App Admin - PRSC
					$this->my_set_flash_msg('',0,'error', "The credentials you've provided are incorrect. Please Try Again");
					return -2;
				}
			}
			else
			{	//TODO Failed Database Rec Read - this should REALLY set off an App Alarm = PRSC
				$this->my_set_flash_msg('',0,'error', "Account not on File. Please Contact Admin.");
				return-3;
			}
			
		}
		else
		{
			$this->my_set_flash_msg('',0,'error', "You must enter an Account and Password.");
			return -1;
		}
		
return -4;		
}																// EO Validate Users					




	
	

/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_profile
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-18		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_profile()
	{
		$this->yprint("Main User Entry Page ",'user->ev_profile',0);		// DBG
		
		$currUserARR = $this->session->all_userdata();
		$this->yprintARR($currUserARR, "User DATA ", 0);    // DBG

		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		
		$userTYPE = $currUserARR['UserGroup']; 
		exit;
		switch($userTYPE)
		{
			default:
				$data['page'] = "pages_v2_4/pg_profile_v2_4" ;
				break;
				
			case 1:
				$data['page'] = "pages_v2_4/pg_profile_v2_4" ;
				break;
			
		}
		
		$data['sessionUserREC'] = $currUserARR;

		$this->load->view('template/master', $data);
	}	


/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_profile_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-18		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_profile_v2_4()
	{
		$this->yprint("Main User Entry Page ",'user->ev_profile',0);		// DBG

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
				
			}	
			
			
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
		
		$userTYPE = $sessionUserREC['UserGroup']; 
		
		switch($userTYPE)
		{
			default:
				$data['page'] = "pages_v2_4/pg_profile_v2_4" ;
				break;
				
			case 1:
				$data['page'] = "pages_v2_4/pg_profile_v2_4" ;
				break;
			
		}
		
		$data['sessionUserREC'] = $sessionUserREC;

		$this->load->view('template/master', $data);

}													// EO Show Profile Function


	
	

//------------------------------------------------( SO User Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-user" view file.
 		This will allow users to create and enter a new user
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_user_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "User Session", 0);    // DBG
		
		
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authoriztion.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Add Users .');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
			
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);
		$this->yprintARR($post, "POST DATA ", 0);    // DBG
		
		if (empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
//			$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/user/pg_add_user_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('UID', 'User ID', 'required');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('UserTypesID', 'User Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('UserDesc', 'User Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_user($post))
				{
					$data['roleREC'] 			= $roleREC;
					$data['sessionUserREC']		= $sessionUserREC;
					
					$this->my_set_flash_msg('',0,'message',
						 'The User record has been successfully added.');

					// Get Necessary Reference Data used in Screen
					$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
					$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

					$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";
					$this->load->view('template/master', $data);
					
//					redirect('display-user/'.$post['REG_NUM']);
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
						'There was an error adding the User record, please try again.');
					redirect('show-users');
				}
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('show-users');
			}
		}
		
	}										// EO Add User Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-user" view file.
 		This will allow users to create and enter a new user
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_user_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
				
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detaledUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display User Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
//		$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_user_by_id($i_recID);
//		$this->yprintARR($resultsARR, "UserDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['userREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

			$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'User '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-users');
			}
			
		}
		
	}											// EO Display User Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_user_and_groups_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-user" view file.
 
 		This will allow users to create and enter a new user
 		as requested.
 		
		This shows the details of the groups the user is part of. 		
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_user_and_groups_v2_4($i_recID = NULL)
	{	
		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();
		$response_page 			= 'show-users';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Detailed User Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
								
//		$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_user_by_id($i_recID);
//		$this->yprintARR($resultsARR, "UserDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{

			$resp = $this->rsc_set_landing_page('display-user/' . $i_recID,'dpcat',0);
				
			/*............Get List of Associated Groups User Needs........PRSC.*/	

			if($roleREC['viewDisabledYN'] == 'Y')	
				$viewDisabledYN = FALSE;
			else 
				$viewDisabledYN = TRUE;
				
			$accessListARR			= array();
			$usersHLDARR			= array();
			$userID					= $i_recID;	
//			$wrkARR 				= $this->main_model_v2_4->md_get_access_users_in_category($categoryID.$viewDisabledYN,'DESC');
			$wrkARR 				= $this->main_model_v2_4->md_get_access_categories_user_is_in($userID,$viewDisabledYN,'DESC');
			$categoriesARR			= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$cntxx					= 0;

			foreach ($wrkARR as $workREC)
			{
			$wID			   						= $workREC['CID'];	

//			$this->yprintARR($workREC, "AccessList", 0);    	// DBG
			
			foreach ($categoriesARR as $categoryREC)
				{
				if($categoryREC['CID'] == $wID)
				{
				$accessListARR[$cntxx]      			= $categoryREC;	
				$cntxx++;
				}
				}
			}
			
//			$this->yprintARR($categoriesARR, "UserCategoriesList", 0);    	// DBG
			$data['categoriesARR'] 				= $accessListARR;
				
		
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

			// Get Master Data
			$data['userREC']			= $resultsARR;
			
//			$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";
			$data['page'] = "pages_v2_4/user/pg_display_user_and_accesses_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'User '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
			redirect($response_page);
			}
			
		}
		
	}											// EO Display User Function
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-user"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_user_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
	/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifyUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Modify User Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		
		
//		$rec_ID_to_update = $this->uri->segment(3);

		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
		}
		else
		{								// Error - Reshow Page
			$this->my_set_flash_msg('',0,'error', 
						'No valid record ID specified. Please try again');
			$data['page'] = 'pages_v2_4/user/pg_change_user_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_user_by_id($rec_ID_to_update);
		$this->yprintARR($resultsARR, "UserDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change User REC [ " . $i_recID . "] has been Updated",'',0);
//			$this->my_set_flash_msg('',0,'message', 
//						"Change User REC [ " . $i_recID . "] has been Updated");
			
			$data['userREC']  = $resultsARR;
			$data['page'] = 'pages_v2_4/user/pg_change_user_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->my_set_flash_msg('',0,'error', 
						'Data for that User not Available. Please try again');
			$data['page'] = 'pages_v2_4/user/pg_change_user_v2_4';
			redirect('show-users');
		}
		
	}												// EO Update User Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_user_v2_4()
	{
		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$post['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifyUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Update User Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		
		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionUserREC['Email'];
		
//		$this->yprint("UPDATE User REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionUserREC['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
//		$this->yprintARR($post, "UPDATE User INFO v2_4", 0);    // DBG
		
		
		if(empty($post))
		{
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('UID', 'User ID', 'required|numeric');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('UserTypesID', 'User Group', 'required');
			$this->form_validation->set_rules('ActiveStatesID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('UserDesc', 'User Desc', '');
			
			$rec_ID_to_update = $post['UID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_user($post))
				{
					$this->my_set_flash_msg('',0,'message', 
							'The User record has been updated successfully.');
					redirect('show-users');
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
							'There was an error updating the User record, please try again.');
//					redirect('change-user/' . $rec_ID_to_update);
					redirect('show-users');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->my_set_flash_msg('',0,'error', 
							'Incorrect User Data Entered. Please try again.');
//				$this->my_set_flash_msg('',0,'error', validation_errors());

			print "something is wrong";
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/user/pg_change_user_v2_4";
			$this->load->view('template/master', $data);
				
//				redirect('change-user/' . $rec_ID_to_update);
//				redirect('show-users');
				
			}
		}
		
	}												// EO Update User Function
	

/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_disable_user_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This will disable or enable a specified record disableYN flag.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-14		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_disable_user_v2_4_CNIU($recID = NULL)
	{

		$this->yprint("Disable Category Request","user->start_login()",0);
		$data['page'] = "pages_v2_4/pg_not_available_v2_4";
		$this->load->view('template/master', $data);
   	
}														// EO disable_user FUNC



	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	flipit_user_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a access record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_flipit_user_v2_4()
	{
		$data['data_state'] 	= "";
		$response_page 			= "show-users";

		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		// Check for new back page on changes - PRSC
		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		$processYN 				= TRUE;
		$esc_module				= "display-access";

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		$this->yprint("LANDING PAGE [ " . $response_page . "]",'',0);
			
		
			
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable-Disable User Records.');
			redirect('new-login');
			}
		}

			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.........PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		
//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

		$this->yprintARR($post, "Change State IT v2_4", 0);    // DBG

		
				
		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That User Confirmed to Delete Else Bail..........*/

		if(empty($post['mod_state']))
		{
				$this->my_set_flash_msg('',0,'message', 'Mod Aborted.' . $post['UID']);
				redirect($response_page);
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			print "STOP";
			exit;
			$data['page'] = "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('UID', 'User ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$recID_to_disable = $post['UID'];
			$i_recID = $recID_to_disable;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
//			$this->yprintARR($post, "Disable User v2_4", 0);    // DBG
				
			if($recID_to_disable)
				{
				// Switch to Choice for generic
				switch($post['mod_state'])
				{
					default:
						
						break;
					
					case 2:
						$post['Active']  = 2;
						$resp = $this->main_model_v2_4->md_flip_user($post);
						$nstate = 'Enabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
						
					case 1:
						$post['Active']  = 1;
						$resp = $this->main_model_v2_4->md_flip_user($post);
						$nstate = 'Disabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
					
				}	

				
				if ($resp)
				{
				$this->my_set_flash_msg('',0,'message', 'User '. $recID_to_disable . ' has been ' . $nstate);
//					$this->yprint("Change State Successful [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'User '.$recID_to_disable.' could not be altered. Please call tech support.');
//					$this->yprint("Change State Failed [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}

}											// EO FlipIt Func
		
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_disable_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-access"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_disable_user_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_user_v2_4($i_recID,1);
	
	}

	public function ctr_enable_user_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_user_v2_4($i_recID,2);
	
	}
	
	
	public function ctr_flip_user_v2_4($i_recID = NULL,$mod_state = NULL)
	{
		$data['data_state'] = "";
		$resultsARR			= array();
		$response_page		= 'show-users';	
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
//		$this->yprint("FlipThisUser REC [ " . $i_recID . "]",'',0);		// DBG
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable/Disable User Records.');
			redirect('new-login');
			}
		}
		
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
			
			/*.........Get User list...................-PRSC */
			$resultsARR = $this->main_model_v2_4->md_get_user_by_id($i_recID);   
			
//			$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "User UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 		= $this->main_model_v2_4->md_get_user_types();
			
			$data['mod_state']			= $mod_state;
			
			$data['userREC']			= $resultsARR;
			$data['page'] 				= "pages_v2_4/user/pg_flipthis_user_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display ENAGB YN Function
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_user_v2_4()
	{
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
		{
		$data['roleREC'] 		= $roleREC;
		}
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Create User Records.');
			redirect('new-login');
			}
		}
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

//		$this->yprint("UPDATE User REC [ " . $rec_ID_to_update . "]",'',0);
		$this->yprintARR($post, "CREATE User INFO v2_4", 0);    // DBG
		if(empty($post))
		{
			$post['CreatedBy']			= $sessionUserREC['Email'];
			$post['sessionUserREC'] 	= $sessionUserREC;
			
			
			// Get Necessary Reference Data used in Screen
			$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$post['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
			$post['page'] = 'pages_v2_4/user/pg_add_user_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('UID', 'User ID', 'required|numeric');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('UserTypesID', 'User Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
			
//			$this->form_validation->set_rules('UserDesc', 'User Desc', '');
			
//		$rec_ID_to_update = $post['UID'];

		if ($this->form_validation->run() == true)
		{
			$post['CreatedBy']			= $sessionUserREC['Email'];
			$post['sessionUserREC'] 	= $sessionUserREC;
			
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_user($post);
   			$this->yprint("NEW User REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "User Record Created Successfully";
				$this->my_set_flash_msg('',0,'success', 'User was created.');
			
				// Get Necessary Reference Data used in Screen
				$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
				$post['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_user" web page.
			
				$post['userREC'] 		= $this->main_model_v2_4->md_get_user_by_id($recID);
				$post['roleREC'] 		= $roleREC;
				
				$post['page'] = "pages_v2_4/user/pg_display_user_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$this->my_set_flash_msg('',0,'error', 'DB Failure Creating User Record.');
				$data['data_state'] = "DB Failure Creatng User Record";
				redirect('show-users');
			}
			
		}	
		else 
		{									// Error in data entry
			$post['roleREC'] 				= $roleREC;
			$post['CreatedBy']				= $sessionUserREC['Email'];
			$post['sessionUserREC'] 		= $sessionUserREC;

			// Get Necessary Reference Data used in Screen
			$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$post['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
			$this->my_set_flash_msg('',0,'error', 'Data Entry Error Creating User Record. Please Try again');
			$post['data_state'] = "Data Entry Error Creatng User Record";
			$post['page'] = "pages_v2_4/user/pg_add_user_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create User Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_user_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display user.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_user_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		
		$this->yprint("DeleteThisUser REC [ " . $i_recID . "]",'',0);		// DBG


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG
		
		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Category Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_user_by_id($i_recID);   

//			$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "User UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
			$data['userREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/user/pg_deletethis_user_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display User Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_user_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a user record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_user_v2_4()
	{
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		
		$processYN 				= TRUE;
		$esc_module				= "display-user";

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete User Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.............PRSC */
		
		
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That User Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->my_set_flash_msg('',0,'message', 'Delete Aborted.' . $post['UID']);
				redirect('show-users');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('UID', 'User ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['UID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_user($rec_ID_to_delete))
				{
					$this->my_set_flash_msg('',0,'message', 'User '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-users');
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'User '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-users');
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-users');
			}
		}

}											// EO Deleteit Func
		
	
	
	
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_users
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available users on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_users_v2_4()
	{

		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['viewUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to List Users.');
			redirect('new-login');
			}
		}
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
	
		/*...............Get Data REC details.............PRSC */
						
		// This shows a list of the Risks for users to reference
		$resultsARR							= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');

		if(!empty($resultsARR))
			{
			$data['usersARR']				= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			}
			
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['page'] = "pages_v2_4/user/pg_show_users_v2_4";
		$this->load->view('template/master', $data);
		
	}	
		
	

/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_disp_user
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This just shows a user and the details attached to it.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_display_user($i_recID = NULL)
	{

		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailUsersYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display User Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		
// Get the other data used to fill the link fields or external refs PRSC
		
//		$data['services'] 					= $this->main_model_v2_4->md_get_services();
//		$data['business_units'] 			= $this->main_model_v2_4->get_business_units();
//		$data['serviceareas']				= $this->main_model_v2_4->get_service_areas();

		// This shows a list of the Risks for users to reference
		$resultsARR							= $this->main_model_v2_4->md_get_user_by_id($i_recID);

		
		$this->yprintARR($resultsARR, "LOGIN REC Dump [" . $i_recID . "]", 0);    // DBG
		
		if(!empty($resultsARR))
			{
			$data['userREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

			$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'User '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
			redirect('show-users');
				
			}
			
	}	
	
	
	
//------------------------------------------------( EO User Functions Block )

	
	
//------------------------------------------------( SO Category Functions Block )

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-category" view file.
 		This will allow users to create and enter a new category
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_category_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

				/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
	   /*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*...............Get Ref tables...................PRSC */
		
		$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Add Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
			
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['page'] = "pages_v2_4/category/pg_add_category_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('CID', 'Category ID', 'required');
			$this->form_validation->set_rules('Name', 'Category Name', 'required');

			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_category($post))
				{
					$this->my_set_flash_msg('',0,'message',
						 'The Category record has been added successfull added.');
					redirect('Add-category/'.$post['REG_NUM']);
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
						'There was an error adding the Category record, please try again.');
					redirect('Add-category');
				}
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('Add-category');
			}
		}
		
	}										// EO Add Category Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-category" view file.
 		This will allow users to create and enter a new category
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_category_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();
		$response_page		= 'show-categories';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		/*...............Determine User Allowance.........*/
		$this->yprint("Main User Entry Page ",'user->ev_profile',0);		// DBG
		
		$currUserARR = $this->session->all_userdata();
		if(empty($currUserARR['loggedIn']))
				redirect('new-login');
		if($currUserARR['loggedIn'] != TRUE)
				redirect('new-login');
				
		$this->yprintARR($currUserARR, "User DATA ", 0);    // DBG
		
		$userTYPE = $currUserARR['UserGroup']; 
		switch($userTYPE)
		{
			default:
				$pageFL = "pages_v2_4/category/pg_display_category_v2_4" ;
				break;
				
			case 1:						/* Admin */
				$pageFL = "pages_v2_4/category/pg_display_category_v2_4_admin" ;
				break;

			case 2:						/* Operations */
				$pageFL = "pages_v2_4/category/pg_display_category_v2_4" ;
				break;

			case 4:						/* Controller */
				$pageFL = "pages_v2_4/category/pg_display_category_v2_4" ;
				break;
		}

		$pageFL = "pages_v2_4/category/pg_display_category_test_v2_4" ;
		
		
		$data['sessionUserREC'] = $currUserARR;
		
//		$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= $pageFL; // "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_category_by_id($i_recID);   

//			$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Category UPDATE INFO v2_4", 0);    	// DBG
			if(!empty($resultsARR))
			{
			$data['categoryREC']	= $resultsARR;
			$data['page'] 			= $pageFL;
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Category '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
			redirect($response_page);
			}
			
			
			
		}
		
	}											// EO Display Category Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_category_full_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-category" view file.
 		This will allow users to create and enter a new category
 		as requested.
 		
 		This will also show a full list of what users have access
 		under this Category.  
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_category_full_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();
		$response_page		= 'show-categories';
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG

		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
				
		
	/*...............Determine User Allowance.........*/
//		$this->yprint("Showing NEW Category FULL ",'user->ev_display_cat_full',0);		// DBG
				
		$currUserARR = $this->session->all_userdata();
		
		if(empty($currUserARR['loggedIn']))
			{
				$this->yprint("Session Not found ",'user->ev_display_cat_full',0);		// DBG
				$this->my_set_flash_msg('',0,'message', 
						'Your Session has been closed. Please log in again.');
//				redirect('new-login');
			}	
		if($currUserARR['loggedIn'] != TRUE)
			{
				$this->yprint("Session Not Logged In ",'user->ev_display_cat_full',0);		// DBG
				$this->my_set_flash_msg('',0,'message', 
						'Your Session has expired. Please log in again.');
//				redirect('new-login');
			}	

				
//		$this->yprintARR($currUserARR, "User DATA ", 0);    // DBG
		
		
		$userTYPE = $currUserARR['UserGroup']; 
		switch($userTYPE)
		{
			default:
//				$pageFL = "pages_v2_4/category/pg_display_category_full_v2_4" ;
				$pageFL = "pages_v2_4/category/pg_display_category_full" ;
				break;
				
			case 1:						/* Admin */
				$pageFL = "pages_v2_4/category/pg_display_category_full_v2_4_admin" ;
				break;

			case 2:						/* Operations */
				$pageFL = "pages_v2_4/category/pg_display_category_full_v2_4" ;
				break;

			case 4:						/* Controller */
				$pageFL = "pages_v2_4/category/pg_display_category_full_v2_4" ;
				break;
		}

//		$pageFL = "pages_v2_4/category/pg_display_category_full" ;
		$pageFL = "pages_v2_4/category/pg_display_category_test_v2_4" ;
		
		$data['sessionUserREC'] = $currUserARR;
		$resp = $this->rsc_set_landing_page('display-category-full/' . $i_recID,'dpcat',0);

		
		if($roleREC['viewDisabledYN'] == 'Y')	
				$viewDisabledYN = FALSE;
			else 
				$viewDisabledYN = TRUE;
		
		
//		$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_category_by_id($i_recID);   

//			$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Category UPDATE INFO v2_4", 0);    	// DBG
			if(!empty($resultsARR))
			{

			/*...............Get List of Associated Documents..........*/	
			$documentsARR = $this->main_model_v2_4->md_get_documents_by_category($i_recID,$viewDisabledYN,'DESC');   
				
			$subcatsARR = $this->main_model_v2_4->md_get_subcats_by_category($i_recID,$viewDisabledYN,'ASC');   
			
//			$this->yprintARR($documentsARR, "Documents INFO v2_4", 0);    	// DBG
			
			$data['documentsARR']		= $documentsARR;
			$data['subcatsARR']			= $subcatsARR;
			
			/*................Get List of Associated Users.........*/	

//			$this->yprintARR($roleREC, "ROLE REC NFO v2_4", 0);    	// DBG
			
			if($roleREC['viewDisabledYN'] == 'Y')	
				$viewDisabledYN = FALSE;
			else 
				$viewDisabledYN = TRUE;
			
			
			$accessListARR			= array();
			$usersHLDARR			= array();
			$categoryID				= $i_recID;	
			$wrkARR 				= $this->main_model_v2_4->md_get_access_users_in_category($categoryID,$viewDisabledYN,'DESC');
			$usersARR				= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$cntxx					= 0;

			
			foreach ($wrkARR as $workREC)
			{
			$wUID			   						= $workREC['UID'];	

//			$this->yprintARR($workREC, "AccessList", 0);    	// DBG
			
			foreach ($usersARR as $userREC)
				{
				if($userREC['UID'] == $wUID)
				{
					
//				$accessListARR[$cntxx]['FirstName']     = $usersHLDARR[$wUID]['FirstName'];	
//				$accessListARR[$cntxx]['LastName']      = $usersHLDARR[$wUID]['LastName'];	
//				$this->yprintARR($userREC, "UserRECMatch", 0);    	// DBG
					
				$accessListARR[$cntxx]      			= $userREC;	
				$cntxx++;
				}
				}
			}
			
//			$this->yprintARR($accessListARR, "UserList", 0);    	// DBG
			
			$data['usersARR'] 				= $accessListARR;

//			$this->yprintARR($roleREC, "ROLE REC NFO v2_4", 0);    	// DBG
			
			if($roleREC['viewDisabledYN'] == 'Y')	
				$viewDisabledYN = FALSE;
			else 
				$viewDisabledYN = TRUE;
			
			
			/*................GET Reference Tables.................*/	
				
			$categoryID	= $i_recID;	
			$data['AccessList'] 				= $this->main_model_v2_4->md_get_access_users_in_category($categoryID,$viewDisabledYN,'DESC');
			
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
				
//			$data['UsersARR'] 				= $this->main_model_v2_4->md_get_users_in_category($i_recID);
			
				
			$data['categoryREC']	= $resultsARR;
//			$data['page'] 			= "pages_v2_4/category/pg_display_category_full_v2_4";
			$data['page'] 			= $pageFL;
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Category '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
			redirect($response_page);
			}
			
		}										// If ResultsARR not empty
		
	}											// EO Display Category Function
	
	
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_category_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display category.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_category_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();
		$response_page		= 'show-categories';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;


		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
	/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		$this->yprint("DeleteThisCategory REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_category_by_id($i_recID);   

//			$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Category UPDATE INFO v2_4", 0);    	// DBG
			
			$data['categoryREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/category/pg_deletethis_category_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Category Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-category"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_category_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
//		$rec_ID_to_update = $this->uri->segment(3);

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
     	/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifyCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Modify Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
			
		/*...................Get Role Information.........PRSC */		
		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG
		
		/*...............Get Ref tables...................PRSC */
		
		$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
		
		/*...............Get Data REC details.............PRSC */
				
		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;
		}
		else
		{								// Error - Reshow Page
			$data['page'] = 'pages_v2_4/category/pg_change_category_v2_4';
			$this->load->view('template/master', $data);
		}

		$this->yprint("Change Category REC [ " . $rec_ID_to_update . "]",'',0);

		$data['categoryREC'] = $this->main_model_v2_4->md_get_category_by_id($rec_ID_to_update);

		$data['page'] = 'pages_v2_4/category/pg_change_category_v2_4';
		$this->load->view('template/master', $data);
		
		
	}												// EO Update Category Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_category_v2_4()
	{
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
		$response_page			= 'show-categories';
		
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$post['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifyCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Update Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

//		$this->yprint("UPDATE Category REC [ " . $rec_ID_to_update . "]",'',0);
		$this->yprintARR($post, "UPDATE Category INFO v2_4", 0);    // DBG


		if(empty($post))
		{
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('CID', 'Category ID', 'required|numeric');
			$this->form_validation->set_rules('Name', 'Category Name', 'required');
			$this->form_validation->set_rules('CategoryDesc', 'Category Desc', '');
			
			$rec_ID_to_update = $post['CID'];

			/*...............Get Ref tables...................PRSC */
		
			$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
			
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_category($post))
				{
					$this->my_set_flash_msg('',0,'message', 
							'The Category record has been updated successfully.');
//					redirect('change-category/' . $rec_ID_to_update);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
							'There was an error updating the Category record, please try again.');
//					redirect('change-category/' . $rec_ID_to_update);
					redirect($response_page);
				}
			}
			else
			{						/* TODO Validation Failed - Redisplay */
				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('change-category/' . $rec_ID_to_update);
				redirect($response_page);
				
			}
		}
		
	}												// EO Update Category Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_category_v2_4()
	{
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Create Category Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Ref tables...................PRSC */
		
		$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
		
		
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionUserREC['Email'];
		$post['sessionUserREC'] 	= $sessionUserREC;
		$post['roleREC'] 		= $roleREC;
		
		
//		$this->yprint("UPDATE Category REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprintARR($post, "CREATE Category INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			$data['page'] = 'pages_v2_4/category/pg_add_category_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('CID', 'Category ID', 'required|numeric');
			$this->form_validation->set_rules('Name', 'Category Name', 'required');
			$this->form_validation->set_rules('CategoryDesc', 'Category Desc', '');
			
//		$rec_ID_to_update = $post['CID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_category($post);
//   			$this->yprint("NEW Category REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "Category Record Created Successfully";
				$this->my_set_flash_msg('',0,'success', 'Category was created.');

				// Create a brand new document holding directory 
    			$nwpath = "uploads/product";
				$nwpath = 'C:\EMOUploads';
//    			$nwpath = realpath('C:/inetpub/wwwroot/hrm_uploads/EMO/');

//				$recID = 199;						// DBG
				$nwNewDIR = $nwpath . '\\' . $recID;
//    			print "KKK [" . $nwNewDIR . "]";
//			    $resp = mkdir($nwNewDIR,0755,TRUE);
//	   			$this->yprint("RESP A [ " . $resp . "]",'',0);   // DBG
			    
    			$resp = is_dir($nwNewDIR);
//	   			$this->yprint("RESP B [ " . $resp . "]",'',0);   // DBG
    			if($resp != true) //create the folder if it's not already exists
			    {
//		   			$this->yprint("NEW Category REC [ " . $nwNewDIR . "]",'',0);   // DBG
					$resp = mkdir($nwNewDIR,0755,TRUE);
					$this->my_set_flash_msg('',0,'message', 
				  		'Directory [' . $recID . '] was Successfully created. Code [' . $resp . "]");
			    }
   				else
   				{						// Directory exists - Warn user
	   				$this->yprint("NEW Category REC [ " . $nwNewDIR . "] EXISTS",'',0);   // DBG
   					$this->my_set_flash_msg('',0,'message', 
						'WARNING - Base Directory already exists');
   				}
				
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_category" web page.
			
			$post['categoryREC'] 		= $this->main_model_v2_4->md_get_category_by_id($recID);

			$post['page'] = "pages_v2_4/category/pg_display_category_v2_4";
			$this->load->view('template/master', $post);
			}
		 else
		 	{ 	
			$this->my_set_flash_msg('',0,'error', 'Error Creating Category Record.');
			$data['data_state'] = "Error Creatng Category Record";
		 	}
		}									// If Form validation is true-false
		else 
		{									// Error in data entry
			$post['roleREC'] 		= $roleREC;
			$this->my_set_flash_msg('',0,'error', 'Data Entry Error');
			$post['data_state'] = "Error Creatng Some Category fields";
			$post['page'] = "pages_v2_4/catagory/pg_add_category_v2_4";
			$this->load->view('template/master', $post);
			
		}
		
	}										// EO If valid data post
		
}											// EO Create Category Function
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_disable_category_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This will disable or enable a specified record disableYN flag.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-14		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_disable_category_v2_4_CNIU($recID = NULL)
	{
		/*...............Get Ref tables...................PRSC */
		
		$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
		

		$this->yprint("Disable Category Request","user->start_login()",0);
		$data['page'] = "pages_v2_4/pg_not_available_v2_4";
		$this->load->view('template/master', $data);
   	
}														// EO disable_category FUNC


	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	flipit_category_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a access record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_flipit_category_v2_4()
	{
		$data['data_state'] = "";
		$response_page		= 'show-categories';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		$processYN 				= TRUE;
		$esc_module				= "display-access";

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
//				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
//				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
//			redirect('new-login');
			exit;
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable-Disable Category Records.');
			redirect('new-login');
			
			}
		}
		
		/*...............Get Data REC details.........PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		
//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

				
		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That Access Confirmed to Delete Else Bail..........*/

		if(empty($post['mod_state']))
		{
				$this->my_set_flash_msg('',0,'message', 'Mod Aborted.' . $post['CID']);
				redirect($response_page);
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('CID', 'Category ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$recID_to_disable = $post['CID'];
			$i_recID = $recID_to_disable;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
//			$this->yprintARR($post, "Disable Category v2_4", 0);    // DBG
				
			if($recID_to_disable)
				{
				// Switch to Choice for generic
				switch($post['mod_state'])
				{
					default:
						
						break;
					
					case 2:
						$post['Active']  = 2;
						$resp = $this->main_model_v2_4->md_flip_category($post);
						$nstate = 'Enabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
						
					case 1:
						$post['Active']  = 1;
						$resp = $this->main_model_v2_4->md_flip_category($post);
						$nstate = 'Disabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
					
				}	

				
				if ($resp)
				{
				$this->my_set_flash_msg('',0,'message', 'Category '. $recID_to_disable . ' has been ' . $nstate);
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Category '.$recID_to_disable.' could not be altered. Please call tech support.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}

}											// EO FlipIt Func
		
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_disable_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-access"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_disable_category_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_category_v2_4($i_recID,1);
	
	}

	public function ctr_enable_category_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_category_v2_4($i_recID,2);
	
	}
	
	
	public function ctr_flip_category_v2_4($i_recID = NULL,$mod_state = NULL)
	{
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
//		$this->yprint("FlipThisCategory REC [ " . $i_recID . "]",'',0);		// DBG
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable/Disable Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
			
			/*.........Get Access list...................-PRSC */
			$resultsARR = $this->main_model_v2_4->md_get_category_by_id($i_recID);   
			
//			$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Access UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();

			$data['mod_state']			= $mod_state;
			
			$data['categoryREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/category/pg_flipthis_category_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display ENAGB YN Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_31
 
 		This function is the controller->director function that 
 		processes the create request to display the "update-category"
 		view.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr2_create_category_v2_4()
	{	
		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();
		
		$data['data_state'] 	= "";
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$post['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Create Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		
		$processYN 				= TRUE;
		$esc_module				= "display-category-full";
		$post = $this->input->post(NULL,TRUE);

//		$this->yprint("CREATE Category REC [ " . $i_recID . "]",'',0);
		$this->yprintARR($post, "Category CREATE INFO v2_4", 0);    // DBG

		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		
		$post['CreatedBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

					
		$file_data = array();

//		$this->form_validation->set_rules('CID', 'Category ID', 'required|numeric');
		$this->form_validation->set_rules('Name', 'CategoryShortName', 'required');
		$this->form_validation->set_rules('Desc', 'CategoryDesc', 'required');
		
		if ($this->form_validation->run() == true)
				$process = TRUE;
					
						
		/*........Determine if to create or reject.........*/		
				
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		  $this->my_set_flash_msg('',0,'error', 'Form Requires Data. Please Re-enter Fields Correctly.');
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			$resp = $this->main_model_v2_4->md_create_category($post);
			
			if($resp > 0)	
			{
					$data['data_state'] = "Category Record Created Successfully";
					$this->my_set_flash_msg('',0,'success', 'Category was created.');
					
					// Create new Directory that will hold the documents associated
					
//					$imgPATH = 'C:\EMOuploads';
//					$newDRC	 = $imgPATH . "\$";
					
			} else { 	
					$this->my_set_flash_msg('',0,'error', 'Error Creating Category Record.');
					$data['data_state'] = "Error Creatng Category Record";
			}
		}

		// This goes to database to get the data used to initialize the 
		// boxes in the "pg_add_category" web page.
			
		$data['categoryREC'] 		= $this->main_model_v2_4->md_get_category_by_id($id);

		$post['page'] = "pages_v2_4/category/pg_display_category_v2_4";
		$this->load->view('template/master', $post);
		
}
	
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_category_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a category record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteCNIU_category_v2_4($i_recID = NULL)
	{
		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "display-category-full";
		$response_page			= 'show-categories';

//		$this->yprint("CREATE Category REC [ " . $i_recID . "]",'',0);
		$this->yprintARR($post, "Category CREATE INFO v2_4", 0);    // DBG

		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		
//		$post['CreatedBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

//		$rec_ID_to_delete = $this->input->post('CID');
		$rec_ID_to_delete = $i_recID;

		$this->yprint("DELETE Category REC [ " . $i_recID . "]",'',0);
		
		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		
		if($rec_ID_to_delete)
		{
			if ($this->main_model_v2_4->md_delete_category($rec_ID_to_delete))
			{
				$this->my_set_flash_msg('',0,'message', 'Category '. $rec_ID_to_delete . ' has been removed.');
				$this->yprint("DELETE Success [ " . $i_recID . "]",'',0);
				
				redirect($response_page);
			}
			else
			{
				$this->my_set_flash_msg('',0,'message', 'Category '.$rec_ID_to_delete.' could not be removed, please try again.');
				$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}
	}
		

	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_category_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a category record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_category_v2_4()
	{
		$data['data_state'] = "";
		$response_page			= '';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		
		$processYN 				= TRUE;
		$esc_module				= "display-category-full";

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*...............Get Ref tables...................PRSC */
		
		$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Category Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.............PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That User Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->my_set_flash_msg('',0,'message', 'Delete Aborted.' . $post['CID']);
				redirect($response_page);
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('CID', 'Category ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['CID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_category($rec_ID_to_delete))
				{
					$this->my_set_flash_msg('',0,'message', 'Category '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Category '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}

}											// EO Deleteit Func
		
	
	
	
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_categories
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available categories on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_categories_v2_4()
	{

		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
			
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['viewCatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to List Categories.');
			redirect('new-login');
			}
		}
		
	
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.............PRSC */

		// This shows a list of the Risks for users to reference
		$resultsARR							= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'ASC');
		
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['categoriesARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/category/pg_show_categories_v2_4";
		$this->load->view('template/master', $data);
		
	}	
		
	

	
//<<<<<----------------------------------------------------[ EO Categories ]


//------------------------------------------------( SO Document Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-document" view file.
 		This will allow documents to create and enter a new document
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_document_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		
		
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
	/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Add Document Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
			
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//			$data['DocumentTypesARR'] 			= $this->main_model_v2_4->md_get_document_types($viewDisabled,'DESC');
						
//			$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/document/pg_add_document_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('DocID', 'Document ID', 'required');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('DocumentTypesID', 'Document Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('DocumentDesc', 'Document Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_document($post))
				{
					$this->my_set_flash_msg('',0,'message',
						 'The Document record has been added successfull added.');
					redirect('display-document/'.$post['REG_NUM']);
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
						'There was an error adding the Document record, please try again.');
					redirect('display-document');
				}
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('display-document');
			}
		}
		
	}										// EO Add Document Function

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_document_to_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-document" view file.
 		This will allow documents to create and enter a new document
 		as requested.
 		
 		This is much the same as add_document accept it knows the
 		category that user is adding too, allowing us to include
 		the sub-category as a selection.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_document_to_cat_v2_4($i_catID = NULL)
	{	
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		
		
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
	/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Add Document Records.');
			redirect('new-login');
			}
		}
		
		
		/*...............Double Check inputs............PRSC */
		
		if ($i_catID > 0)
		{
			$catID     		= $i_catID;
		}
		else 
		{
			$catID			= 0;
		}

		// Get the Category Detals to past to Display
		
		if($catID > 0)
		{
			$data['use_CATID']    = $catID;
			$catREC					= array();
			$catREC	= $this->main_model_v2_4->md_get_category_by_id($catID);
			
			if(!empty($catREC))
				$data['categoryREC']	= $catREC;
			
		}

		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		$this->yprintARR($post, "Add Doc to Cat ", 0);    // DBG

		// Get Necessary Reference Data used in Screen
		$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN ,'ASC');
//		$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
		$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//		$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types($viewDisabled,'DESC');
		$data['SubcatsARR'] 		= $this->main_model_v2_4->md_get_subcats_by_category($catID, $viewDisabledYN ,'ASC');
			
//			$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
		$data['page'] = "pages_v2_4/document/pg_add_document_to_cat_v2_4";
		$this->load->view('template/master', $data);
		
	}										// EO Add Document Function
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_document_UL_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-document" view file.
 		This will allow documents to create and enter a new document
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_document_UL_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Document Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
	
		/*...............Get Data REC details.............PRSC */
		
		
		
//		$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_document_by_id($i_recID);
//		$this->yprintARR($resultsARR, "DocumentDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['documentREC']			= $resultsARR;

			// Set Document ID in the event uploader is called
			$this->session->set_userdata('currCID', $resultsARR['CID']);	
			$this->session->set_userdata('currDocID', $i_recID);	
			$this->session->set_userdata('currVERS', $resultsARR['Version']);	
			
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['SubcatsARR'] 		= $this->main_model_v2_4->md_get_subcats($viewDisabledYN,'DESC');
			//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();

			
			
			$data['page'] = "pages_v2_4/document/pg_display_document_UL_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Document '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-documents');
			}
			
		}
		
	}											// EO Display Document UL Function
	
	
		
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-document" view file.
 		This will allow documents to create and enter a new document
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_document_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Document Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
		
		
		
//		$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_document_by_id($i_recID);
//		$this->yprintARR($resultsARR, "DocumentDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['documentREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
			
			$data['page'] = "pages_v2_4/document/pg_display_document_UL_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Document '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-documents');
			}
			
		}
		
	}											// EO Display Document Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-document"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_document_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
//		$rec_ID_to_update = $this->uri->segment(3);

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
	/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifyDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Modify Document Records.');
			redirect('new-login');
			}
		}
		

		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.............PRSC */
		
		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//			$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types($viewDisabled,'DESC');
			
		}
		else
		{								// Error - Reshow Page
			$this->my_set_flash_msg('',0,'error', 
						'No valid record ID specified. Please try again');
			$data['page'] = 'pages_v2_4/document/pg_change_document_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_document_by_id($rec_ID_to_update);
		$this->yprintARR($resultsARR, "DocumentDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change Document REC [ " . $i_recID . "] has been Updated",'',0);
//			$this->my_set_flash_msg('',0,'message', 
//						"Change Document REC [ " . $i_recID . "] has been Updated");

			// Get The Reference for Sub-Categories
			
			$CID 		= $resultsARR['CID'];
//			$data['SubcatsARR'] 		= $this->main_model_v2_4->md_get_subcats_by_category($CID);
			$data['SubcatsARR'] 		= $this->main_model_v2_4->md_get_subcats_by_category($CID, $viewDisabledYN ,'DESC');
			
			$data['documentREC']  = $resultsARR;
			$data['page'] = 'pages_v2_4/document/pg_change_document_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->my_set_flash_msg('',0,'error', 
						'Data for that Document not Available. Please try again');
			$data['page'] = 'pages_v2_4/document/pg_change_document_v2_4';
			redirect('show-documents');
		}
		
	}												// EO Update Document Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_document_v2_4()
	{
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$post['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifyDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Update Document Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionUserREC['Email'];
		
//		$this->yprint("UPDATE Document REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionUserREC['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
//		$this->yprintARR($post, "UPDATE Document INFO v2_4", 0);    // DBG
		
		
		if(empty($post))
		{
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('DocID', 'Document ID', 'required|numeric');
			$this->form_validation->set_rules('DocShortName', 'Document Name', 'required');
			$this->form_validation->set_rules('Version', 'Version Number', 'required');
			//			$this->form_validation->set_rules('DocumentDesc', 'Document Desc', '');
			
			$rec_ID_to_update = $post['DocID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_document($post))
				{
					$this->my_set_flash_msg('',0,'message', 
							'The Document record has been updated successfully.');
					redirect('show-documents');
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
							'There was an error updating the Document record, please try again.');
//					redirect('change-document/' . $rec_ID_to_update);
					redirect('show-documents');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->my_set_flash_msg('',0,'error', 
							'Incorrect Document Data Entered. Please try again.');
//				$this->my_set_flash_msg('',0,'error', validation_errors());

			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/document/pg_change_document_v2_4";
			$this->load->view('template/master', $data);
				
//				redirect('change-document/' . $rec_ID_to_update);
//				redirect('show-documents');
				
			}
		}
		
	}												// EO Update Document Function
	

/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_disable_document_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This will disable or enable a specified record disableYN flag.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-14		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_disable_document_v2_4_CNIU($recID = NULL)
	{

		$this->yprint("Disable Category Request","document->start_login()",0);
		$data['page'] = "pages_v2_4/pg_not_available_v2_4";
		$this->load->view('template/master', $data);
   	
}														// EO disable_document FUNC
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	flipit_document_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a access record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_flipit_document_v2_4()
	{
		$data['data_state'] = "";
		
		$response_page		= 'show-documents';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		$processYN 				= TRUE;
		$esc_module				= "display-access";

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable-Disable Document Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.........PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		
//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

				
		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That Document Confirmed to Delete Else Bail..........*/

		if(empty($post['mod_state']))
		{
				$this->my_set_flash_msg('',0,'message', 'Mod Aborted.' . $post['DocID']);
				redirect($response_page);
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('DocID', 'Document ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$recID_to_disable = $post['DocID'];
			$i_recID = $recID_to_disable;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
//			$this->yprintARR($post, "Disable Document v2_4", 0);    // DBG
				
			if($recID_to_disable)
				{
				// Switch to Choice for generic
				switch($post['mod_state'])
				{
					default:
						
						break;
					
					case 2:
						$post['Active']  = 2;
						$resp = $this->main_model_v2_4->md_flip_document($post);
						$nstate = 'Enabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
						
					case 1:
						$post['Active']  = 1;
						$resp = $this->main_model_v2_4->md_flip_document($post);
						$nstate = 'Disabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
					
				}	

				
				if ($resp)
				{
				$this->my_set_flash_msg('',0,'message', 'Document '. $recID_to_disable . ' has been ' . $nstate);
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Document '.$recID_to_disable.' could not be altered. Please call tech support.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}

}											// EO FlipIt Func
		
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_disable_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-access"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_disable_document_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_document_v2_4($i_recID,1);
	
	}

	public function ctr_enable_document_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_document_v2_4($i_recID,2);
	
	}
	
	
	public function ctr_flip_document_v2_4($i_recID = NULL,$mod_state = NULL)
	{
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
//		$this->yprint("FlipThisDocument REC [ " . $i_recID . "]",'',0);		// DBG
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable/Disable Document Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
			
			/*.........Get Document list...................-PRSC */
			$resultsARR = $this->main_model_v2_4->md_get_document_by_id($i_recID);   
			
//			$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Document UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();

			$data['mod_state']			= $mod_state;
			
			$data['documentREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/document/pg_flipthis_document_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display ENAGB YN Function
	



	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_document_v2_4()
	{
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG

		$response_page			= 'show-documents';
		
		/*...........Fetch User Session Info-PRSC */
		
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
		{
		$post['roleREC'] 		= $roleREC;
		$data['roleREC'] 		= $roleREC;
		}
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Create Document Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionUserREC['Email'];
		$post['sessionUserREC'] 	= $sessionUserREC;

		/*...............Get Data REC details.............PRSC */
		//		$this->yprint("UPDATE Document REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprintARR($post, "CREATE Document INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			// TODO Might need to grab Reference Info for Screen here first PRSC
			
			$data['page'] = 'pages_v2_4/document/pg_add_document_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('DocID', 'Document ID', 'required|numeric');
			$this->form_validation->set_rules('DocShortName', 'Document Name', 'required');
			$this->form_validation->set_rules('Version', 'Version Number', 'required');
			
//			$this->form_validation->set_rules('DocumentDesc', 'Document Desc', '');
			
//		$rec_ID_to_update = $post['DocID'];

			if(!empty($post['CID']))	
				$CID = $post['CID'];
			else 
				{				// Possible Attempt to hack - should always be CID - PRSC
				$this->my_set_flash_msg('',0,'breach', 
						'You do not have CID Authority to Create Document Records.');
				redirect('new-login');
				}		
			
		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_document($post);
//   			$this->yprint("NEW Document REC [ " . $recID . "]",'',0);   // DBG

//			$post['roleREC'] 		= $roleREC;
			$post['roleREC'] 		= $roleREC;
			
			if($recID)	
			{
				$data['data_state'] = "Document Record Created Successfully";
				$this->my_set_flash_msg('',0,'success', 'Document was created.');

				/* Notify User to confirm creation */
				$this->my_set_flash_msg('',0,'success', 
					'Document [' . $recID . '] Created Successfully.');
				
				
			$this->session->set_userdata('currCID', $CID);	
			$this->session->set_userdata('currDocID', $recID);	
			if(!empty($post['Version']))
				$this->session->set_userdata('currVERS', $post['Version']);	
			else
				$this->session->set_userdata('currVERS', "1_0");	
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_document" web page.
			
				$documentREC 			= $this->main_model_v2_4->md_get_document_by_id($recID);
				$post['documentREC']   = $documentREC;
//				$this->yprintARR($documentREC, "DOCUMENT REC", 0);    // DBG
				
				
				
			// Get Necessary Reference Data used in Screen
				$post['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,"DESC");
//				$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
				$post['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//				$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types($viewDisabled,'DESC');

				$CID = $documentREC['CID'];
				$SubcatsARR = $this->main_model_v2_4->md_get_subcats_by_category($CID,$viewDisabledYN,"DESC");				
				$post['SubcatsARR']      = $SubcatsARR;
				$post['page'] = "pages_v2_4/document/pg_display_document_UL_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$this->my_set_flash_msg('',0,'error', 'DB Error Creating Document Record.');
				$data['data_state'] = "Error Creatng Document Record";
				redirect($response_page);
			}
			
		}	
		else 
		{									// Error in data entry
			$this->my_set_flash_msg('',0,'error', 'Error Creating Document Record.');
			$post['data_state'] = "Error Creatng Document Record";
			
			$post['roleREC'] 		= $roleREC;

			// Get Necessary Reference Data used in Screen
			$post['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$post['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//			$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types($viewDisabledYN,'DESC');
			
			$post['page'] = "pages_v2_4/document/pg_add_document_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create Document Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_document_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display document.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_document_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		$this->yprint("DeleteThisDocument REC [ " . $i_recID . "]",'',0);		// DBG


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Document Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.............PRSC */
		
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_document_by_id($i_recID);   

//			$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Document UPDATE INFO v2_4", 0);    	// DBG

			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabled,'DESC');
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
						
			$data['documentREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/document/pg_deletethis_document_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Document Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_document_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a document record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_document_v2_4()
	{
		$data['data_state'] = "";
		$response_pages			= 'show-documents';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		$processYN 				= TRUE;
		$esc_module				= "display-document";

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Document Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.............PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That Document Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->my_set_flash_msg('',0,'message', 'Delete Aborted.' . $post['DocID']);
				redirect($response_page);
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('DocID', 'Document ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['DocID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_document($rec_ID_to_delete))
				{
					$this->my_set_flash_msg('',0,'message', 'Document '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Document '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}

}											// EO Deleteit Func
		
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_documents_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available documents on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_documents_v2_4()
	{

		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['viewDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to List Documents.');
			redirect('new-login');
			}
		}

		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
	
		/*...............Get Data REC details.............PRSC */
				
		// Get Necessary Reference Data used in Screen
		$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
		$data['SubcatsARR'] 		= $this->main_model_v2_4->md_get_subcats($viewDisabledYN,'DESC');
//		$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
		$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//		$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types($viewDisabledYN,'DESC');
		
		// This shows a list of the Risks for documents to reference
		$resultsARR							= $this->main_model_v2_4->md_get_documents($viewDisabledYN,'ASC');
		
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['documentsARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/document/pg_show_documents_v2_4";
		$this->load->view('template/master', $data);
		
	}	
	
//------------------------------------------------( EO Document Functions Block )

	

//------------------------------------------------( SO Access Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-access" view file.
 		This will allow accesses to create and enter a new access
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_access_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		
		
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
	    /*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Add Access Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
			
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['LastModBy']			= $sessionUserREC['Email'];
			$data['sessionUserREC'] 	= $sessionUserREC;
			
			
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
//			$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/access/pg_add_access_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('AID', 'Access ID', 'required');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('AccessTypesID', 'Access Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('AccessDesc', 'Access Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_access($post))
				{
					$this->my_set_flash_msg('',0,'message',
						 'The Access record has been added successfull added.');
					redirect('display-access/'.$post['REG_NUM']);
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
						'There was an error adding the Access record, please try again.');
					redirect('display-access');
				}
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('display-access');
			}
		}
		
	}										// EO Add Access Function


	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-access" view file.
 		This will allow accesses to create and enter a new access
 		as requested.
 		
 		ARGS:		$rec_ID (User ID)
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_access_to_user_v2_4($i_recID = NULL)
	{	
		$data['data_state'] 		= "";
		$response_page				= 'display-user';
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		
		
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
	    /*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Add Access Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
			
		
		//Load Model
//		$this->load->model('main_model_v2_4');
//		$post = $this->input->post(null, true);

		if($i_recID)
		{
			$resultsARR = $this->main_model_v2_4->md_get_user_by_id($i_recID);
//			$this->yprintARR($resultsARR, "UserDATA ", 0);    	// DBG

			$data['userREC']  = $resultsARR;
		
			$data['LastModBy']			= $sessionUserREC['Email'];
			$data['sessionUserREC'] 	= $sessionUserREC;
		
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
			
//			$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/access/pg_add_access_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('AID', 'Access ID', 'required');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('AccessTypesID', 'Access Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('AccessDesc', 'Access Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				$rec_ID = $this->main_model_v2_4->md_create_access($post);
				if ($rec_ID > 0)
				{
					$this->my_set_flash_msg('',0,'message',
						 'The Access record has been successfully added.');
					redirect($response_page . '/' . $rec_ID);
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
						'There was an error adding the Access record, please try again.');
					redirect($response_page . '/' . $rec_ID);
				}
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('add-access');
			}
		}
		
	}										// EO Add Access Function
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-access" view file.
 		This will allow accesses to create and enter a new access
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_access_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Access Records.');
			redirect('new-login');
			}
		}
		
	
		/*.....Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
				$viewDisabledYN = FALSE;
		else 
				$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.............PRSC */
//		$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_access_by_id($i_recID,$viewDisabledYN,'DESC');
//		$this->yprintARR($resultsARR, "AccessDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['accessREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
			$data['page'] = "pages_v2_4/access/pg_display_access_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Access '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-accesses');
			}
			
		}
		
	}											// EO Display Access Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-access"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_access_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		

		/*...............Get Data REC details.............PRSC */
		
//		$rec_ID_to_update = $this->uri->segment(3);
				
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG
		
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
				$viewDisabledYN = FALSE;
		else 
				$viewDisabledYN = TRUE;
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
				
		
		/*...............Get Data REC details.............PRSC */
		
		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
		}
		else
		{								// Error - No Record defined. Go back to show list.
			$this->my_set_flash_msg('',0,'error', 
						'No valid record ID specified. Please try again');
			redirect('show-accesses');		
			
			
			// CNIU Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
			
//			$data['page'] = 'pages_v2_4/access/pg_change_access_v2_4';
			$data['page'] = 'pages_v2_4/access/pg_change_access_limited_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_access_by_id($rec_ID_to_update,$viewDisabledYN,'DESC');
//		$this->yprintARR($resultsARR, "AccessDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change Access REC [ " . $i_recID . "] has been Updated",'',0);
			$this->my_set_flash_msg('',0,'message', 
						"Change Access REC [ " . $rec_ID_to_update . "] has been Updated");
			
			$data['accessREC']  = $resultsARR;
//			$data['page'] = 'pages_v2_4/access/pg_change_access_v2_4';
			$data['page'] = 'pages_v2_4/access/pg_change_access_limited_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->my_set_flash_msg('',0,'error', 
						'Data for that Access not Available. Please try again');
			$data['page'] = 'pages_v2_4/access/pg_change_access_v2_4';
//			redirect('show-accesses');
		}
		
	}												// EO Update Access Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	flipit_access_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a access record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_flipit_access_v2_4()
	{
		$data['data_state'] = "";
		$response_page			= 'show-accesses';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		$processYN 				= TRUE;
		$esc_module				= "display-access";

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable-Disable Access Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.........PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		
//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

				
		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That Access Confirmed to Delete Else Bail..........*/

		if(empty($post['mod_state']))
		{
				$this->my_set_flash_msg('',0,'message', 'Delete Aborted.' . $post['AID']);
				redirect($response_page);
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('AID', 'Access ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['AID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
			$this->yprintARR($post, "Flip IT v2_4", 0);    // DBG
				
			if($rec_ID_to_delete)
				{
				// Switch to Choice for generic
				switch($post['mod_state'])
				{
					default:
						
						break;
					
					case 2:
						$post['Active']  = 2;
						$resp = $this->main_model_v2_4->md_flip_access($post);
						$nstate = 'Enabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
						
					case 1:
						$post['Active']  = 1;
						$resp = $this->main_model_v2_4->md_flip_access($post);
						$nstate = 'Disabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
					
				}	

				
				if ($resp)
				{
				$this->my_set_flash_msg('',0,'message', 'Access '. $rec_ID_to_delete . ' has been ' . $nstate);
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Access '.$rec_ID_to_delete.' could not be altered. Please call tech support.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}

}											// EO FlipIt Func
		
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_disable_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-access"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_disable_access_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_access_v2_4($i_recID,1);
	
	}

	public function ctr_enable_access_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_access_v2_4($i_recID,2);
	
	}
	
	
	public function ctr_flip_access_v2_4($i_recID = NULL,$mod_state = NULL)
	{
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		$this->yprint("FlipThisAccess REC [ " . $i_recID . "]",'',0);		// DBG
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable/Disable Access Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
			
			/*.........Get Access list...................-PRSC */
			$resultsARR = $this->main_model_v2_4->md_get_access_by_id($i_recID,$viewDisabledYN,'DESC');   
			
//			$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Access UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();

			$data['mod_state']			= $mod_state;
			
			$data['accessREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/access/pg_flipthis_access_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display ENAGB YN Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_access_v2_4()
	{
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		/*..............Check Session Authorization.........PRSC */

		
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$post['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		$this->yprintARR($roleREC, "User ROLE Conditions", 0);    // DBG
		
		
		if(!empty($roleREC))
		{
			$data['roleREC'] 		= $roleREC;
		}
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifyAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Update Access Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionUserREC['Email'];
		
//		$this->yprint("UPDATE Access REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionUserREC['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
		$this->yprintARR($post, "UPDATE Access INFO v2_4", 0);    // DBG
		
		if(empty($post))
		{
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('AID', 'Access ID', 'required|numeric');
			$this->form_validation->set_rules('CID', 'Category', 'required');
			$this->form_validation->set_rules('UID', 'User', 'required');

//			$this->form_validation->set_rules('AccessDesc', 'Access Desc', '');
			
			if(!empty($post['AID']))
				$rec_ID_to_update = $post['AID'];
			else 
			{
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
			}
				
			
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
			
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_access($post))
				{
					$this->my_set_flash_msg('',0,'message', 
							'The Access record has been updated successfully.');
					
							/*...............Get Data REC details.............PRSC */
				
				// Get Necessary Reference Data used in Screen
//					$post['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//					$post['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
	
					$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
					$this->load->view('template/master', $data);
					
//					redirect('show-accesses');
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
							'There was an error updating the Access record, please try again.');
//					redirect('change-access/' . $rec_ID_to_update);
					redirect('show-accesses');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->my_set_flash_msg('',0,'error', 
							'Incorrect Access Data Entered. Please try again.');
//				$this->my_set_flash_msg('',0,'error', validation_errors());

				$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');

				$post['sessionUserREC'] 	= $sessionUserREC;
				$post['roleREC'] 			= $roleREC;
//				$post['page'] 	= "pages_v2_4/access/pg_change_access_v2_4";
				$post['page'] 	= "pages_v2_4/access/pg_change_access_limited_v2_4";
				$this->load->view('template/master', $post);
				
//				redirect('change-access/' . $rec_ID_to_update);
//				redirect('show-accesses');
				
			}
		}
		
	}												// EO Update Access Function
	


/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_disable_access_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This will disable or enable a specified record disableYN flag.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-14		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_disable_access_v2_4_CNIU()
	{

		$this->yprint("Disable Category Request","access->start_login()",0);
		$data['page'] = "pages_v2_4/pg_not_available_v2_4";
		$this->load->view('template/master', $data);
   	
}														// EO disable_access FUNC
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_access_v2_4()
	{
		$response_page			= 'show-accesses';
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
	
		
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Create Access Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']			= $sessionUserREC['Email'];
		$post['sessionUserREC'] 	= $sessionUserREC;
		$post['roleREC']			= $roleREC;		

			
		
//		$this->yprint("UPDATE Access REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprintARR($post, "CREATE Access INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//			$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types($viewDisabledYN,'DESC');
			
//			TODO this is no good - as UID is not in data entered- so no cancel button  PRSC		
//			$data['response_page']		= $response_page;
			$data['page'] = 'pages_v2_4/access/pg_add_access_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('AID', 'Access ID', 'required|numeric');
			$this->form_validation->set_rules('CID', 'Category', 'required');
			$this->form_validation->set_rules('UID', 'User', 'required');
			
//			$this->form_validation->set_rules('AccessDesc', 'Access Desc', '');
			
//		$rec_ID_to_update = $post['AID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_access($post);
//   			$this->yprint("NEW Access REC [ " . $recID . "]",'',0);   // DBG
			
			if(!empty($post['UID']))
					$UID = $post['UID'];
			if(!empty($sessionUserREC['landing_page']))
				$response_page = $sessionUserREC['landing_page'] . '/' . $UID;
			
			
			if($recID)	
			{
				/*..........Set View disabled True or False - PRSC  */
				if($roleREC['viewDisabledYN'] == 'Y')	
						$viewDisabledYN = FALSE;
				else 
						$viewDisabledYN = TRUE;
				
				$post['data_state'] = "Accesss Record Created Successfully";
				$this->my_set_flash_msg('',0,'success', 'Access was created.');
	
				// Get Necessary Reference Data used in Screen
				$post['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
				$post['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
				$post['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//				$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types();
				
				$post['response_page']		= $response_page;
				
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_access" web page.
			
				$post['accessREC'] 		= $this->main_model_v2_4->md_get_access_by_id($recID,$viewDisabledYN,'DESC');

				$post['page'] = "pages_v2_4/access/pg_display_access_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$post['LastModBy']			= $sessionUserREC['Email'];
				$post['sessionUserREC'] 	= $sessionUserREC;
			
				// Get Necessary Reference Data used in Screen
				$post['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
				$post['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
				$post['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
				
				$post['response_page']		= $response_page;
				
				$this->my_set_flash_msg('',0,'error', 'Error Creating Access Record.');
				$post['data_state'] = "Error Creatng Access Record";
				$post['page'] = "pages_v2_4/access/pg_add_access_v2_4";
				$this->load->view('template/master', $post);
				
			}
			
		}	
		else 
		{									// Error in data entry
			$post['roleREC'] 			= $roleREC;
			$post['LastModBy']			= $sessionUserREC['Email'];
			$post['sessionUserREC'] 	= $sessionUserREC;
			$post['response_page']		= $response_page;
			
			$this->my_set_flash_msg('',0,'error', 'Error Creating Access Record.');
			$post['data_state'] = "Error Creatng Access Record";
			$post['page'] = "pages_v2_4/access/pg_add_access_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create Access Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_access_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display access.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_access_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		$this->yprint("DeleteThisAccess REC [ " . $i_recID . "]",'',0);		// DBG
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Access Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
			
			/*.........Get Access list...................-PRSC */
			$resultsARR = $this->main_model_v2_4->md_get_access_by_id($i_recID,$viewDisabledYN,'DESC');   
			
			
//			$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Access UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
			$data['accessREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/access/pg_deletethis_access_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Access Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_access_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a access record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_access_v2_4()
	{
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		$processYN 				= TRUE;
		$esc_module				= "display-access";

		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Access Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.........PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That Access Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->my_set_flash_msg('',0,'message', 'Delete Aborted.' . $post['AID']);
				redirect('show-accesses');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('AID', 'Access ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['AID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_access($rec_ID_to_delete))
				{
					$this->my_set_flash_msg('',0,'message', 'Access '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-accesses');
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Access '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-accesses');
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-accesses');
			}
		}

}											// EO Deleteit Func
		
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_accesses_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available accesses on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_accesses_v2_4()
	{

		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['viewAccessYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to List Access Records.');
			redirect('new-login');
			}
		}
	
		
			/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
				
		// This shows a list of the Risks for accesses to reference
		$resultsARR							= $this->main_model_v2_4->md_get_accesses($viewDisabledYN,'DESC');

		/*....................Load REF Data..........PRSC*/
		$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
		$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
		$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
		
		
//	$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['accessesARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/access/pg_show_accesses_v2_4";
		$this->load->view('template/master', $data);
		
	}	
	
	
	

//------------------------------------------------( SO Subcat Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-subcat" view file.
 		This will allow subcats to create and enter a new subcat
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_subcat_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Add Sub-Category Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
			
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			$data['CategoriesARR'] 			= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			
//			$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/subcat/pg_add_subcat_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required');
			$this->form_validation->set_rules('SubcatShortName', 'Subcat Short Name', 'required');
			
//			$this->form_validation->set_rules('UserTypesID', 'Subcat Group', 'required');
//			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_subcat($post))
				{
					$this->my_set_flash_msg('',0,'message',
						 'The Subcat record has been added successfull added.');
					redirect('display-subcat/'.$post['REG_NUM']);
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
						'There was an error adding the Subcat record, please try again.');
					redirect('display-subcat');
				}
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('display-subcat');
			}
		}
		
	}										// EO Add Subcat Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-subcat" view file.
 		This will allow subcats to create and enter a new subcat
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_subcat_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
				
		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Sub-Category Records.');
			redirect('new-login');
			}
		}
		
	
		/*...............Get Data REC details.............PRSC */
		

//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		
//		$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_subcat_by_id($i_recID);
		$this->yprintARR($resultsARR, "SubcatDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['subcatREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
//			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			$data['CategoriesARR'] 			= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			
			$data['page'] = "pages_v2_4/subcat/pg_display_subcat_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Subcat '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-subcats');
			}
			
		}
		
	}											// EO Display Subcat Function
	

		
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_subcat_full_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-subcat" view file.
 		This will allow subcats to create and enter a new subcat
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_subcat_full_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
				
		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Display Sub-Category Records.');
			redirect('new-login');
			}
		}

		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
	
		/*...............Get Data REC details.............PRSC */
		

//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG
		
		
		
		
//		$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_subcat_by_id($i_recID);
//		$this->yprintARR($resultsARR, "SubcatDATA ", 0);    	// DBG
		
		
		if(!empty($resultsARR))
			{
			$data['subcatREC']			= $resultsARR;
//			$this->yprintARR($resultsARR, "Results ", 0);    // DBG
			$CID						= $resultsARR['CID'];
//			$this->yprint("CID Discovered [ " . $CID. "]",'',0);
			
			if($CID)
			{
			$CID						= $resultsARR['CID'];
			$this->yprint("CID Discovered2 [ " . $CID. "]",'',0);
			
			$SubcatsARR			 		= $this->main_model_v2_4->md_get_subcats_by_category($CID,$viewDisabledYN,"DESC");
//			$this->yprintARR($SubcatsARR, "SUBCATS ", 0);    // DBG
			$data['subcatsARR']			= $SubcatsARR;
			}
			
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
//			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			$data['CategoriesARR'] 			= $this->main_model_v2_4->md_get_categories($viewDisabledYN,"DESC");
			
			$data['page'] = "pages_v2_4/subcat/pg_display_subcat_full_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Subcat '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-subcats');
			}
			
		}
		
	}											// EO Display Subcat Function
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-subcat"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_subcat_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

				/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
//		$rec_ID_to_update = $this->uri->segment(3);
				
	/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifySubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Modify Document Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */

		// Get Necessary Reference Data used in Screen
		$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
//		$data['UserTypesARR'] 		= $this->main_model_v2_4->md_get_user_types();
		$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
				
		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			
		}
		else
		{								// Error - Reshow Page
			$this->my_set_flash_msg('',0,'error', 
						'No valid record ID specified. Please try again');
			$data['page'] = 'pages_v2_4/subcat/pg_change_subcat_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_subcat_by_id($rec_ID_to_update);
		$this->yprintARR($resultsARR, "SubcatDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change Subcat REC [ " . $i_recID . "] has been Updated",'',0);
//			$this->my_set_flash_msg('',0,'message', 
//						"Change Subcat REC [ " . $i_recID . "] has been Updated");
			
			$data['subcatREC']  = $resultsARR;
			$data['page'] = 'pages_v2_4/subcat/pg_change_subcat_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->my_set_flash_msg('',0,'error', 
						'Data for that Subcat not Available. Please try again');
			$data['page'] = 'pages_v2_4/subcat/pg_change_subcat_v2_4';
			redirect('show-subcats');
		}
		
	}												// EO Update Subcat Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_subcat_v2_4()
	{
		$data['data_state'] 		= "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$post['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['ModifySubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Update Sub-Category Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
		
		
		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionSubcatARR['Email'];
		
//		$this->yprint("UPDATE Subcat REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionSubcatARR['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
//		$this->yprintARR($post, "UPDATE Subcat INFO v2_4", 0);    // DBG
		
// Get Necessary Reference Data used in Screen
		$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
//		$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
		$data['CategoriesARR'] 			= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
		
		if(empty($post))
		{
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
		
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required');
			$this->form_validation->set_rules('SubcatShortName', 'Subcat Short Name', 'required');
			
//			$this->form_validation->set_rules('UserTypesID', 'Subcat Group', 'required');
//			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
						
			$rec_ID_to_update = $post['SCID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_subcat($post))
				{
					$this->my_set_flash_msg('',0,'message', 
							'The Subcat record has been updated successfully.');
					redirect('show-subcats');
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
							'There was an error updating the Subcat record, please try again.');
//					redirect('change-subcat/' . $rec_ID_to_update);
					redirect('show-subcats');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->my_set_flash_msg('',0,'error', 
							'Incorrect Subcat Data Entered. Please try again.');
//				$this->my_set_flash_msg('',0,'error', validation_errors());

			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_change_subcat_v2_4";
			$this->load->view('template/master', $data);
				
//				redirect('change-subcat/' . $rec_ID_to_update);
//				redirect('show-subcats');
				
			}
		}
		
	}												// EO Update Subcat Function
	

/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_disable_subcat_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	20170609
 * 
 * 		This will disable or enable a specified record disableYN flag.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-06-14		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */
	
	public function ctr_disable_subcat_v2_4_CNIU($recID = NULL)
	{

		$this->yprint("Disable Category Request","subcat->start_login()",0);
		$data['page'] = "pages_v2_4/pg_not_available_v2_4";
		$this->load->view('template/master', $data);
   	
}														// EO disable_subcat FUNC

	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	flipit_subcat_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a access record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_flipit_subcat_v2_4()
	{
		$data['data_state'] 	= "";
		$response_page 			= 'show-subcats';

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		$processYN 				= TRUE;
		$esc_module				= "display-access";

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable-Disable Subcat Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.........PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		
//		if($post['LastModBy'] !=  $sessionUserREC['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Change State IT v2_4", 0);    // DBG

				
		$post['LastModBy']   = $sessionUserREC['Email'];

		/*............Check That Subcat Confirmed to Delete Else Bail..........*/

		if(empty($post['mod_state']))
		{
				$this->my_set_flash_msg('',0,'message', 'Mod Aborted.' . $post['SCID']);
				redirect($response_page);
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$recID_to_disable = $post['SCID'];
			$i_recID = $recID_to_disable;

			$post['LastModBy']   = $sessionUserREC['Email'];
			
			if ($this->form_validation->run() == true)
			{
//			$this->yprintARR($post, "Disable Subcat v2_4", 0);    // DBG
				
			if($recID_to_disable)
				{
				// Switch to Choice for generic
				switch($post['mod_state'])
				{
					default:
						
						break;
					
					case 2:
						$post['Active']  = 2;
						$resp = $this->main_model_v2_4->md_flip_subcat($post);
						$nstate = 'Enabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
						
					case 1:
						$post['Active']  = 1;
						$resp = $this->main_model_v2_4->md_flip_subcat($post);
						$nstate = 'Disabled';
						$this->yprint("RESP [ " . $resp . "]",'',0);
						break;
					
				}	

				
				if ($resp)
				{
				$this->my_set_flash_msg('',0,'message', 'Subcat '. $recID_to_disable . ' has been ' . $nstate);
//					$this->yprint("Change State Successful [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Subcat '.$recID_to_disable.' could not be altered. Please call tech support.');
//					$this->yprint("Change State Failed [ " . $i_recID . "]",'',0);
					redirect($response_page);
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect($response_page);
			}
		}

}											// EO FlipIt Func
		
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_disable_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-access"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_disable_subcat_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_subcat_v2_4($i_recID,1);
	
	}

	public function ctr_enable_subcat_v2_4($i_recID = NULL)
	{
		$resp = $this->ctr_flip_subcat_v2_4($i_recID,2);
	
	}
	
	
	public function ctr_flip_subcat_v2_4($i_recID = NULL,$mod_state = NULL)
	{
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
//		$this->yprint("FlipThisSubcat REC [ " . $i_recID . "]",'',0);		// DBG
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DisableSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Enable/Disable Subcat Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			/*..........Set View disabled True or False - PRSC  */
			if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE;
			else 
					$viewDisabledYN = TRUE;
			
			/*.........Get Subcat list...................-PRSC */
			$resultsARR = $this->main_model_v2_4->md_get_subcat_by_id($i_recID);   
			
//			$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Subcat UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users($viewDisabledYN,'DESC');
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();

			$data['mod_state']			= $mod_state;
			
			$data['subcatREC']			= $resultsARR;
			$data['page'] 				= "pages_v2_4/subcat/pg_flipthis_subcat_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display ENAGB YN Function
	


	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_subcat_v2_4()
	{
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
		{
		$data['roleREC'] 		= $roleREC;
		}
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['AddSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Create Sub-Category Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.............PRSC */
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionUserREC['Email'];
		$post['sessionUserREC'] 	= $sessionUserREC;
		$post['roleREC'] 		= $roleREC;

		// Get Necessary Reference Data used in Screen
		$post['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
//		$post['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
		$post['CategoriesARR'] 			= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
		
		
//		$this->yprint("UPDATE Subcat REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprintARR($post, "CREATE Subcat INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			$data['page'] = 'pages_v2_4/subcat/pg_add_subcat_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required|numeric');
			$this->form_validation->set_rules('SubcatShortName', 'Subcat Short Name', 'required');
			
//			$this->form_validation->set_rules('UserTypesID', 'Subcat Group', 'required');
//			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
						
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
			
//		$rec_ID_to_update = $post['SCID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_subcat($post);
//   			$this->yprint("NEW Subcat REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "Subcat Record Created Successfully";
				$this->my_set_flash_msg('',0,'success', 'Subcat was created.');
	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_subcat" web page.
			
				$post['subcatREC'] 		= $this->main_model_v2_4->md_get_subcat_by_id($recID);

				$post['page'] = "pages_v2_4/subcat/pg_display_subcat_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$post['roleREC'] 		= $roleREC;
				
				$this->my_set_flash_msg('',0,'error', 'Error Creating Subcat Record.');
				$data['data_state'] = "Error Creatng Subcat Record";
				$post['page'] = "pages_v2_4/user/pg_add_subcat_v2_4";
				$this->load->view('template/master', $post);
			}
			
		}	
		else 
		{									// Error in data entry
			$this->my_set_flash_msg('',0,'error', 'Error Creating Subcat Record.');
			$post['data_state'] = "Error Creatng Subcat Record";
			$post['page'] = "pages_v2_4/subcat/pg_add_subcat_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create Subcat Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_subcat_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display subcat.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_subcat_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		
		$this->yprint("DeleteThisSubcat REC [ " . $i_recID . "]",'',0);		// DBG

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Sub-Category Records.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
		/*...............Get Data REC details.............PRSC */
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->my_set_flash_msg('',0,'error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_subcat_by_id($i_recID);   

//			$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Subcat UPDATE INFO v2_4", 0);    	// DBG

			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
//			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			$data['CategoriesARR'] 			= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
			
			$data['subcatREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/subcat/pg_deletethis_subcat_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Subcat Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_subcat_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a subcat record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_subcat_v2_4()
	{
		$data['data_state'] = "";

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;
		
		$processYN 				= TRUE;
		$esc_module				= "display-subcat";


		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['DeleteSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Delete Sub-Category Records.');
			redirect('new-login');
			}
		}
		
		/*...............Get Data REC details.............PRSC */
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionSubcatARR['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->my_set_flash_msg('',0,'message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionSubcatARR['Email'];

		/*............Check That Subcat Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->my_set_flash_msg('',0,'message', 'Delete Aborted.' . $post['SCID']);
				redirect('show-subcats');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->my_set_flash_msg('',0,'message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['SCID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionSubcatARR['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_subcat($rec_ID_to_delete))
				{
					$this->my_set_flash_msg('',0,'message', 'Subcat '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-subcats');
				}
				else
				{
					$this->my_set_flash_msg('',0,'message', 'Subcat '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-subcats');
				}
			}
				
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-subcats');
			}
		}

}											// EO Deleteit Func
		
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_subcats_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available subcats on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_subcats_v2_4()
	{

			$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['viewSubcatsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to List Sub-Categories.');
			redirect('new-login');
			}
		}
		
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		/*...............Get Data REC details.............PRSC */
				
		// Get Necessary Reference Data used in Screen
		$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
		$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
		
		// This shows a list of the Risks for subcats to reference
		$resultsARR							= $this->main_model_v2_4->md_get_subcats($viewDisabledYN,'ASC');
		
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['subcatsARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/subcat/pg_show_subcats_v2_4";
		$this->load->view('template/master', $data);
		
	}	
	
//------------------------------------------------( EO Subcat Functions Block )

	
	
	
/* 	
=============================================================================================
 signup() - Enables people to join and create new accounts using an email address.
---------------------------------------------------------------------------------------------
 Registration: Step 1 of 2
---------------------------------------------------------------------------------------------
 - validate submitted information
 - encrypt email address to generate activation code
 - check if that email adderss already exist in the system
 - add a new unactivated user to the database
 - send activation email with activation link that sends them to activation function below
 - redirect with confirmation message, otherwise redirect to  with error
=============================================================================================
*/
	public function ctr_signup()
	{
		if($data = $this->input->post(null, true))
		{
			$this->form_validation->set_rules('first_name', 'first name', 'trim|min_length[2]|required');
			$this->form_validation->set_rules('last_name', 'last name', 'trim|min_length[2]|required');
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
			// $this->form_validation->set_rules('password_confirmation', 'confirm password', 'trim|matches[password]|required');

			if($this->form_validation->run() != false)
			{
				$activation_code = $this->encrypt->encode($data['email_address']);

				// check if that email adderss already exist in the system
				if(!$this->user_model->email_address_exist($data['email_address']))
				{
					if($this->user_model->add_user($data))
					{
					    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
						$this->email->to($data['email_address']); 
					    $this->email->subject('Activate your account');
						// $this->email->message('<p>Hi '.$data['first_name'].', welcome to <strong>Help4Hire</strong>! </p><p>Click the link below to verify your email address and activate your account. </p><p><a href="'.base_url().'user/activation/'.$activation_code.'">'.base_url().'user/activation/'.$activation_code.'</a>');	
						$this->email->message('<p>Click the link below to verify your email address and activate your account. </p><p><a href="'.base_url().'user/activation/'.$activation_code.'">'.base_url().'user/activation/'.$activation_code.'</a>');	
					    // echo $this->email->print_debugger();

					    if($this->email->send())
					    {
							// $this->my_set_flash_msg('',0,'confirmation', 'Please check your email to complete the registration process and activate your account. <a href="'.base_url().'user/activation/'.$activation_code.'">Activate Now</a>');	
							$this->my_set_flash_msg('',0,'confirmation', 'Check your email to complete the registration process.');
							redirect('user-');
					    }
					    else 
					    {
					    	$this->my_set_flash_msg('',0,'error', 'Account was created but sending activation email failed.');
							redirect('user-signup');
						}					    
					}
					else
					{
						$this->my_set_flash_msg('',0,'error', 'Error creating user account.');
						redirect('user-signup');
					}
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 'An account with that email address already exists - try again or <a href="'.base_url().'user/">log in</a>');
					redirect('user-signup');
				} 
			}
		}
		else
		{
			// $data['page'] = 'pages_v2_4/content/signup';
			// $this->load->view('pages_v2_4/general', $data);	
			$this->load->view('pages_v2_4/signup');
		}

	}
/* 	
=============================================================================================
 activation() - Activates new user accounts.
---------------------------------------------------------------------------------------------
 Registration: Step 2 of 2
---------------------------------------------------------------------------------------------
 - decode activation code to get the email address
 - use the email address to activate the account and retrieve user data
 - send confirmation email
 - create a session and log user in
 - send them to the profile page with confirmation message
=============================================================================================
*/
	// public function activation($encoded_email = null, $activation_code = null)
	public function activation($activation_code = false)
	{
		// if(isset($encoded_email, $activation_code))
		// if(isset($activation_code))
		if($activation_code)
		{
			// get user email by decoding activation code
			$email_address = $this->encrypt->decode($activation_code);

			// get user data (** adjust to search only non-activated accounts **)
			$data = $this->user_model->activate_user($email_address);

			if($data)
			{
			    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
				$this->email->to($data['email_address']); 
			    $this->email->subject('Welcome to EMO Secure!');
				$this->email->message('<p>Hi '.$data['first_name'].' </p><p>Your account has been activated. </p><p>You can  here: <a href="'.base_url().'user//'.$data['email_address'].'">'.base_url().'user//'.$data['email_address'].'</a> </p>');	
			    $this->email->send();
			    // echo $this->email->print_debugger();

				$this->session->set_userdata($data);
				$this->my_set_flash_msg('',0,'confirmation', 'Your account has been activated. Welcome to EMO Secure!');
				redirect('user-profile');
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', 'User not found');
				redirect('user-');
			}

		}
		$this->my_set_flash_msg('',0,'error', 'There was a problem activating your account. Try the activation link again or <a href="'.base_url().'contact/feedback">contact support.</a>');
		redirect('user-');
	}													// EO Func
	
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	ctr_forgot_password()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-21
 * 
 * 		This handles the initial reach out for user requiring new password.
 * 
 * 		Note - The page this displays should be self-contained with no
 * 		default header or footer displayed.
 * 
 * 		MODIFICATION HISTORY
 * 
 * 
 * ------------------------------------------------------------------------
 */	
	
	public function ctr_forgot_password_v2_4()
	{
		// if post data exists, capture it
		$this->load->library('encrypt');
		$post = $this->input->post(NULL,true);
		
		if(!empty($post['email_address']))
		{
		$email_address = $post['email_address'];
		
		
		if($email_address)
		{
			// $email_address = $this->input->post('email_address', true);
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');

			// validation failed
			if ($this->form_validation->run() == false)
			{
				// User got something wrong.  Message them, record attempt, and redisplay - PRSC
				// $data['page'] = 'pages_v2_4/content/forgot-password';
				// $this->load->view('pages_v2_4/general', $data);
				
				$this->load->view('pages_v2_4/pg_forgot_password_v2_4');

			}
			// validation succeeded
			else
			{
				// Reget after scanning clear   - PRSC
				$email_address = $post['email_address'];
				
			
				$data = $this->main_model_v2_4->md_get_user_by_email($email_address); 
				$this->yprintARR($data, "GotEmail", 0);    // DBG

				if($data) 
				{
					// email code generated using a combination of the users email, today's date, and the system encryption key

					$reset_code = md5($this->config->item('encryption_key').date('Y-m-d').$email_address);
//					$encrypted_email_address = $this->encrypt->encode($data['email_address']);
					$encrypted_email_address = $this->encrypt->encode($data['Email']);
					
					// set flash data to display a confirmation (temporarily display reset link in confirmation)
					// $this->my_set_flash_msg('',0,'confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$reset_code.'">'.base_url().'user/reset_password/'.$reset_code.'</a>');	

					$msg1 = 'Password reset instructions have been sent to '.$email_address;
//					$msg2 = '<br><a href="' . base_url() . 'user/reset_password/'. $encrypted_email_address . '/'.$reset_code.'">';
					$msg2 = '<br><a href="' . base_url() . 'user/reset_password/'. $email_address . '/'.$reset_code.'">';
					$msg3 = base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.'</a>';	

					$this->my_set_flash_msg('',0,'confirmation', $msg1);	
					
//					$this->my_set_flash_msg('',0,'confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.'">'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.'</a>');	

					
// 					$this->my_set_flash_msg('',0,'confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$email_address.'/'.$reset_code.'">Reset Password</a>');	
//					$this->my_set_flash_msg('',0,'confirmation', 'Password reset instructions have been sent to '.$email_address);	

					// email the user a URL to the code

//					$this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
					$this->email->from('emocentral@halifax.ca', 'EMO Administration');
					$this->email->to($email_address); 
				    $this->email->subject('EMO Document Access - Please Reset your password');
//Content-type: text/plain; charset=iso-8859-1 

//					$config['crlf'] 			= '\r\n';
//					$config['wordwrap'] 		= TRUE;
//					$config['charset'] = 'iso-8859-1';					
//					$this->email->initialize($config);

				$passed_email = str_replace('@', "AAATTT", $email_address);    
				    
				    $usrmsg1 = 'Hello. 
				    
We received a request for this email address to reset their password. Please click on the link below and it will take you to a screen where you enter your email and new password.';
					$usrmsg2 = '

Click this link below to reset your password now.

'.base_url().'user-redo-password/'.$passed_email.'/'.$reset_code;	

					$usrmsg = $usrmsg1 . $usrmsg2;
					// $this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password at <strong>Help4Hire.com</a>. </p><p><a href="'.base_url().'user/reset_password/'.$email_address.'/'.$reset_code.'">Tap here</a> to reset your password. </p><p>For assistance contact <a href="mailto:noreply@help4hire.com">noreply@help4hire.com</a>. </p><p>Help4Hire Support</p>');	
					// $this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password. </p><p>Click the link below to reset your password now. </p><p>'.base_url().'user/reset_password/'.$reset_code.' </p>');	
					$this->email->message($usrmsg);

//					$this->yprint('UNKNOWN CONDITION. Application Halted. Notify [' . $usrmsg . "]", 'EMAIL REDO', 0);
					
				    $this->email->send();

				    // echo $this->email->print_debugger();
				}
				else
				{   // Request invalid.  Record attempt, and try again.
					
					$this->my_set_flash_msg('',0,'error', 'That email address doesn\'t exist in our database. &nbsp;
					<a href="'.base_url().'user-join">Create an account.</a>');	
				}									// EO If $Data returned from DB Call

				$this->session->keep_flashdata('email_address');
				redirect('user-signin');

			}										// EO If Form validation
		}
		else
		{
			// $data['page'] = 'pages_v2_4/content/forgot-password';
			$this->load->view('pages_v2_4/pg_forgot_password_v2_4');
		}											// EO If User EMail defined
		
	}												// EO If Post Data found
	else
	{		// No Posted Form Data so assume just entered or fake entry
		
			// $data['page'] = 'pages_v2_4/content/forgot-password';
			$this->load->view('pages_v2_4/pg_forgot_password_v2_4');
	}											// EO If User EMail defined
	
	
	
}													// EO Function	
	
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	ctr_redo_password_v2_4()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-06-21
 * 
 * 		This handles the user clicking in to re-enter their password.
 * 
 * 		MODIFICATION HISTORY
 * 
 * 
 * ------------------------------------------------------------------------
 */	
	
	public function ctr_redo_password_v2_4($email_address = null, $reset_code = null)
	{
		$this->load->library('encrypt');
		
		if(isset($email_address, $reset_code))
		{
			// decode email address
//			$email_address = $this->encrypt->decode($encrypted_email_address);

			// verify email code - if varified display password reset from view

			$real_email = str_replace("AAATTT",'@', $email_address);    
			
			
			$resp = md5($this->config->item('encryption_key').date('Y-m-d') . $real_email);

//			$this->yprint('RESET [' . $reset_code . "]", 'EMAIL REDO', 0);
//			$this->yprint('REPLY [' . $resp . "]", 'EMAIL REDO', 0);
//			exit();
			
			
			if($reset_code == $resp)
			{
				// create reset_hash from email address and reset_code to pass to the password reset form view
				$reset_hash = md5($email_address.$reset_code);

				$data['email_address']	= $email_address;
				$data['reset_code']	= $reset_code;
				$data['reset_hash']	= $reset_hash;

				// $data['page_info']['path'] 	= "sections/user/reset_password";
				// $this->load->view('template/master', $data);

				// $data['page'] = 'pages_v2_4/content/reset_password';
				$this->load->view('pages_v2_4/pg_redo_password_v2_4', $data);
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', 'Password reset has expired, try again.');
				redirect('user-forgot-password');
			}
		}
		else
		{	// TODO - This should generate a hack alert
			$this->my_set_flash_msg('',0,'error', 'Email address or reset code was missing, try again.');
			redirect('user-forgot-password');
		}
}											// EO Function redo_password
	

/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_submit_new_password_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_06_22
 *		
 *		This is used only for users who are changing their password
 *		after they have forgotten it.  So no login user session will be
 *		present.  The only check is the chk.
 *
 * 		RETURNS:	n/a
 * 		
 * --------------------------------------------------------------------------
 * 	MODIFICATION HISTORY
 * 
 * 	20170530 	PRSC	Fixed this for new argument passing protocol.
 * 		
 * --------------------------------------------------------------------------
*/ 		
	
	public function ctr_submit_new_password_v2_4()
	{
		
	
		/*...............Get Data REC details.............PRSC */
		
		$post = $this->input->post(null, true);

		
		/*...............Check that two new ones match........PRSC */
		
		
		if(!empty($post['npassword']) && !empty($post['cpassword']))
		{
			$npassword = $post['npassword'];
			$cpassword = $post['cpassword'];
			
			if($npassword != $cpassword)	
			{	
			$this->my_set_flash_msg('',0,'error', 
					'New and Confirm password fields do not match. Please try link again.');
			redirect('new-login');
			}
			
		}	
		else 
		{
			$this->my_set_flash_msg('',0,'error', 
					'You MUST enter data in both password fields. Please try link again.');
			redirect('new-login');
		}
		
		$this->yprintARR($post, "POST PASS", 0);    // DBG
		
		if(empty($post))
		{
			// This should be hack warning error - PRSC
			$this->my_set_flash_msg('',0,'error', 
					'You MUST enter data all fields. Please try link again.');
			redirect('new-login');
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('email_address', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('npassword', 'New Password', 'required');
			$this->form_validation->set_rules('cpassword', 'Confirmation Password', 'required');
			
		
			if ($this->form_validation->run() == true)
			{
				/*.............So now we can change it............*/
				// TODO Replace this with Error Test	
				$post['LastModBy']		= $post['email_address'];
				$post['Password']		= $post['cpassword'];			// use confirm one
				
				if ($this->main_model_v2_4->md_update_user_password($post))
				{
					$this->my_set_flash_msg('',0,'message', 
							'Your password has been successfully changed.');
					redirect('new-login');
				}
				else
				{
					$this->my_set_flash_msg('',0,'error', 
							'There was an error updating the User record. Please Contact help desk.');
					redirect('new-login');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->my_set_flash_msg('',0,'error', 
							'Incorrect Change Attempt. Please Contact your Admin.');
//				$this->my_set_flash_msg('',0,'error', validation_errors());
				redirect('new-login');
				
			}
		}
		
	}												// EO Update User Function
	

	
/* 	
=============================================================================================
 reset_password() - Display a form that enables a user to reset their password
---------------------------------------------------------------------------------------------
 Password Reset: Step 2 of 3
---------------------------------------------------------------------------------------------
 Requires email_address and reset_code. If the reset_code is valid it combines
 the email_address and reset_code then hashes them using MD5 to create the 
 reset_hash. All three values are then passed to the reset password view. The 
 reset_code and reset_hash are loaded in the view as hidden form fields and 
 will be used to validate the request to change password when the form is 
 submited to the change_password() function below. 
=============================================================================================
*/
	public function reset_password($encrypted_email_address = null, $reset_code = null)
	{
		if(isset($encrypted_email_address, $reset_code))
		{
			// decode email address
			$email_address = $this->encrypt->decode($encrypted_email_address);

			// verify email code - if varified display password reset from view
			if($reset_code == md5($this->config->item('encryption_key').date('Y-m-d').$email_address))
			{
				// create reset_hash from email address and reset_code to pass to the password reset form view
				$reset_hash = md5($email_address.$reset_code);

				$data['email_address']	= $email_address;
				$data['reset_code']	= $reset_code;
				$data['reset_hash']	= $reset_hash;

				// $data['page_info']['path'] 	= "sections/user/reset_password";
				// $this->load->view('template/master', $data);

				// $data['page'] = 'pages_v2_4/content/reset_password';
				$this->load->view('pages_v2_4/reset_password', $data);
			}
			else
			{
				$this->my_set_flash_msg('',0,'error', 'Password reset has expired, try again.');
				redirect('user-forgot-password');
			}
		}
		else
		{
			$this->my_set_flash_msg('',0,'error', 'Email address or reset code was missing, try again.');
			redirect('user-forgot-password');
		}
	}
/* 	
=============================================================================================
 change_password() - Display a form that enables a user to reset their password
---------------------------------------------------------------------------------------------
 Password Reset: Step 3 of 3
---------------------------------------------------------------------------------------------
 Requires email_address and reset_code. If the reset_code is valid it combines
 the email_address and reset_code then hashes them using MD5 to create the 
 reset_hash. All three values are then passed to the reset password view. The 
 reset_code and reset_hash are loaded into the form as hidden fields and will 
 be used to validate the reset password request when the form is submited.
=============================================================================================
*/
	public function ctr_change_password()
	{
		if($this->input->post('email_address', true) && $this->input->post('reset_code', true) && $this->input->post('reset_hash', true))
		{
			// form validation rules
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
			$this->form_validation->set_rules('password_confirmation', 'confirm password', 'trim|matches[password]|required');
			$this->form_validation->set_rules('reset_code', 'reset code', 'trim|exact_length[32]|required');
			$this->form_validation->set_rules('reset_hash', 'reset hash', 'trim|exact_length[32]|required');

			// validation
			if ($this->form_validation->run() == false)
			{
				// $data['page'] = 'pages_v2_4/content/reset_password';
				$this->load->view('pages_v2_4/reset_password');
			}
			else
			{
				$data = $this->input->post(null, true);

				// verify that reset code is correct
				if($data['reset_code'] == md5($this->config->item('encryption_key').date('Y-m-d').$data['email_address']))
				{
					// Verify that reset hash is correct
					if($data['reset_hash'] = md5($data['email_address'].$data['reset_code']))
					{
						// reset password
						// echo "Change password.";

						$result = $this->user_model->update_password($data['email_address'], $data['password']);

						// if successful, load user data into session and goto user profile
						if($result)
						{
						    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
							$this->email->to($result['email_address']); 
						    $this->email->subject('Your password has been changed');
							$this->email->message('<p>Hi '.$result['first_name'].' </p><p>Your password at <strong>Help4Hire.com</strong> has been changed. </p><p>
							          <a href="'.base_url().'user-/'.$result['email_address'].'">
							          Tab here</a> to . </p>
							          <p>For assistance contact <a href="mailto:noreply@help4hire.com">noreply@help4hire.com</a>. </p><p>Help4Hire Support</p>');	
						    $this->email->send();
						    // echo $this->email->print_debugger();

							$this->session->set_userdata($result);
							$this->my_set_flash_msg('',0,'confirmation', 'Your password was updated.');
							redirect('user/profile');
						}
						// if failed, goto  with error
						else
						{
							$this->my_set_flash_msg('',0,'error', 'There was a problem updating your password, try again. If the issue persists <a href="'.base_url().'contact/feedback">contact support</a>');
							redirect('user/reset_password/'.$data['email_address'].'/'.$data['reset_code']);
						} 

						// hash the users new password
						// update the users password in the database
						// update users last modified date in the database
						// log the user in to the website
						// send them to their profile page
						// confirm that their password was successfully changed
						// send a confirmation email
					}
					else 
					{
						$this->my_set_flash_msg('',0,'error', 'Reset hash was invalid.');
						redirect('user-forgot-password');
					}
				}
				else 
				{
					$this->my_set_flash_msg('',0,'error', 'Reset code was invalid.');
					redirect('user-forgot-password');
				}
			}	
		}
		// if no post data, display the  view
		else
		{
			$this->my_set_flash_msg('',0,'error', 'Email address, reset code or reset hash was missing, try again.');
			redirect('user-forgot-password');
		}
	}
/* 	
=============================================================================================
 logout() - Logs the user out, destroys their session and returns them to the 
  page with a notice telling them they've been logged out.
=============================================================================================
*/
	public function ctr_logout()
	{
		$this->session->sess_destroy();
		$this->session->sess_create();
		$this->my_set_flash_msg('',0,'confirmation', 'You\'ve been logged out.');
		redirect('new-login');
	}

	
	
/*---------------------------------------------------------
 * 		FUNCTION:	yprintARR()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This is used for array debugging and can be 
 * 		easily adapted to remote testing as well.
 * 		Data will be dumped to a external log file.
 * 
 *--------------------------------------------------------- 
 */

function yprintARR($vdata = NULL, $vmsg = NULL, $xcond = NULL)
{

$dbgYN  = TRUE;	
	
	if($dbgYN)
	{	
		print "===================================----->>>> SO [" . $vmsg . "]  Data DMP)";
		print "<pre>";
		if(!empty($vdata))
			print_r($vdata);
//			print "HERE";
		else 
			print "NO CELLS IN ARRAY<br><br>";	
		print "</pre>--------------<<<< EO [" . $vmsg . "]   Data DMP Ends)<br><br>";
		
	}	
}												// EO Array Display Func


/*---------------------------------------------------------
 * 		FUNCTION:	rsc_set_landing_page()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 *		This function will add the last landing page to
 *		user session array.  That way when finishing some
 *		tasks, the system knows what was the most recent
 *		landing page to return to.
 * 
 *--------------------------------------------------------- 
 */

function rsc_set_landing_page($i_lastpage = NULL, $cdloc = NULL, $xcond = NULL)
{
	
	$sessionUserREC = array();
	$sessionUserREC = $this->session->all_userdata();
		
	$this->session->set_userdata('landing_page',$i_lastpage);
	
//	$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
	
	
	
}	





/*---------------------------------------------------------
 * 		FUNCTION:	yprint()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This is used for one line debugging and can be 
 * 		easily adapted to remote testing as well.
 * 
 *--------------------------------------------------------- 
 */

function yprint($msg = NULL, $cdloc = NULL, $xcond = NULL)
{
//	print '<font color="FFFFFF">';
//	PRINT "<br> MSG [" . $msg . "][" . $cdloc . "]";
//	PRINT " MSG [" . $msg . "] " . $cdloc . "<br>" . "\r\n";
	
//	print "</font>";
	
}	
	

/*---------------------------------------------------------
 * 		FUNCTION:	my_set_flash_msg()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This is used to trap and set CI Flashmessages.
 * 
 *--------------------------------------------------------- 
 */

function my_set_flash_msg($xcond = NULL, $clloc = NULL, $msgtype = NULL, $msg = NULL)
{
//	print '<font color="FFFFFF">';
//	PRINT "<br> MSG [" . $msg . "][" . $cdloc . "]";
//	PRINT " MSG [" . $msg . "]<br>" . "\r\n";
	
	switch($msgtype)
	{
		default:
			$this->yprint('UNKNOWN CONDITION. Application Halted. Notify Service Desk. [' . $msgtype . "]", $msg, 0);
			$this->session->set_flashdata($msgtype, $msg);
			exit;			
			break;
			
		case 'success':	
		case 'message':
		case 'confirmation':	
			$this->yprint('USER WARNING', $msg, 0);
			$this->session->set_flashdata($msgtype, $msg);
			break;
			
		case 'error':
			$this->yprint('CATOSTROPHIC FAIL', $msg, 0);
			$this->session->set_flashdata($msgtype, $msg);
			break;

		case 'breach1':
			$this->yprint('BREACH 1', $msg, 0);
			$this->session->set_flashdata($msgtype, $msg);
			break;
			
		case 'breach2':
			$this->yprint('BREACH 2', $msg, 0);
			$this->session->set_flashdata($msgtype, $msg);
			break;
			
		case 'breach3':
			$this->yprint('BREACH 3', $msg, 0);
			$this->session->set_flashdata($msgtype, $msg);
			break;
			
		case 'breach6':
			$this->yprint('BREACH 6', $msg, 0);
			$this->session->set_flashdata($msgtype, $msg);
			break;
			
	}									// EO Switch
	
	
//	print "</font>";
	
}	
	

/*---------------------------------------------------------
 * 		FUNCTION:	ctr_show_fulldoc
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This loads and displays the PDFS to the viewer.
 * 
 *--------------------------------------------------------- 
 */



function ctr_show_fulldoc_B_v2_4($fname = NULL, $cdloc = NULL, $xcond = NULL)
{
	
		// load file and download helper classes
	$this->load->helper('file');
	$this->load->helper('download');

	// Create a temporary file and open it with write permissions
//	$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
	$tfilePTH = 'C:/inetpub/hrm_uploads/EMO/' . "tada.pdf";
	
	$filePTH = 'C:/EMO_uploads/' . "simpletest2.pdf";

//	force_download($filePTH, NULL);	
	print "[" . $filePTH . "]";
	
	$filename = 'TestDoc-'.date('M-d-Y').'.pdf';

//	header("Content-type: application/pdf");
//	header("Content-Length: " . filesize($filePTH));
	
	$myfile = fopen($filePTH, "rb") or die("Unable to open source file!");
	$data = fread($myfile,filesize($filePTH));

//	$tpfile = fopen($tfilePTH, "w") or die("Unable to open temp file!");
//	fwrite($tpfile,filesize($filePTH));
	
	fclose($myfile);
//	fclose($tpfile);
	
}											// EO Function	



function ctr_show_fulldoc_A_v2_4($fname = NULL, $cdloc = NULL, $xcond = NULL)
{
	
		// load file and download helper classes
	$this->load->helper('file');
	$this->load->helper('download');

	// Create a temporary file and open it with write permissions
//	$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
	$tfilePTH = 'C:/inetpub/hrm_uploads/EMO/' . "tada.pdf";
	
	$filePTH = 'C:/EMO_uploads/' . "simpletest1.pdf";

//	force_download($filePTH, NULL);	
	print "[" . $filePTH . "]";
//	exit;
	
//	$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
	$filename = 'TestDoc-'.date('M-d-Y').'.pdf';
	
	$myfile = fopen($filePTH, "rb") or die("Unable to open source file!");
	$data = fread($myfile,filesize($filePTH));

	$tpfile = fopen($tfilePTH, "w") or die("Unable to open temp file!");
	fwrite($tpfile,filesize($filePTH));
	
	fclose($myfile);
	fclose($tpfile);
	
//	$data = utf8_decode(file_get_contents($filePTH));  // get data and encode it as UTF8
	
//	$data = readfile($filePTH);
	force_download($filename, $data);	
	
	exit;
	
	
	
	$filename = "/path/to/the/file.pdf";
	$filePTH = 'C:/inetpub/hrm_uploads/EMO/' . "simpletest.pdf";

	$destfile = 'C:/inetpub/wwwroot/hrm_uploads/' . "readthis.pdf";
			// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');
	
	
	$this->yprint('FILENM [' . $filePTH . "]", '', 0);
	
	
//	header("Content-type: application/pdf");
//	header("Content-Length: " . filesize($filePTH));
	// Send the file to the browser.

//	readfile($filePTH);
	
//	$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";

	$data['targfile']		= $filePTH;
	$data['destfile']		= $destfile;
	//	$this->load->view('"pages_v2_4/pg_show', $data);
	$data['page'] = "pages_v2_4/pg_show";
	$this->load->view('template/master', $data);
 	
	
		
	
	
}											// EO Function	



function ctr_show_fulldoc_v2_4($fname = NULL, $cdloc = NULL, $xcond = NULL)
{
//	$filePTH = 'C:/EMO_uploads/' . "simpletest1.pdf";
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');
	
		$resultsARR = array();
		$data['sessionUserREC'] = $this->session->all_userdata();

		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
//		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG

		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		/*..................Check Role Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['viewDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to List Documents.');
			redirect('new-login');
			}
		}
			
	/*...................GET Document Information.............PRSC */	

	// Note there is a problem here if someone deletes the underlying document record
	

	$docID   = 0;
	$post = $this->input->post(null, true);

	/*...............Get Data REC details.............PRSC */
//	$this->yprint("UPDATE Document REC [ " . $rec_ID_to_update . "]",'',0);
//	$this->yprintARR($post, "Passed  Document INFO v2_4", 0);    // DBG

	if(empty($post))
	{						// Try to grab from session Info
//		$docID 		= $this->session->userdata('currDocID');
//		$CID 		= $this->session->userdata('currCID');
//		$currVERS	= $this->session->userdata('currVERS');
//		$LastModBy	= $this->session->userdata('Email');

		// Likely Security Breach Attempt if not outright error - PRSC
		$this->my_set_flash_msg('',0,'breach6', 
					'You do not have Authority to View This Documents.');
		redirect('new-login');
	
	}
	else 
	{
		$docID 			= $post['DocID'];
		$i_filename 	= $post['DocFileNM'];
	}
		
	// Deal with undefined DocumentID - should not get this far but...   PRSC
	if($docID < 0)
	{
	
	}	
	
	$documentREC = $this->main_model_v2_4->md_get_document_by_id($docID);   
//	$this->yprintARR($documentREC, "Document Details", 0);    // DBG

	if(!empty($documentREC))
	{
		
		if($documentREC['DocLoc'])
			$filePTH = $documentREC['DocLoc'];
		if($documentREC['DocFileName'])
			$filename = $documentREC['DocFileName'];
			
		$fullfilename = $filePTH . "\\" . $filename;		
		
		// Check for manipulated web input
		if($i_filename != $filename)
		{
		$this->my_set_flash_msg('',0,'breach6', 
					'Database Access Restriction E-0066. Please contact Tech Support.');
		redirect('new-login');
		}
		
	}	
	else 
	{
		$this->my_set_flash_msg('',0,'breach', 
					'Database Access Restriction E-0045. Please contact Tech Support.');
		redirect('new-login');
	}
		
//	$this->yprint("FILENAME [" . $fullfilename . "]",'user->ev_displayPDF',0);		// DBG
	
//	$filePTH = realpath('https://appsdev.halifax.ca/hrm_uploads/readthis.pdf');
//	$filePTH = 'C:/inetpub/wwwroot/hrm_uploads/' . "readthis.pdf";

//	$myfile = fopen($fullfilename, "rb") or die("Unable to open source file!");
//	$FLdata = fread($myfile,filesize($fullfilename));
//	fclose($myfile);
	
//	header("Content-type: application/pdf");
//	header("Content-Length: " . filesize($fullfilename));
	
	
//	print $FLdata;
	
//	force_download($fullfilename, NULL);	
//	print "[" . $filePTH . "]";
	
//	$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
//	$filename = 'TestDoc-'.date('M-d-Y').'.pdf';
	
//	$myfile = fopen($filePTH, "rb") or die("Unable to open source file!");
//	$FLdata = fread($myfile,filesize($filePTH));

//	$tpfile = fopen($tfilePTH, "w") or die("Unable to open temp file!");
//	fwrite($tpfile,filesize($filePTH));
	
//	fclose($myfile);
//	fclose($tpfile);
	
	
//	header("Content-type: application/pdf");
//	header("Content-Length: " . filesize($filePTH));
// Send the file to the browser.

//	readfile($filePTH);
	
//	$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";

	$data['targfile']		= $fullfilename;
//	$data['destfile']		= $destfile;s

	$this->load->view('pages_v2_4/pg_show', $data);
//	$data['page'] = "pages_v2_4/pg_show";
//	$this->load->view('template/master', $data);
	
	
}											// EO Function	



/*---------------------------------------------------------
 * 	
 * 		FUNCTION:	uploadthis()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 *		This is the controller for uploading documents
 *		to the EMO Safe Storage area.
 * 
 *--------------------------------------------------------- 
 */
	
public function uploadthis()
{
		$data['data_state'] = "";
		$resultsARR			= array();
		
		$response_page		= 'show-documents';
		
		/*...........Fetch User Roles Parameters - PRSC  */
		$rolesARR  	= array();
		$rolesARR 	= $this->inner_get_roles();
//		$this->yprintARR($rolesARR, "User Roles", 0);    // DBG
		
		/*...........Fetch User Session Info-PRSC */
		$sessionUserREC = array();
		$sessionUserREC = $this->session->all_userdata();
		$this->yprintARR($sessionUserREC, "SESSION", 0);    // DBG
		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach6', 
						'Your login Session has ended. You must log in again.');
				redirect('new-login');
			}	
		$data['LastModBy']		= $sessionUserREC['Email'];
		$data['sessionUserREC'] 	= $sessionUserREC;

		if(!empty($sessionUserREC['landing_page']))
			$response_page = $sessionUserREC['landing_page'];
		
		/*..............Check Session Authorization.........PRSC */

		if(empty($sessionUserREC['loggedIn']))
			{
				$this->my_set_flash_msg('',0,'breach2', 
						'Your Session has been closed. Please log in again.');
				redirect('new-login');
			}	
		if($sessionUserREC['loggedIn'] != TRUE)
			{
				$this->my_set_flash_msg('',0,'breach3', 
						'Your Session has expired. Please log in again.');
				redirect('new-login');
			}	
		
		
		/*...................Get Role Information.........PRSC */		

		$userTYPE				= $sessionUserREC['UserGroup'];
//		$this->yprint("UserType [" . $userTYPE . "]",'user->ev_display_cat_full',0);		// DBG
		
		$roleREC	= array();
		$roleREC 	= $rolesARR[$userTYPE];
		if(!empty($roleREC))
			$data['roleREC'] 		= $roleREC;
		else 
		{							// User Rold not defined
			$this->my_set_flash_msg('',0,'error', 
					'Session Failure. Please log in again.');
			redirect('new-login');
		}			
//		$this->yprintARR($roleREC, "RoleRECss ", 0);    // DBG

		
		/*..................Check Authorization........PRSC */
		
		if(!empty($roleREC))
		{
		if($roleREC['detailDocsYN'] != 'Y')
			{
			$this->my_set_flash_msg('',0,'breach', 
					'You do not have Authority to Upload Document Records.');
			redirect('new-login');
			}
		}

		
		
		// Load Category Details to find target location

		$CID 		= $this->session->userdata('currCID');
		$DocID 		= $this->session->userdata('currDocID');
		$currVERS	= $this->session->userdata('currVERS');
		$LastModBy	= $this->session->userdata('Email');
		
		if($CID < 1)
			{							// CID Not defined- Should be Error PRSC
			$this->my_set_flash_msg('',0,'breach', 
					'No Category Defined for Upload.');
			redirect('new-login');
			}
		
		$categoryREC		= array();

		// Define Target Directory - Else assume it is zero aka. public base doc - PRSC	
		if($CID > 0 )
			{
			$categoryREC 		= $this->main_model_v2_4->md_get_category_by_id($CID);
//			$categoryREC   		= $categoriesARR[$CID];
//			$this->yprintARR($categoryREC, "CATEGORU UPload ", 0);    // DBG

// Use this when you if Target Direc to be specified in the Category REC 			
//			if(!empty($categoryREC['targetDIR']))
//				$targetDIR			= $categoryRED['targetDIR'];
//			else 
//				$targetDIR			= '0';
			$targetDIR			= $CID;
			}
			
			
//		$file_name = $_FILES['file_var_name']['name'];		
//		$this->yprintARR($_FILES, "Upload Files ", 0);    // DBG
		$upNM	= $_FILES['userfile']['name'];	
		
		/*.........Set up the Upload Config Array...........*/
		$FLDateStamp		= date("Y-m-d-H-i-s");
		$destFLNM			= $FLDateStamp . "_" . $currVERS . "_" . $upNM;
		
		$config		= array();
//		$fullpath 	= 'C:/inetpub/hrm_uploads/EMO/';
//		$fullpath 	= realpath('C:/inetpub/hrm_uploads/');
		$imgPATH = realpath('C:/inetpub/wwwroot/hrm_uploads/EMO/');
		$imgPATH = 'C:/inetpub/wwwroot/hrm_uploads/EMO/';
		$imgPATH = 'C:\inetpub\hrm_uploads\EMO' ;
		$fullPATH = realpath('C:/inetpub/wwwroot/hrm_uploads/EMO/');
		$imgPATH = 'C:\EMOUploads';

		// Define Upload destination
		$imgPATH = 'C:\EMOUploads' . "\\" . $targetDIR;
		
	
		$this->yprint("Upload Target [ " . $imgPATH . "]",'',0);
		$this->yprint("Upload FileNew  [ " . $destFLNM . "]",'',0);
		
         $config['upload_path']   = $imgPATH; 
         $config['allowed_types'] = 'pdf'; 
         $config['max_size']      = 2000; 
         $config['max_width']     = 1024; 
         $config['max_height']    = 768;  
         if($destFLNM != '')
	         $config['file_name']      = $destFLNM;  
         
		$this->yprintARR($config, "CONFIG SETTING  ", 0);    // DBG
		        
		/*..........Set View disabled True or False - PRSC  */
		if($roleREC['viewDisabledYN'] == 'Y')	
					$viewDisabledYN = FALSE	;
			else 
					$viewDisabledYN = TRUE;
		
		
        $this->load->library('upload', $config);

 		if ( ! $this->upload->do_upload('userfile')) 
 			{
            $error = array('error' => $this->upload->display_errors()); 
//            $this->load->view('upload_form', $error); 
//			$this->yprintARR($error, "Upload Errors ", 0);    	// DBG
 			}
			
         else
         	{ 
            $data = array('upload_data' => $this->upload->data()); 
 //           $this->load->view('upload_success', $data); 

 //         	$this->yprint("IT WORKS ",'uploader', 0);    	// DBG

		/*..................Update the Document REC info and save it.......PRSC.*/	

		$doc_modsARR = array(
			'DocID'					=>  $DocID,			
			'DocLoc'				=>  $imgPATH,			
			'DocType'				=>  'PDF',
			'DocFileName'			=>  $destFLNM,
//			'AdminID'				=> $data['AdminID'],
			'LastModBy'				=> $LastModBy
		);
          	
		$resp 	= $this->main_model_v2_4->md_update_document_upload($doc_modsARR);
			
		/*..................Fetch Document Data again for redisplay......PRSC */	
			// Successful Upload - get and redisplay base info	
			$this->yprint("DocID [ " . $DocID . "]",'uploader', 0);    	// DBG
		
			$DocREC			= $this->main_model_v2_4->md_get_document_by_id($DocID);
//			$this->yprintARR($DocREC, "DocumentDATA ", 0);    	// DBG

		
		if(!empty($DocREC))
			{
			$data['documentREC']			= $DocREC;
			$data['roleREC']				= $roleREC;
			$data['sessionUserREC']			= $sessionUserREC;
			
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories($viewDisabledYN,'DESC');
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
			$data['SubcatsARR'] 		= $this->main_model_v2_4->md_get_subcats($viewDisabledYN,'DESC');
			
			$data['page'] = "pages_v2_4/document/pg_display_document_UL_v2_4";
			$this->load->view("template/master", $data);
			}
			else 
			{
			$this->my_set_flash_msg('',0,'message', 'Upload Document '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
			$this->yprint("Upload Final Failed [ " . $i_recID . "]",'',0);
			redirect($response_page);
			}
			
	}								// EO If Results not empty
			
         	
	
}											// EO Function uploader		


}											// EO Class