<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*           
============================================================================
 Class: 		Deliverables
 Description:	Main application controller. 
 				Interacts with the deliverables_model and application views.
============================================================================
*/
class Calendar extends CI_Controller {

/*           
============================================================================
 index
----------------------------------------------------------------------------
 Load calendar page
============================================================================
*/
	public function index()
	{
		
		$data['page'] = "pages/calendar";
		$this->load->view('template/master', $data);

	}


}