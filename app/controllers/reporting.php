<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*           
============================================================================
 Class: 		Deliverables
 Description:	Main application controller. 
 				Interacts with the deliverables_model and application views.
============================================================================
*/
class Reporting extends CI_Controller {

/*           
============================================================================
 index
----------------------------------------------------------------------------
 Load reporting dashboard
============================================================================
*/
	public function index()
	{
		
		$data['page'] = "pages/reporting";
		$this->load->view('template/master', $data);

	}


}