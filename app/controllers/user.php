<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

/*           
============================================================================
 index
----------------------------------------------------------------------------
 Load signin view
============================================================================
*/
	public function index()
	{
		$this->signin();
	}

/* 	
=============================================================================================
 signin() - Attempts to log a user in using email address and password.
---------------------------------------------------------------------------------------------
 Requires a valid email_address and password from post data. Validates those credentials 
 against records in the database. If valid, an array of user data is returned and used to 
 create a session. If invalid, the user is returned to the signin page with error message.
=============================================================================================
*/
	public function signin()
	{
		$data = $this->input->post(null, true);

		print "HERE2";
		
		// if credentials were supplied, attempt to authenticate
		// if($this->input->post('email_address', true) && $this->input->post('password', true))
		if($data)
		{
			$this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email|min_length[6]|max_length[100]|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[5]|max_length[15]|required');

			// validation failed
			if ($this->form_validation->run() == false)
			{
				$this->load->view('pages/signin');
			}
			// validation successful
			else
			{
				// get data signin data and authenticate user
				print "BREAK";
				$result = $this->user_model->authenticate($data);
				$this->yprintARR($result, "LOGIN REC Dump", 0);    // DBG
				
				exit;
				
				// if successful, load user data into session and goto user profile
				if($result)
				{
					// echo "<pre>";
					// print_r($result);
					// echo "</pre>";
					// die();

					$this->session->set_userdata($result);
					redirect('user/profile');
				}
				// if failed, goto signin with error
				else
				{
					$this->session->set_flashdata('error', 'Invalid credentials.<br>Try again or <a href="'.base_url().'user/forgot_password">reset your password</a>');
					$this->session->set_flashdata('email_address', $data['email_address']);
					redirect('user/signin');
				} 
			}
		}
		// if no credentials, goto signin with error
		else
		{
			// $data['content'] = 'pages/content/signin';
			// $this->load->view('pages/general', $data);
			$this->load->view('pages/signin');
			
			// $data['page'] = "pages/signin";
			// $this->load->view('template/master', $data);
		}
	}

/*           
============================================================================
 profile
----------------------------------------------------------------------------
 Loads users profile
============================================================================
*/
	public function profile()
	{

		$data['session_data'] = $this->session->all_userdata();

		$data['page'] = "pages/profile";
		$this->load->view('template/master', $data);
	}	



/* 	
=============================================================================================
 signup() - Enables people to join and create new accounts using an email address.
---------------------------------------------------------------------------------------------
 Registration: Step 1 of 2
---------------------------------------------------------------------------------------------
 - validate submitted information
 - encrypt email address to generate activation code
 - check if that email adderss already exist in the system
 - add a new unactivated user to the database
 - send activation email with activation link that sends them to activation function below
 - redirect with confirmation message, otherwise redirect to signin with error
=============================================================================================
*/
	public function signup()
	{
		if($data = $this->input->post(null, true))
		{
			$this->form_validation->set_rules('first_name', 'first name', 'trim|min_length[2]|required');
			$this->form_validation->set_rules('last_name', 'last name', 'trim|min_length[2]|required');
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
			// $this->form_validation->set_rules('password_confirmation', 'confirm password', 'trim|matches[password]|required');

			if($this->form_validation->run() != false)
			{
				$activation_code = $this->encrypt->encode($data['email_address']);

				// check if that email adderss already exist in the system
				if(!$this->user_model->email_address_exist($data['email_address']))
				{
					if($this->user_model->add_user($data))
					{
					    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
						$this->email->to($data['email_address']); 
					    $this->email->subject('Activate your account');
						// $this->email->message('<p>Hi '.$data['first_name'].', welcome to <strong>Help4Hire</strong>! </p><p>Click the link below to verify your email address and activate your account. </p><p><a href="'.base_url().'user/activation/'.$activation_code.'">'.base_url().'user/activation/'.$activation_code.'</a>');	
						$this->email->message('<p>Click the link below to verify your email address and activate your account. </p><p><a href="'.base_url().'user/activation/'.$activation_code.'">'.base_url().'user/activation/'.$activation_code.'</a>');	
					    // echo $this->email->print_debugger();

					    if($this->email->send())
					    {
							// $this->session->set_flashdata('confirmation', 'Please check your email to complete the registration process and activate your account. <a href="'.base_url().'user/activation/'.$activation_code.'">Activate Now</a>');	
							$this->session->set_flashdata('confirmation', 'Check your email to complete the registration process.');
							redirect('user/signin');
					    }
					    else 
					    {
					    	$this->session->set_flashdata('error', 'Account was created but sending activation email failed.');
							redirect('user/signup');
						}					    
					}
					else
					{
						$this->session->set_flashdata('error', 'Error creating user account.');
						redirect('user/signup');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'An account with that email address already exists - try again or <a href="'.base_url().'user/signin">log in</a>');
					redirect('user/signup');
				} 
			}
		}
		else
		{
			// $data['content'] = 'pages/content/signup';
			// $this->load->view('pages/general', $data);	
			$this->load->view('pages/signup');
		}

	}
/* 	
=============================================================================================
 activation() - Activates new user accounts.
---------------------------------------------------------------------------------------------
 Registration: Step 2 of 2
---------------------------------------------------------------------------------------------
 - decode activation code to get the email address
 - use the email address to activate the account and retrieve user data
 - send confirmation email
 - create a session and log user in
 - send them to the profile page with confirmation message
=============================================================================================
*/
	// public function activation($encoded_email = null, $activation_code = null)
	public function activation($activation_code = false)
	{
		// if(isset($encoded_email, $activation_code))
		// if(isset($activation_code))
		if($activation_code)
		{
			// get user email by decoding activation code
			$email_address = $this->encrypt->decode($activation_code);

			// get user data (** adjust to search only non-activated accounts **)
			$data = $this->user_model->activate_user($email_address);

			if($data)
			{
			    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
				$this->email->to($data['email_address']); 
			    $this->email->subject('Welcome to Help4Hire!');
				$this->email->message('<p>Hi '.$data['first_name'].' </p><p>Your account has been activated. </p><p>You can signin here: <a href="'.base_url().'user/signin/'.$data['email_address'].'">'.base_url().'user/signin/'.$data['email_address'].'</a> </p>');	
			    $this->email->send();
			    // echo $this->email->print_debugger();

				$this->session->set_userdata($data);
				$this->session->set_flashdata('confirmation', 'Your account has been activated. Welcome to Help4Hire.com!');
				redirect('user/profile');
			}
			else
			{
				$this->session->set_flashdata('error', 'User not found');
				redirect('user/signin');
			}

		}
		$this->session->set_flashdata('error', 'There was a problem activating your account. Try the activation link again or <a href="'.base_url().'contact/feedback">contact support.</a>');
		redirect('user/signin');
	}
/* 	
=============================================================================================
 forgot_password() - Emails a url to a user so they can reset their password.
---------------------------------------------------------------------------------------------
 Password Reset: Step 1 of 3
---------------------------------------------------------------------------------------------
 Accepts a valid email address from post. Checks if that email address exists
 in the database. If it does it's combined with the current date and system 
 encryption key then hashed with MD5 to create the reset_code. The email 
 address and reset code are then used to create a URL that's emailed to the 
 user. That URL passes the values to the reset_password() function below and 
 are used to authenticate the reset request.
=============================================================================================
*/
	public function forgot_password()
	{
		// if post data exists, capture it
		if($email_address = $this->input->post('email_address', true))
		{
			// $email_address = $this->input->post('email_address', true);
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');

			// validation failed
			if ($this->form_validation->run() == false)
			{
				// $data['content'] = 'pages/content/forgot_password';
				// $this->load->view('pages/general', $data);
				$this->load->view('pages/forgot_password');
			}
			// validation succeeded
			else
			{
				// $this->load->model('user_model');
				// $data = $this->user_model->get_user_by_email($email_address);
				// if($this->user_model->email_address_exist($email_address)) 
				if($data = $this->user_model->get_user_by_email($email_address)) 
				{
					// email code generated using a combination of the users email, today's date, and the system encryption key
					$reset_code = md5($this->config->item('encryption_key').date('Y-m-d').$email_address);
					$encrypted_email_address = $this->encrypt->encode($data['email_address']);

					// set flash data to display a confirmation (temporarily display reset link in confirmation)
					// $this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$reset_code.'">'.base_url().'user/reset_password/'.$reset_code.'</a>');	
					$this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.'">'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.'</a>');	
					// $this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$email_address.'/'.$reset_code.'">Reset Password</a>');	
					// $this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address);	

					// email the user a URL to the code
				    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
					$this->email->to($email_address); 
				    $this->email->subject('Reset your password');
					// $this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password at <strong>Help4Hire.com</a>. </p><p><a href="'.base_url().'user/reset_password/'.$email_address.'/'.$reset_code.'">Tap here</a> to reset your password. </p><p>For assistance contact <a href="mailto:noreply@help4hire.com">noreply@help4hire.com</a>. </p><p>Help4Hire Support</p>');	
					// $this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password. </p><p>Click the link below to reset your password now. </p><p>'.base_url().'user/reset_password/'.$reset_code.' </p>');	
					$this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password. </p><p>Click the link below to reset your password now. </p><p>'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.' </p>');	
				    $this->email->send();
				    // echo $this->email->print_debugger();
				}
				else
				{
					$this->session->set_flashdata('error', 'That email address doesn\'t exist in our database. &nbsp;<a href="'.base_url().'user/join">Create an account.</a>');	
				}

				$this->session->keep_flashdata('email_address');
				redirect('user/signin');

			}
		}
		else
		{
			// $data['content'] = 'pages/content/forgot_password';
			$this->load->view('pages/forgot_password');
		}
	}
/* 	
=============================================================================================
 reset_password() - Display a form that enables a user to reset their password
---------------------------------------------------------------------------------------------
 Password Reset: Step 2 of 3
---------------------------------------------------------------------------------------------
 Requires email_address and reset_code. If the reset_code is valid it combines
 the email_address and reset_code then hashes them using MD5 to create the 
 reset_hash. All three values are then passed to the reset password view. The 
 reset_code and reset_hash are loaded in the view as hidden form fields and 
 will be used to validate the request to change password when the form is 
 submited to the change_password() function below. 
=============================================================================================
*/
	public function reset_password($encrypted_email_address = null, $reset_code = null)
	{
		if(isset($encrypted_email_address, $reset_code))
		{
			// decode email address
			$email_address = $this->encrypt->decode($encrypted_email_address);

			// verify email code - if varified display password reset from view
			if($reset_code == md5($this->config->item('encryption_key').date('Y-m-d').$email_address))
			{
				// create reset_hash from email address and reset_code to pass to the password reset form view
				$reset_hash = md5($email_address.$reset_code);

				$data['email_address']	= $email_address;
				$data['reset_code']	= $reset_code;
				$data['reset_hash']	= $reset_hash;

				// $data['page_info']['path'] 	= "sections/user/reset_password";
				// $this->load->view('template/master', $data);

				// $data['content'] = 'pages/content/reset_password';
				$this->load->view('pages/reset_password', $data);
			}
			else
			{
				$this->session->set_flashdata('error', 'Password reset has expired, try again.');
				redirect('user/forgot_password');
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Email address or reset code was missing, try again.');
			redirect('user/forgot_password');
		}
	}
/* 	
=============================================================================================
 change_password() - Display a form that enables a user to reset their password
---------------------------------------------------------------------------------------------
 Password Reset: Step 3 of 3
---------------------------------------------------------------------------------------------
 Requires email_address and reset_code. If the reset_code is valid it combines
 the email_address and reset_code then hashes them using MD5 to create the 
 reset_hash. All three values are then passed to the reset password view. The 
 reset_code and reset_hash are loaded into the form as hidden fields and will 
 be used to validate the reset password request when the form is submited.
=============================================================================================
*/
	public function change_password()
	{
		if($this->input->post('email_address', true) && $this->input->post('reset_code', true) && $this->input->post('reset_hash', true))
		{
			// form validation rules
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
			$this->form_validation->set_rules('password_confirmation', 'confirm password', 'trim|matches[password]|required');
			$this->form_validation->set_rules('reset_code', 'reset code', 'trim|exact_length[32]|required');
			$this->form_validation->set_rules('reset_hash', 'reset hash', 'trim|exact_length[32]|required');

			// validation
			if ($this->form_validation->run() == false)
			{
				// $data['content'] = 'pages/content/reset_password';
				$this->load->view('pages/reset_password');
			}
			else
			{
				$data = $this->input->post(null, true);

				// verify that reset code is correct
				if($data['reset_code'] == md5($this->config->item('encryption_key').date('Y-m-d').$data['email_address']))
				{
					// Verify that reset hash is correct
					if($data['reset_hash'] = md5($data['email_address'].$data['reset_code']))
					{
						// reset password
						// echo "Change password.";

						$result = $this->user_model->update_password($data['email_address'], $data['password']);

						// if successful, load user data into session and goto user profile
						if($result)
						{
						    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
							$this->email->to($result['email_address']); 
						    $this->email->subject('Your password has been changed');
							$this->email->message('<p>Hi '.$result['first_name'].' </p><p>Your password at <strong>Help4Hire.com</strong> has been changed. </p><p><a href="'.base_url().'user/signin/'.$result['email_address'].'">Tab here</a> to signin. </p><p>For assistance contact <a href="mailto:noreply@help4hire.com">noreply@help4hire.com</a>. </p><p>Help4Hire Support</p>');	
						    $this->email->send();
						    // echo $this->email->print_debugger();

							$this->session->set_userdata($result);
							$this->session->set_flashdata('confirmation', 'Your password was updated.');
							redirect('user/profile');
						}
						// if failed, goto signin with error
						else
						{
							$this->session->set_flashdata('error', 'There was a problem updating your password, try again. If the issue persists <a href="'.base_url().'contact/feedback">contact support</a>');
							redirect('user/reset_password/'.$data['email_address'].'/'.$data['reset_code']);
						} 

						// hash the users new password
						// update the users password in the database
						// update users last modified date in the database
						// log the user in to the website
						// send them to their profile page
						// confirm that their password was successfully changed
						// send a confirmation email
					}
					else 
					{
						$this->session->set_flashdata('error', 'Reset hash was invalid.');
						redirect('user/forgot_password');
					}
				}
				else 
				{
					$this->session->set_flashdata('error', 'Reset code was invalid.');
					redirect('user/forgot_password');
				}
			}	
		}
		// if no post data, display the signin view
		else
		{
			$this->session->set_flashdata('error', 'Email address, reset code or reset hash was missing, try again.');
			redirect('user/forgot_password');
		}
	}
/* 	
=============================================================================================
 logout() - Logs the user out, destroys their session and returns them to the 
 signin page with a notice telling them they've been logged out.
=============================================================================================
*/
	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->sess_create();
		$this->session->set_flashdata('confirmation', 'You\'ve been logged out.');
		redirect('user/signin');
	}


/*---------------------------------------------------------
 * 		FUNCTION:	yprintARR()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This is used for array debugging and can be 
 * 		easily adapted to remote testing as well.
 * 		Data will be dumped to a external log file.
 * 
 *--------------------------------------------------------- 
 */

function yprintARR($vdata = NULL, $vmsg = NULL, $xcond = NULL)
{
		print "<pre>--------------->>>> SO [" . $vmsg . "]  Data DMP)";
		if(!empty($vdata))
			print_r($vdata);
//			print "HERE";
		else 
			print "NO CELLS IN ARRAY<br><br>";	
		print "</pre>--------------<<<< EO [" . $vmsg . "]   Data DMP Ends)<br><br>";
}

/*---------------------------------------------------------
 * 		FUNCTION:	yprint()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This is used for one line debugging and can be 
 * 		easily adapted to remote testing as well.
 * 
 *--------------------------------------------------------- 
 */

function yprint($msg = NULL, $cdloc = NULL, $xcond = NULL)
{
//	print '<font color="FFFFFF">';
//	PRINT "<br> MSG [" . $msg . "][" . $cdloc . "]";
	PRINT " MSG [" . $msg . "]<br>" . "\r\n";
	
//	print "</font>";
	
}	
	

}