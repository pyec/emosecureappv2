<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*           
============================================================================
 Class: 		Deliverables
 Description:	Main application controller. 
 				Interacts with the deliverables_model and application views.
============================================================================
*/
class News extends CI_Controller {

/*           
============================================================================
 index
----------------------------------------------------------------------------
 Load news page
============================================================================
*/
	public function index()
	{
		
		$data['page'] = "pages/news";
		$this->load->view('template/master', $data);

	}


}