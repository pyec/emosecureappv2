<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User_v2_4 extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('main_model_v2_4');
	}

/*           
============================================================================
 index
----------------------------------------------------------------------------
 Load  view
============================================================================
*/
	public function index()
	{
		$this->yprint("Loading Signin","user->signin()",0);
		$this->ctr_signin();
	}

/* 	
=============================================================================================
 () - Attempts to log a user in using email address and password.
---------------------------------------------------------------------------------------------
 Requires a valid email_address and password from post data. Validates those credentials 
 against records in the database. If valid, an array of user data is returned and used to 
 create a session. If invalid, the user is returned to the  page with error message.
=============================================================================================
*/
	public function ctr_signin()
	{
		$data = $this->input->post(null, true);

		// if credentials were supplied, attempt to authenticate
		// if($this->input->post('email_address', true) && $this->input->post('password', true))
		if($data)
		{
			$this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email|min_length[6]|max_length[100]|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[5]|max_length[15]|required');

			// validation failed
			if ($this->form_validation->run() == false)
			{
				$this->load->view('pages_v2_4/pg_signin_v2_4');
			}
			// validation successful
			else
			{
				// get data  data and authenticate user
				$this->yprint("Attempting to Authenticate","user->authenticate()",0);
				$result = $this->user_model->authenticate($data);
				$this->yprintARR($result, "LOGIN REC Dump", 0);    // DBG
				
				// if successful, load user data into session and goto user profile
				if($result)
				{
					// echo "<pre>";
					// print_r($result);
					// echo "</pre>";
					// die();

					$this->session->set_userdata($result);
					$this->yprint("Authenticate Success","user->authenticate()",0);
										
					redirect('user-profile');
				}
				// if failed, goto  with error
				else
				{
					$this->session->set_flashdata('error', 'Invalid credentials.<br>Try again or <a href="'.base_url().'user-forgot-password">reset your password</a>');
					$this->session->set_flashdata('email_address', $data['email_address']);
					$this->yprint("Authenticate Failed","user->authenticate()",0);
					redirect('user-signin');
				} 
			}
		}
		// if no credentials, goto  with error
		else
		{
			// $data['page'] = 'pages_v2_4/content/';
			// $this->load->view('pages_v2_4/general', $data);
			
			 $data['page'] = "pages_v2_4/pg_signin_v2_4";
			 $this->load->view('template/master', $data);
		}
	}


/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_profile
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the template for the basic sign-in page used within the
 * 		new version of EMOSecureApp.
 * 
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-18		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_profile()
	{

		$data['session_data'] = $this->session->all_userdata();

		$data['page'] = "pages_v2_4/pg_profile_v2_4";
		$this->load->view('template/master', $data);
	}	


	

//------------------------------------------------( SO User Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-user" view file.
 		This will allow users to create and enter a new user
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_user_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
//			$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/user/pg_add_user_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('UID', 'User ID', 'required');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('UserTypesID', 'User Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('UserDesc', 'User Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_user($post))
				{
					$this->session->set_flashdata('message',
						 'The User record has been added successfull added.');
					redirect('display-user/'.$post['REG_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 
						'There was an error adding the User record, please try again.');
					redirect('display-user');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('display-user');
			}
		}
		
	}										// EO Add User Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-user" view file.
 		This will allow users to create and enter a new user
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_user_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

//		$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_user_by_id($i_recID);
//		$this->yprintARR($resultsARR, "UserDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['userREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

			$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'User '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-users');
			}
			
		}
		
	}											// EO Display User Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_user_and_groups_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-user" view file.
 
 		This will allow users to create and enter a new user
 		as requested.
 		
		This shows the details of the groups the user is part of. 		
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_user_and_groups_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

//		$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_user_by_id($i_recID);
//		$this->yprintARR($resultsARR, "UserDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{

				
			/*............Get List of Associated Groups User Needs........PRSC.*/	

			$accessListARR			= array();
			$usersHLDARR			= array();
			$userID					= $i_recID;	
//			$wrkARR 				= $this->main_model_v2_4->md_get_access_users_in_category($categoryID);
			$wrkARR 				= $this->main_model_v2_4->md_get_access_categories_user_is_in($userID);
			$categoriesARR			= $this->main_model_v2_4->md_get_categories();
			$cntxx					= 0;

			foreach ($wrkARR as $workREC)
			{
			$wID			   						= $workREC['CID'];	

//			$this->yprintARR($workREC, "AccessList", 0);    	// DBG
			
			foreach ($categoriesARR as $categoryREC)
				{
				if($categoryREC['CID'] == $wID)
				{
				$accessListARR[$cntxx]      			= $categoryREC;	
				$cntxx++;
				}
				}
			}
			
//			$this->yprintARR($categoriesARR, "UserCategoriesList", 0);    	// DBG
			$data['categoriesARR'] 				= $accessListARR;
				
		
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

			// Get Master Data
			$data['userREC']			= $resultsARR;
			
			
//			$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";
			$data['page'] = "pages_v2_4/user/pg_display_user_and_accesses_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'User '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-users');
			}
			
		}
		
	}											// EO Display User Function
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-user"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_user_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";
		
//		$rec_ID_to_update = $this->uri->segment(3);
				

		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
		}
		else
		{								// Error - Reshow Page
			$this->session->set_flashdata('error', 
						'No valid record ID specified. Please try again');
			$data['page'] = 'pages_v2_4/user/pg_change_user_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_user_by_id($rec_ID_to_update);
		$this->yprintARR($resultsARR, "UserDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change User REC [ " . $i_recID . "] has been Updated",'',0);
//			$this->session->set_flashdata('message', 
//						"Change User REC [ " . $i_recID . "] has been Updated");
			
			$data['userREC']  = $resultsARR;
			$data['page'] = 'pages_v2_4/user/pg_change_user_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->session->set_flashdata('error', 
						'Data for that User not Available. Please try again');
			$data['page'] = 'pages_v2_4/user/pg_change_user_v2_4';
			redirect('show-users');
		}
		
	}												// EO Update User Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_user_v2_4()
	{
		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		$post['LastModBy']		= $sessionUserARR['Email'];
		$data['session_data'] 	= $sessionUserARR;

		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionUserARR['Email'];
		
//		$this->yprint("UPDATE User REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionUserARR['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
//		$this->yprintARR($post, "UPDATE User INFO v2_4", 0);    // DBG
		
		
		if(empty($post))
		{
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('UID', 'User ID', 'required|numeric');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('UserTypesID', 'User Group', 'required');
			$this->form_validation->set_rules('ActiveStatesID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('UserDesc', 'User Desc', '');
			
			$rec_ID_to_update = $post['UID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_user($post))
				{
					$this->session->set_flashdata('message', 
							'The User record has been updated successfully.');
					redirect('show-users');
				}
				else
				{
					$this->session->set_flashdata('error', 
							'There was an error updating the User record, please try again.');
//					redirect('change-user/' . $rec_ID_to_update);
					redirect('show-users');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->session->set_flashdata('error', 
							'Incorrect User Data Entered. Please try again.');
//				$this->session->set_flashdata('error', validation_errors());

			print "something is wrong";
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/user/pg_change_user_v2_4";
			$this->load->view('template/master', $data);
				
//				redirect('change-user/' . $rec_ID_to_update);
//				redirect('show-users');
				
			}
		}
		
	}												// EO Update User Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_user_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_user_v2_4()
	{
		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
//		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionUserARR['Email'];
		$data['session_data'] 	= $sessionUserARR;
		
		
//		$this->yprint("UPDATE User REC [ " . $rec_ID_to_update . "]",'',0);
		$this->yprintARR($post, "CREATE User INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			$data['page'] = 'pages_v2_4/user/pg_add_user_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('UID', 'User ID', 'required|numeric');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('UserTypesID', 'User Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
			
//			$this->form_validation->set_rules('UserDesc', 'User Desc', '');
			
//		$rec_ID_to_update = $post['UID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_user($post);
//   			$this->yprint("NEW User REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "User Record Created Successfully";
				$this->session->set_flashdata('success', 'User was created.');
	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_user" web page.
			
				$post['userREC'] 		= $this->main_model_v2_4->md_get_user_by_id($recID);

				$post['page'] = "pages_v2_4/user/pg_display_user_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$this->session->set_flashdata('error', 'Error Creating User Record.');
				$data['data_state'] = "Error Creatng User Record";
			}
			
		}	
		else 
		{									// Error in data entry
			$this->session->set_flashdata('error', 'Error Creating User Record.');
			$post['data_state'] = "Error Creatng User Record";
			$post['page'] = "pages_v2_4/user/pg_add_user_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create User Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_user_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display user.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_user_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		$this->yprint("DeleteThisUser REC [ " . $i_recID . "]",'',0);		// DBG
		
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_user_by_id($i_recID);   

//			$this->yprint("Display User REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "User UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
			$data['userREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/user/pg_deletethis_user_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display User Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_user_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a user record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_user_v2_4()
	{
		$data['data_state'] = "";

		$processYN 				= TRUE;
		$esc_module				= "display-user";

		/*......Get Session Info and check valid.....*/
		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
//		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionUserARR['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->session->set_flashdata('message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionUserARR['Email'];

		/*............Check That User Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->session->set_flashdata('message', 'Delete Aborted.' . $post['UID']);
				redirect('show-users');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->session->set_flashdata('message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/user/pg_show_users_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('UID', 'User ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['UID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionUserARR['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_user($rec_ID_to_delete))
				{
					$this->session->set_flashdata('message', 'User '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-users');
				}
				else
				{
					$this->session->set_flashdata('message', 'User '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-users');
				}
			}
				
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-users');
			}
		}

}											// EO Deleteit Func
		
	
	
	
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_users
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available users on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_users_v2_4()
	{

		$resultsARR = array();
		$data['session_data'] = $this->session->all_userdata();

		// Get the other data used to fill the link fields or external refs PRSC
		
//		$data['services'] 					= $this->main_model_v2_4->md_get_services();
//		$data['business_units'] 			= $this->main_model_v2_4->get_business_units();
//		$data['serviceareas']				= $this->main_model_v2_4->get_service_areas();

		// This shows a list of the Risks for users to reference
		$resultsARR							= $this->main_model_v2_4->md_get_users();

		if(!empty($resultsARR))
			{
			$data['usersARR']				= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			}
			
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['page'] = "pages_v2_4/user/pg_show_users_v2_4";
		$this->load->view('template/master', $data);
		
	}	
		
	

/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_disp_user
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This just shows a user and the details attached to it.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_display_user($i_recID = NULL)
	{

		$resultsARR = array();
		$data['session_data'] = $this->session->all_userdata();

		// Get the other data used to fill the link fields or external refs PRSC
		
//		$data['services'] 					= $this->main_model_v2_4->md_get_services();
//		$data['business_units'] 			= $this->main_model_v2_4->get_business_units();
//		$data['serviceareas']				= $this->main_model_v2_4->get_service_areas();

		// This shows a list of the Risks for users to reference
		$resultsARR							= $this->main_model_v2_4->md_get_user_by_id($i_recID);

		
		$this->yprintARR($resultsARR, "LOGIN REC Dump [" . $i_recID . "]", 0);    // DBG
		
		if(!empty($resultsARR))
			{
			$data['userREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

			$data['page'] = "pages_v2_4/user/pg_display_user_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'User '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
			redirect('show-users');
				
			}
			
	}	
	
	
	
//------------------------------------------------( EO User Functions Block )

	
	
//------------------------------------------------( SO Category Functions Block )

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-category" view file.
 		This will allow users to create and enter a new category
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_category_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['page'] = "pages_v2_4/category/pg_add_category_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('CID', 'Category ID', 'required');
			$this->form_validation->set_rules('Name', 'Category Name', 'required');

			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_category($post))
				{
					$this->session->set_flashdata('message',
						 'The Category record has been added successfull added.');
					redirect('add-category/'.$post['REG_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 
						'There was an error adding the Category record, please try again.');
					redirect('add-category');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('add-category');
			}
		}
		
	}										// EO Add Category Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-category" view file.
 		This will allow users to create and enter a new category
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_category_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

//		$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_category_by_id($i_recID);   

//			$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Category UPDATE INFO v2_4", 0);    	// DBG
			if(!empty($resultsARR))
			{
			$data['categoryREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/category/pg_display_category_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'Category '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
			redirect('show-categories');
			}
			
			
			
		}
		
	}											// EO Display Category Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_category_full_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-category" view file.
 		This will allow users to create and enter a new category
 		as requested.
 		
 		This will also show a full list of what users have access
 		under this Category.  
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_category_full_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

//		$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_category_by_id($i_recID);   

//			$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Category UPDATE INFO v2_4", 0);    	// DBG
			if(!empty($resultsARR))
			{
				
			/*................Get List of Associated Users.........*/	

			$accessListARR			= array();
			$usersHLDARR			= array();
			$categoryID				= $i_recID;	
			$wrkARR 				= $this->main_model_v2_4->md_get_access_users_in_category($categoryID);
			$usersARR				= $this->main_model_v2_4->md_get_users();
			$cntxx					= 0;

			
			foreach ($wrkARR as $workREC)
			{
			$wUID			   						= $workREC['UID'];	

//			$this->yprintARR($workREC, "AccessList", 0);    	// DBG
			
			foreach ($usersARR as $userREC)
				{
				if($userREC['UID'] == $wUID)
				{
					
//				$accessListARR[$cntxx]['FirstName']     = $usersHLDARR[$wUID]['FirstName'];	
//				$accessListARR[$cntxx]['LastName']      = $usersHLDARR[$wUID]['LastName'];	
//				$this->yprintARR($userREC, "UserRECMatch", 0);    	// DBG
					
				$accessListARR[$cntxx]      			= $userREC;	
				$cntxx++;
				}
				}
			}
			
//			$this->yprintARR($accessListARR, "UserList", 0);    	// DBG
			
			$data['usersARR'] 				= $accessListARR;
			
			/*................GET Reference Tables.................*/	
				
			$categoryID	= $i_recID;	
			$data['AccessList'] 				= $this->main_model_v2_4->md_get_access_users_in_category($categoryID);
			
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
				
//			$data['UsersARR'] 				= $this->main_model_v2_4->md_get_users_in_category($i_recID);
			
				
			$data['categoryREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/category/pg_display_category_full_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'Category '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
			redirect('show-categories');
			}
			
			
			
		}
		
	}											// EO Display Category Function
	
	
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_category_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display category.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_category_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		$this->yprint("DeleteThisCategory REC [ " . $i_recID . "]",'',0);		// DBG
		
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_category_by_id($i_recID);   

//			$this->yprint("Display Category REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Category UPDATE INFO v2_4", 0);    	// DBG
			
			$data['categoryREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/category/pg_deletethis_category_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Category Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-category"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_category_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";
		
//		$rec_ID_to_update = $this->uri->segment(3);
				

		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;
		}
		else
		{								// Error - Reshow Page
			$data['page'] = 'pages_v2_4/category/pg_change_category_v2_4';
			$this->load->view('template/master', $data);
		}

		$this->yprint("Change Category REC [ " . $rec_ID_to_update . "]",'',0);

		$data['categoryREC'] = $this->main_model_v2_4->md_get_category_by_id($rec_ID_to_update);

		$data['page'] = 'pages_v2_4/category/pg_change_category_v2_4';
		$this->load->view('template/master', $data);
		
		
	}												// EO Update Category Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_category_v2_4()
	{
		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		$post['LastModBy']		= $sessionUserARR['Email'];
		$data['session_data'] 	= $sessionUserARR;

		
		$post = $this->input->post(null, true);

//		$this->yprint("UPDATE Category REC [ " . $rec_ID_to_update . "]",'',0);
		$this->yprintARR($post, "UPDATE Category INFO v2_4", 0);    // DBG


		if(empty($post))
		{
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('CID', 'Category ID', 'required|numeric');
			$this->form_validation->set_rules('Name', 'Category Name', 'required');
			$this->form_validation->set_rules('CategoryDesc', 'Category Desc', '');
			
			$rec_ID_to_update = $post['CID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_category($post))
				{
					$this->session->set_flashdata('message', 
							'The Category record has been updated successfully.');
//					redirect('change-category/' . $rec_ID_to_update);
					redirect('show-categories');
				}
				else
				{
					$this->session->set_flashdata('error', 
							'There was an error updating the Category record, please try again.');
//					redirect('change-category/' . $rec_ID_to_update);
					redirect('show-categories');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->session->set_flashdata('error', validation_errors());
				redirect('change-category/' . $rec_ID_to_update);
				redirect('show-categories');
				
			}
		}
		
	}												// EO Update Category Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_category_v2_4()
	{
		
		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
//		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionUserARR['Email'];
		$data['session_data'] 	= $sessionUserARR;
		
		
//		$this->yprint("UPDATE Category REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprintARR($post, "CREATE Category INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			$data['page'] = 'pages_v2_4/category/pg_add_category_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('CID', 'Category ID', 'required|numeric');
			$this->form_validation->set_rules('Name', 'Category Name', 'required');
			$this->form_validation->set_rules('CategoryDesc', 'Category Desc', '');
			
//		$rec_ID_to_update = $post['CID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_category($post);
//   			$this->yprint("NEW Category REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "Category Record Created Successfully";
				$this->session->set_flashdata('success', 'Category was created.');

				// Create a brand new document holding directory 
    			$nwpath = "uploads/product";
				$nwpath = 'C:\EMOuploads';
//    			$nwpath = realpath('C:/inetpub/wwwroot/hrm_uploads/EMO/');

//				$recID = 199;						// DBG
				$nwNewDIR = $nwpath . '\\' . $recID;
//    			print "KKK [" . $nwNewDIR . "]";
//			    $resp = mkdir($nwNewDIR,0755,TRUE);
//	   			$this->yprint("RESP A [ " . $resp . "]",'',0);   // DBG
			    
    			$resp = is_dir($nwNewDIR);
//	   			$this->yprint("RESP B [ " . $resp . "]",'',0);   // DBG
    			if($resp != true) //create the folder if it's not already exists
			    {
//		   			$this->yprint("NEW Category REC [ " . $nwNewDIR . "]",'',0);   // DBG
					$resp = mkdir($nwNewDIR,0755,TRUE);
					$this->session->set_flashdata('message', 
				  		'Directory [' . $recID . '] was Successfully created. Code [' . $resp . "]");
			    }
   				else
   				{						// Directory exists - Warn user
	   				$this->yprint("NEW Category REC [ " . $nwNewDIR . "] EXISTS",'',0);   // DBG
   					$this->session->set_flashdata('message', 
						'WARNING - Base Directory already exists');
   				}
				
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_category" web page.
			
			$post['categoryREC'] 		= $this->main_model_v2_4->md_get_category_by_id($recID);

			$post['page'] = "pages_v2_4/category/pg_display_category_v2_4";
			$this->load->view('template/master', $post);
			}
		 else
		 	{ 	
			$this->session->set_flashdata('error', 'Error Creating Category Record.');
			$data['data_state'] = "Error Creatng Category Record";
		 	}
		}									// If Form validation is true-false
		else 
		{									// Error in data entry
			$this->session->set_flashdata('error', 'Data Entry Error');
			$post['data_state'] = "Error Creatng Some Category fields";
			$post['page'] = "pages_v2_4/catagory/pg_add_category_v2_4";
			$this->load->view('template/master', $post);
			
		}
		
	}										// EO If valid data post
		
}											// EO Create Category Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_category_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_31
 
 		This function is the controller->director function that 
 		processes the create request to display the "update-category"
 		view.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr2_create_category_v2_4()
	{	
		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "display-category";
		$post = $this->input->post(NULL,TRUE);

//		$this->yprint("CREATE Category REC [ " . $i_recID . "]",'',0);
		$this->yprintARR($post, "Category CREATE INFO v2_4", 0);    // DBG

		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		
		$post['CreatedBy']		= $sessionUserARR['Email'];
		$data['session_data'] 	= $sessionUserARR;
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

					
		$file_data = array();

//		$this->form_validation->set_rules('CID', 'Category ID', 'required|numeric');
		$this->form_validation->set_rules('Name', 'CategoryShortName', 'required');
		$this->form_validation->set_rules('Desc', 'CategoryDesc', 'required');
		
		if ($this->form_validation->run() == true)
				$process = TRUE;
					
						
		/*........Determine if to create or reject.........*/		
				
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		  $this->session->set_flashdata('error', 'Form Requires Data. Please Re-enter Fields Correctly.');
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model_v2_4->md_create_category($post))	
			{
					$data['data_state'] = "Category Record Created Successfully";
					$this->session->set_flashdata('success', 'Category was created.');
					
			} else { 	
					$this->session->set_flashdata('error', 'Error Creating Category Record.');
					$data['data_state'] = "Error Creatng Category Record";
			}
		}

		// This goes to database to get the data used to initialize the 
		// boxes in the "pg_add_category" web page.
			
		$data['categoryREC'] 		= $this->main_model_v2_4->md_get_category_by_id($id);

		$post['page'] = "pages_v2_4/category/pg_display_category_v2_4";
		$this->load->view('template/master', $post);
		
}
	
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_category_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a category record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteCNIU_category_v2_4($i_recID = NULL)
	{
		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "display-category";

//		$this->yprint("CREATE Category REC [ " . $i_recID . "]",'',0);
		$this->yprintARR($post, "Category CREATE INFO v2_4", 0);    // DBG

		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		
//		$post['CreatedBy']		= $sessionUserARR['Email'];
		$data['session_data'] 	= $sessionUserARR;

//		$rec_ID_to_delete = $this->input->post('CID');
		$rec_ID_to_delete = $i_recID;

		$this->yprint("DELETE Category REC [ " . $i_recID . "]",'',0);
		
		if($rec_ID_to_delete)
		{
			if ($this->main_model_v2_4->md_delete_category($rec_ID_to_delete))
			{
				$this->session->set_flashdata('message', 'Category '. $rec_ID_to_delete . ' has been removed.');
				$this->yprint("DELETE Success [ " . $i_recID . "]",'',0);
				
				redirect('show-categories');
			}
			else
			{
				$this->session->set_flashdata('message', 'Category '.$rec_ID_to_delete.' could not be removed, please try again.');
				$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
				redirect('show-categories');
			}
		}
	}
		

	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_category_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a category record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_category_v2_4()
	{
		$data['data_state'] = "";

		$processYN 				= TRUE;
		$esc_module				= "display-category";

		/*......Get Session Info and check valid.....*/
		$sessionUserARR = array();
		$sessionUserARR = $this->session->all_userdata();
//		$this->yprintARR($sessionUserARR, "SESSION", 0);    // DBG
		
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionUserARR['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->session->set_flashdata('message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionUserARR['Email'];

		/*............Check That User Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->session->set_flashdata('message', 'Delete Aborted.' . $post['CID']);
				redirect('show-categories');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->session->set_flashdata('message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/category/pg_show_categories_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('CID', 'Category ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['CID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionUserARR['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_category($rec_ID_to_delete))
				{
					$this->session->set_flashdata('message', 'Category '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-categories');
				}
				else
				{
					$this->session->set_flashdata('message', 'Category '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-categories');
				}
			}
				
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-categories');
			}
		}

}											// EO Deleteit Func
		
	
	
	
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_categories
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available categories on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_categories_v2_4()
	{

		$resultsARR = array();
		$data['session_data'] = $this->session->all_userdata();

		// Get the other data used to fill the link fields or external refs PRSC
		
//		$data['services'] 					= $this->main_model_v2_4->md_get_services();
//		$data['business_units'] 			= $this->main_model_v2_4->get_business_units();
//		$data['serviceareas']				= $this->main_model_v2_4->get_service_areas();

		// This shows a list of the Risks for users to reference
		$resultsARR							= $this->main_model_v2_4->md_get_categories();
		
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['categoriesARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/category/pg_show_categories_v2_4";
		$this->load->view('template/master', $data);
		
	}	
		
	

	
//<<<<<----------------------------------------------------[ EO Categories ]


//------------------------------------------------( SO Document Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-document" view file.
 		This will allow documents to create and enter a new document
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_document_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//			$data['DocumentTypesARR'] 			= $this->main_model_v2_4->md_get_document_types();
						
//			$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/document/pg_add_document_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('DocID', 'Document ID', 'required');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('DocumentTypesID', 'Document Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('DocumentDesc', 'Document Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_document($post))
				{
					$this->session->set_flashdata('message',
						 'The Document record has been added successfull added.');
					redirect('display-document/'.$post['REG_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 
						'There was an error adding the Document record, please try again.');
					redirect('display-document');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('display-document');
			}
		}
		
	}										// EO Add Document Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-document" view file.
 		This will allow documents to create and enter a new document
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_document_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

//		$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_document_by_id($i_recID);
//		$this->yprintARR($resultsARR, "DocumentDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['documentREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
			
			$data['page'] = "pages_v2_4/document/pg_display_document_UL_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'Document '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-documents');
			}
			
		}
		
	}											// EO Display Document Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-document"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_document_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";
		
//		$rec_ID_to_update = $this->uri->segment(3);
				

		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//			$data['DocumentTypesARR'] 			= $this->main_model_v2_4->md_get_document_types();
			
		}
		else
		{								// Error - Reshow Page
			$this->session->set_flashdata('error', 
						'No valid record ID specified. Please try again');
			$data['page'] = 'pages_v2_4/document/pg_change_document_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_document_by_id($rec_ID_to_update);
		$this->yprintARR($resultsARR, "DocumentDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change Document REC [ " . $i_recID . "] has been Updated",'',0);
//			$this->session->set_flashdata('message', 
//						"Change Document REC [ " . $i_recID . "] has been Updated");
			
			$data['documentREC']  = $resultsARR;
			$data['page'] = 'pages_v2_4/document/pg_change_document_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->session->set_flashdata('error', 
						'Data for that Document not Available. Please try again');
			$data['page'] = 'pages_v2_4/document/pg_change_document_v2_4';
			redirect('show-documents');
		}
		
	}												// EO Update Document Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_document_v2_4()
	{
		$sessionDocumentARR = array();
		$sessionDocumentARR = $this->session->all_userdata();
		$this->yprintARR($sessionDocumentARR, "SESSION", 0);    // DBG
		$post['LastModBy']		= $sessionDocumentARR['Email'];
		$data['session_data'] 	= $sessionDocumentARR;

		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionDocumentARR['Email'];
		
//		$this->yprint("UPDATE Document REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionDocumentARR['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
//		$this->yprintARR($post, "UPDATE Document INFO v2_4", 0);    // DBG
		
		
		if(empty($post))
		{
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('DocID', 'Document ID', 'required|numeric');
			$this->form_validation->set_rules('DocShortName', 'Document Name', 'required');
			$this->form_validation->set_rules('Version', 'Version Number', 'required');
			//			$this->form_validation->set_rules('DocumentDesc', 'Document Desc', '');
			
			$rec_ID_to_update = $post['DocID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_document($post))
				{
					$this->session->set_flashdata('message', 
							'The Document record has been updated successfully.');
					redirect('show-documents');
				}
				else
				{
					$this->session->set_flashdata('error', 
							'There was an error updating the Document record, please try again.');
//					redirect('change-document/' . $rec_ID_to_update);
					redirect('show-documents');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->session->set_flashdata('error', 
							'Incorrect Document Data Entered. Please try again.');
//				$this->session->set_flashdata('error', validation_errors());

			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/document/pg_change_document_v2_4";
			$this->load->view('template/master', $data);
				
//				redirect('change-document/' . $rec_ID_to_update);
//				redirect('show-documents');
				
			}
		}
		
	}												// EO Update Document Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_document_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_document_v2_4()
	{
		$sessionDocumentARR = array();
		$sessionDocumentARR = $this->session->all_userdata();
//		$this->yprintARR($sessionDocumentARR, "SESSION", 0);    // DBG
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionDocumentARR['Email'];
		$data['session_data'] 	= $sessionDocumentARR;
		
		
//		$this->yprint("UPDATE Document REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprintARR($post, "CREATE Document INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			$data['page'] = 'pages_v2_4/document/pg_add_document_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('DocID', 'Document ID', 'required|numeric');
			$this->form_validation->set_rules('DocShortName', 'Document Name', 'required');
			$this->form_validation->set_rules('Version', 'Version Number', 'required');
			
//			$this->form_validation->set_rules('DocumentDesc', 'Document Desc', '');
			
//		$rec_ID_to_update = $post['DocID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_document($post);
//   			$this->yprint("NEW Document REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "Document Record Created Successfully";
				$this->session->set_flashdata('success', 'Document was created.');
	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_document" web page.
			
				$post['documentREC'] 		= $this->main_model_v2_4->md_get_document_by_id($recID);

			// Get Necessary Reference Data used in Screen
				$post['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//				$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
				$post['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//				$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types();
				
				$post['page'] = "pages_v2_4/document/pg_display_document_UL_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$this->session->set_flashdata('error', 'Error Creating Document Record.');
				$data['data_state'] = "Error Creatng Document Record";
			}
			
		}	
		else 
		{									// Error in data entry
			$this->session->set_flashdata('error', 'Error Creating Document Record.');
			$post['data_state'] = "Error Creatng Document Record";

			// Get Necessary Reference Data used in Screen
			$post['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$post['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//			$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types();
			
			$post['page'] = "pages_v2_4/document/pg_add_document_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create Document Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_document_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display document.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_document_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		$this->yprint("DeleteThisDocument REC [ " . $i_recID . "]",'',0);		// DBG
		
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_document_by_id($i_recID);   

//			$this->yprint("Display Document REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Document UPDATE INFO v2_4", 0);    	// DBG

			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
						
			$data['documentREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/document/pg_deletethis_document_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Document Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_document_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a document record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_document_v2_4()
	{
		$data['data_state'] = "";

		$processYN 				= TRUE;
		$esc_module				= "display-document";

		/*......Get Session Info and check valid.....*/
		$sessionDocumentARR = array();
		$sessionDocumentARR = $this->session->all_userdata();
//		$this->yprintARR($sessionDocumentARR, "SESSION", 0);    // DBG
		
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionDocumentARR['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->session->set_flashdata('message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionDocumentARR['Email'];

		/*............Check That Document Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->session->set_flashdata('message', 'Delete Aborted.' . $post['DocID']);
				redirect('show-documents');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->session->set_flashdata('message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/document/pg_show_documents_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('DocID', 'Document ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['DocID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionDocumentARR['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_document($rec_ID_to_delete))
				{
					$this->session->set_flashdata('message', 'Document '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-documents');
				}
				else
				{
					$this->session->set_flashdata('message', 'Document '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-documents');
				}
			}
				
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-documents');
			}
		}

}											// EO Deleteit Func
		
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_documents_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available documents on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_documents_v2_4()
	{

		$resultsARR = array();
		$data['session_data'] = $this->session->all_userdata();

		// Get Necessary Reference Data used in Screen
		$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
//		$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
		$data['ActiveStatesARR'] 	= $this->main_model_v2_4->md_get_active_states();
//		$data['DocumentTypesARR'] 	= $this->main_model_v2_4->md_get_document_types();
		
		// This shows a list of the Risks for documents to reference
		$resultsARR							= $this->main_model_v2_4->md_get_documents();
		
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['documentsARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/document/pg_show_documents_v2_4";
		$this->load->view('template/master', $data);
		
	}	
	
//------------------------------------------------( EO Document Functions Block )

	

//------------------------------------------------( SO Access Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-access" view file.
 		This will allow accesses to create and enter a new access
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_access_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
//			$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/access/pg_add_access_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('AID', 'Access ID', 'required');
			$this->form_validation->set_rules('Email', 'EMail / AccountID', 'required');
			$this->form_validation->set_rules('Password', 'Password', 'required');
			$this->form_validation->set_rules('FirstName', 'First Name', 'required');
			$this->form_validation->set_rules('LastName', 'Last Name', 'required');
			$this->form_validation->set_rules('AccessTypesID', 'Access Group', 'required');
			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('AccessDesc', 'Access Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_access($post))
				{
					$this->session->set_flashdata('message',
						 'The Access record has been added successfull added.');
					redirect('display-access/'.$post['REG_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 
						'There was an error adding the Access record, please try again.');
					redirect('display-access');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('display-access');
			}
		}
		
	}										// EO Add Access Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-access" view file.
 		This will allow accesses to create and enter a new access
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_access_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

//		$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_access_by_id($i_recID);
//		$this->yprintARR($resultsARR, "AccessDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['accessREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
			$data['page'] = "pages_v2_4/access/pg_display_access_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'Access '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-accesses');
			}
			
		}
		
	}											// EO Display Access Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-access"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_access_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";
		
//		$rec_ID_to_update = $this->uri->segment(3);
				

		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			// Get Necessary Reference Data used in Screen
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
		}
		else
		{								// Error - Reshow Page
			$this->session->set_flashdata('error', 
						'No valid record ID specified. Please try again');
			$data['page'] = 'pages_v2_4/access/pg_change_access_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_access_by_id($rec_ID_to_update);
		$this->yprintARR($resultsARR, "AccessDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change Access REC [ " . $i_recID . "] has been Updated",'',0);
//			$this->session->set_flashdata('message', 
//						"Change Access REC [ " . $i_recID . "] has been Updated");
			
			$data['accessREC']  = $resultsARR;
			$data['page'] = 'pages_v2_4/access/pg_change_access_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->session->set_flashdata('error', 
						'Data for that Access not Available. Please try again');
			$data['page'] = 'pages_v2_4/access/pg_change_access_v2_4';
			redirect('show-accesses');
		}
		
	}												// EO Update Access Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_access_v2_4()
	{
		$sessionAccessARR = array();
		$sessionAccessARR = $this->session->all_userdata();
		$this->yprintARR($sessionAccessARR, "SESSION", 0);    // DBG
		$post['LastModBy']		= $sessionAccessARR['Email'];
		$data['session_data'] 	= $sessionAccessARR;

		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionAccessARR['Email'];
		
//		$this->yprint("UPDATE Access REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionAccessARR['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
//		$this->yprintARR($post, "UPDATE Access INFO v2_4", 0);    // DBG
		
		
		if(empty($post))
		{
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('AID', 'Access ID', 'required|numeric');
			$this->form_validation->set_rules('CID', 'Category', 'required');
			$this->form_validation->set_rules('UID', 'User', 'required');

//			$this->form_validation->set_rules('AccessDesc', 'Access Desc', '');
			
			$rec_ID_to_update = $post['AID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_access($post))
				{
					$this->session->set_flashdata('message', 
							'The Access record has been updated successfully.');
					redirect('show-accesses');
				}
				else
				{
					$this->session->set_flashdata('error', 
							'There was an error updating the Access record, please try again.');
//					redirect('change-access/' . $rec_ID_to_update);
					redirect('show-accesses');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->session->set_flashdata('error', 
							'Incorrect Access Data Entered. Please try again.');
//				$this->session->set_flashdata('error', validation_errors());

			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/access/pg_change_access_v2_4";
			$this->load->view('template/master', $data);
				
//				redirect('change-access/' . $rec_ID_to_update);
//				redirect('show-accesses');
				
			}
		}
		
	}												// EO Update Access Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_access_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_access_v2_4()
	{
		$sessionAccessARR = array();
		$sessionAccessARR = $this->session->all_userdata();
//		$this->yprintARR($sessionAccessARR, "SESSION", 0);    // DBG
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionAccessARR['Email'];
		$data['session_data'] 	= $sessionAccessARR;
		
		
//		$this->yprint("UPDATE Access REC [ " . $rec_ID_to_update . "]",'',0);
		$this->yprintARR($post, "CREATE Access INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			$data['page'] = 'pages_v2_4/access/pg_add_access_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('AID', 'Access ID', 'required|numeric');
			$this->form_validation->set_rules('CID', 'Category', 'required');
			$this->form_validation->set_rules('UID', 'User', 'required');
			
//			$this->form_validation->set_rules('AccessDesc', 'Access Desc', '');
			
//		$rec_ID_to_update = $post['AID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_access($post);
//   			$this->yprint("NEW Access REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "Access Record Created Successfully";
				$this->session->set_flashdata('success', 'Access was created.');
	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_access" web page.
			
				$post['accessREC'] 		= $this->main_model_v2_4->md_get_access_by_id($recID);

				$post['page'] = "pages_v2_4/access/pg_display_access_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$this->session->set_flashdata('error', 'Error Creating Access Record.');
				$data['data_state'] = "Error Creatng Access Record";
			}
			
		}	
		else 
		{									// Error in data entry
			$this->session->set_flashdata('error', 'Error Creating Access Record.');
			$post['data_state'] = "Error Creatng Access Record";
			$post['page'] = "pages_v2_4/access/pg_add_access_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create Access Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_access_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display access.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_access_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		$this->yprint("DeleteThisAccess REC [ " . $i_recID . "]",'',0);		// DBG
		
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_access_by_id($i_recID);   

//			$this->yprint("Display Access REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Access UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
			$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			
			$data['accessREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/access/pg_deletethis_access_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Access Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_access_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a access record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_access_v2_4()
	{
		$data['data_state'] = "";

		$processYN 				= TRUE;
		$esc_module				= "display-access";

		/*......Get Session Info and check valid.....*/
		$sessionAccessARR = array();
		$sessionAccessARR = $this->session->all_userdata();
//		$this->yprintARR($sessionAccessARR, "SESSION", 0);    // DBG
		
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionAccessARR['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->session->set_flashdata('message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionAccessARR['Email'];

		/*............Check That Access Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->session->set_flashdata('message', 'Delete Aborted.' . $post['AID']);
				redirect('show-accesses');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->session->set_flashdata('message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/access/pg_show_accesses_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('AID', 'Access ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['AID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionAccessARR['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_access($rec_ID_to_delete))
				{
					$this->session->set_flashdata('message', 'Access '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-accesses');
				}
				else
				{
					$this->session->set_flashdata('message', 'Access '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-accesses');
				}
			}
				
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-accesses');
			}
		}

}											// EO Deleteit Func
		
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_accesses_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available accesses on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_accesses_v2_4()
	{

		$resultsARR = array();
		$data['session_data'] = $this->session->all_userdata();

		// Get the other data used to fill the link fields or external refs PRSC
		
//		$data['serviceareas']				= $this->main_model_v2_4->get_service_areas();

		// This shows a list of the Risks for accesses to reference
		$resultsARR							= $this->main_model_v2_4->md_get_accesses();

		/*....................Load REF Data..........PRSC*/
		$data['CategoriesARR'] 		= $this->main_model_v2_4->md_get_categories();
		$data['UsersARR'] 			= $this->main_model_v2_4->md_get_users();
		
		
//	$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['accessesARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/access/pg_show_accesses_v2_4";
		$this->load->view('template/master', $data);
		
	}	
	
	
	

//------------------------------------------------( SO Subcat Functions Block )

	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_add_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-subcat" view file.
 		This will allow subcats to create and enter a new subcat
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
*  		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_add_subcat_v2_4($rec_ID = NULL)
	{	
		$data['data_state'] = "";
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
//			$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($data['ActiveStatesARR'], "Options ", 0);    	// DBG
			
			$data['page'] = "pages_v2_4/subcat/pg_add_subcat_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required');
			$this->form_validation->set_rules('SubcatShortName', 'Subcat Short Name', 'required');
			
//			$this->form_validation->set_rules('UserTypesID', 'Subcat Group', 'required');
//			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
			
			$file_data = array();

			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_create_subcat($post))
				{
					$this->session->set_flashdata('message',
						 'The Subcat record has been added successfull added.');
					redirect('display-subcat/'.$post['REG_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 
						'There was an error adding the Subcat record, please try again.');
					redirect('display-subcat');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('display-subcat');
			}
		}
		
	}										// EO Add Subcat Function

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_display_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-subcat" view file.
 		This will allow subcats to create and enter a new subcat
 		as requested.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
*
* * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_display_subcat_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

//		$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
		$resultsARR					= $this->main_model_v2_4->md_get_subcat_by_id($i_recID);
//		$this->yprintARR($resultsARR, "SubcatDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
			{
			$data['subcatREC']			= $resultsARR;
				
			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();

			$data['page'] = "pages_v2_4/subcat/pg_display_subcat_v2_4";
			$this->load->view('template/master', $data);
			}
			else 
			{
			$this->session->set_flashdata('message', 'Subcat '.$i_recID .' could not be accessed. Please Call Tech Suppor.');
//			$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
	//		redirect('show-subcats');
			}
			
		}
		
	}											// EO Display Subcat Function
	
	
	
	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_change_update_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
 		This function is the controller->director function that 
 		processes the updated values to display the "update-subcat"
 		view.
 		
 		Note - This only works when you past arguments on command line.
 		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_change_subcat_v2_4($i_recID = NULL)
	{

		$data = array();
		$data['data_state'] 		= "";
		
//		$rec_ID_to_update = $this->uri->segment(3);
				

		if($i_recID)
		{								// Arguement pasted on Command line
			$rec_ID_to_update = $i_recID;

			// Get Necessary Reference Data used in Screen
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 		= $this->main_model_v2_4->md_get_user_types();
			
		}
		else
		{								// Error - Reshow Page
			$this->session->set_flashdata('error', 
						'No valid record ID specified. Please try again');
			$data['page'] = 'pages_v2_4/subcat/pg_change_subcat_v2_4';
			$this->load->view('template/master', $data);
		}

		$resultsARR = $this->main_model_v2_4->md_get_subcat_by_id($rec_ID_to_update);
		$this->yprintARR($resultsARR, "SubcatDATA ", 0);    	// DBG
		
		if(!empty($resultsARR))
		{		
//			$this->yprint("Change Subcat REC [ " . $i_recID . "] has been Updated",'',0);
//			$this->session->set_flashdata('message', 
//						"Change Subcat REC [ " . $i_recID . "] has been Updated");
			
			$data['subcatREC']  = $resultsARR;
			$data['page'] = 'pages_v2_4/subcat/pg_change_subcat_v2_4';
			$this->load->view('template/master', $data);
		}
		else
		{
			$this->session->set_flashdata('error', 
						'Data for that Subcat not Available. Please try again');
			$data['page'] = 'pages_v2_4/subcat/pg_change_subcat_v2_4';
			redirect('show-subcats');
		}
		
	}												// EO Update Subcat Function
	

/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_update_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after new data is entered and then the data is
		written out to the database.  This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_update_subcat_v2_4()
	{
		$data['data_state'] 		= "";
		
		$sessionSubcatARR = array();
		$sessionSubcatARR = $this->session->all_userdata();
		$this->yprintARR($sessionSubcatARR, "SESSION", 0);    // DBG
		$post['LastModBy']		= $sessionSubcatARR['Email'];
		$data['session_data'] 	= $sessionSubcatARR;

		$post = $this->input->post(null, true);

		// TODO Replace this with Error Test
		$post['LastModBy']		= $sessionSubcatARR['Email'];
		
//		$this->yprint("UPDATE Subcat REC [ " . $rec_ID_to_update . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $sessionSubcatARR['Email'] . "]",'',0);
//		$this->yprint("UPDATE Email Modby [ " . $data['LastModBy'] . "]",'',0);
//		$this->yprintARR($post, "UPDATE Subcat INFO v2_4", 0);    // DBG
		
		
		if(empty($post))
		{
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$file_data = array();

			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required');
			$this->form_validation->set_rules('SubcatShortName', 'Subcat Short Name', 'required');
			
//			$this->form_validation->set_rules('UserTypesID', 'Subcat Group', 'required');
//			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
						
			$rec_ID_to_update = $post['SCID'];
			
			if ($this->form_validation->run() == true)
			{
				if ($this->main_model_v2_4->md_update_subcat($post))
				{
					$this->session->set_flashdata('message', 
							'The Subcat record has been updated successfully.');
					redirect('show-subcats');
				}
				else
				{
					$this->session->set_flashdata('error', 
							'There was an error updating the Subcat record, please try again.');
//					redirect('change-subcat/' . $rec_ID_to_update);
					redirect('show-subcats');
				}
			}
			else
			{						/* Validation Failed - Redisplay */
				$this->session->set_flashdata('error', 
							'Incorrect Subcat Data Entered. Please try again.');
//				$this->session->set_flashdata('error', validation_errors());

			$this->session->set_flashdata('error', 
						'There is no record ID specified. Update Aborted.');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_change_subcat_v2_4";
			$this->load->view('template/master', $data);
				
//				redirect('change-subcat/' . $rec_ID_to_update);
//				redirect('show-subcats');
				
			}
		}
		
	}												// EO Update Subcat Function
	

	
/* ------------------------------------------------------------------------
 *           
 		FUNCTION:	ctr_create_subcat_v2_4
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	2017_05_30
 
		This is called after entering new data, to make a new record. 
		This assumes a Form submission.
		
 		ARGS:		$rec_ID
 		
 		RETURNS:	n/a
* 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170530 	PRSC	Fixed this for new argument passing protocol.
* 		
* --------------------------------------------------------------------------
*/ 		
	
	public function ctr_create_subcat_v2_4()
	{
		$sessionSubcatARR = array();
		$sessionSubcatARR = $this->session->all_userdata();
//		$this->yprintARR($sessionSubcatARR, "SESSION", 0);    // DBG
		
		$post = $this->input->post(null, true);

		$post['CreatedBy']		= $sessionSubcatARR['Email'];
		$data['session_data'] 	= $sessionSubcatARR;
		
		
//		$this->yprint("UPDATE Subcat REC [ " . $rec_ID_to_update . "]",'',0);
		$this->yprintARR($post, "CREATE Subcat INFO v2_4", 0);    // DBG

		if(empty($post))
		{
			$data['page'] = 'pages_v2_4/subcat/pg_add_subcat_v2_4';
			$this->load->view('template/master', $post);
		}
		else
		{
			$file_data = array();

//			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required|numeric');
			$this->form_validation->set_rules('SubcatShortName', 'Subcat Short Name', 'required');
			
//			$this->form_validation->set_rules('UserTypesID', 'Subcat Group', 'required');
//			$this->form_validation->set_rules('ActiveStateID', 'Active Yes No', 'required');
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
						
//			$this->form_validation->set_rules('SubcatDesc', 'Subcat Desc', '');
			
//		$rec_ID_to_update = $post['SCID'];

		if ($this->form_validation->run() == true)
		{
			$recID = 0;
			$recID = $this->main_model_v2_4->md_create_subcat($post);
//   			$this->yprint("NEW Subcat REC [ " . $recID . "]",'',0);   // DBG
			
			if($recID)	
			{
				$data['data_state'] = "Subcat Record Created Successfully";
				$this->session->set_flashdata('success', 'Subcat was created.');
	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_subcat" web page.
			
				$post['subcatREC'] 		= $this->main_model_v2_4->md_get_subcat_by_id($recID);

				$post['page'] = "pages_v2_4/subcat/pg_display_subcat_v2_4";
				$this->load->view('template/master', $post);
					
			}
			else
			{ 	
				$this->session->set_flashdata('error', 'Error Creating Subcat Record.');
				$data['data_state'] = "Error Creatng Subcat Record";
			}
			
		}	
		else 
		{									// Error in data entry
			$this->session->set_flashdata('error', 'Error Creating Subcat Record.');
			$post['data_state'] = "Error Creatng Subcat Record";
			$post['page'] = "pages_v2_4/subcat/pg_add_subcat_v2_4";
			$this->load->view('template/master', $post);
		}
	}										// EO If post not empty
		
}											// EO Create Subcat Function
	
	
/* ------------------------------------------------------------------------
 *           
 *		FUNCTION:	ctr_deletethis_subcat_v2_4
 *		AUTHOR:		R.Stephen Chafe (Zen River Software)
 *		CREATED:	2017_05_30
 *
 *		This simply loads the "Do you want to delete this"
 *		screen which is very much like display subcat.  
 *		This is safer to use than JS driven dialogs.
 * 		
 *		ARGS:		$rec_ID
 *		
 *		RETURNS:	n/a
 *
 * 		
* --------------------------------------------------------------------------
* 	MODIFICATION HISTORY
* 
* 	20170517 	PRSC	Fixed this for new argument passing protocol.
* 		
*--------------------------------------------------------------------------
*/ 		
 
	public function ctr_deletethis_subcat_v2_4($i_recID = NULL)
	{	
		$data['data_state'] = "";
		$resultsARR			= array();

		$this->yprint("DeleteThisSubcat REC [ " . $i_recID . "]",'',0);		// DBG
		
		
		if (empty($i_recID))
		{						/* ID Not Defined */
			$this->session->set_flashdata('error', 
						'There is no record ID specified. Please try again');
			
			$data['page'] 	= "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			$resultsARR = $this->main_model_v2_4->md_get_subcat_by_id($i_recID);   

//			$this->yprint("Display Subcat REC [ " . $i_recID . "]",'',0);		// DBG
//			$this->yprintARR($resultsARR, "Subcat UPDATE INFO v2_4", 0);    	// DBG

			/*....................Load REF Data..........PRSC*/
			$data['ActiveStatesARR'] 		= $this->main_model_v2_4->md_get_active_states();
			$data['UserTypesARR'] 			= $this->main_model_v2_4->md_get_user_types();
			
			$data['subcatREC']	= $resultsARR;
			$data['page'] 			= "pages_v2_4/subcat/pg_deletethis_subcat_v2_4";
			$this->load->view('template/master', $data);
		}
		
	}											// EO Display Subcat Function
	
	
/*-------------------------------------------------------------------------
 * 
 * 		FUNCTION:	delete_subcat_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017-05-30
 * 
 * 		This Controller handles deletion of a subcat record.
 * 		Not to be confused with deleting a simple upload (see below).
 *
 *  	RETURNS:	N/A 
 *--------------------------------------------------------------------------
 * 
 * MODIFICATION HISTORY
 * 
 * 20170530 	PRSC	Fixed this for new argument passing protocol.
 * 				PRSC	Also changed to use virtual paths.
 *
 *--------------------------------------------------------------------------
 */

	public function ctr_deleteit_subcat_v2_4()
	{
		$data['data_state'] = "";

		$processYN 				= TRUE;
		$esc_module				= "display-subcat";

		/*......Get Session Info and check valid.....*/
		$sessionSubcatARR = array();
		$sessionSubcatARR = $this->session->all_userdata();
//		$this->yprintARR($sessionSubcatARR, "SESSION", 0);    // DBG
		
		
		//Load Model
//		$this->load->model('main_model_v2_4');
		$post = $this->input->post(null, true);

//		if($post['LastModBy'] !=  $sessionSubcatARR['Email'])
//		{
			// Raise Alarm and redirect to login
//			$this->session->set_flashdata('message',
//						 'Security Alert.');
//			redirect('ctl-signin');
//			
//		}

//		$this->yprintARR($post, "Delete IT v2_4", 0);    // DBG

		$post['LastModBy']   = $sessionSubcatARR['Email'];

		/*............Check That Subcat Confirmed to Delete Else Bail..........*/
		if(!empty($post['choice']))
		{
			if($post['choice'] != 'YES')
			{
				$this->session->set_flashdata('message', 'Delete Aborted.' . $post['SCID']);
				redirect('show-subcats');
			}
		}	
		
		if (empty($post))
		{
			// Note this should signal a hacking error condition attempt - PRSC
			$this->session->set_flashdata('message',
						 'Invalid Data Entry.');
			$data['page'] = "pages_v2_4/subcat/pg_show_subcats_v2_4";
			$this->load->view('template/master', $data);
		}
		else
		{
			
			$this->form_validation->set_rules('SCID', 'Subcat ID', 'required');
//			$this->form_validation->set_rules('LastModBy', 'LastModBy', 'required');

			$file_data = array();

			$rec_ID_to_delete = $post['SCID'];
			$i_recID = $rec_ID_to_delete;

			$post['LastModBy']   = $sessionSubcatARR['Email'];
			
			if ($this->form_validation->run() == true)
			{
				
			if($rec_ID_to_delete)
				{
				if ($this->main_model_v2_4->md_delete_subcat($rec_ID_to_delete))
				{
					$this->session->set_flashdata('message', 'Subcat '. $rec_ID_to_delete . ' has been removed.');
//					$this->yprint("DELETE Successful [ " . $i_recID . "]",'',0);
					redirect('show-subcats');
				}
				else
				{
					$this->session->set_flashdata('message', 'Subcat '.$rec_ID_to_delete.' could not be removed, please try again.');
//					$this->yprint("DELETE Failed [ " . $i_recID . "]",'',0);
					redirect('show-subcats');
				}
			}
				
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
//				$this->yprint("Errors have occurred [ " . $i_recID . "]",'',0);
				redirect('show-subcats');
			}
		}

}											// EO Deleteit Func
		
	
/*----------------------------------------------------------------------------
 * 
 * 		FUNCTION: 	ctr_show_subcats_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	Unknown
 * 
 * 		This is the controller that carries out listing all of the
 * 		available subcats on one screen.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-29		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */

	public function ctr_show_subcats_v2_4()
	{

		$resultsARR = array();
		$data['session_data'] = $this->session->all_userdata();

		// Get the other data used to fill the link fields or external refs PRSC
		
//		$data['services'] 					= $this->main_model_v2_4->md_get_services();
//		$data['business_units'] 			= $this->main_model_v2_4->get_business_units();
//		$data['serviceareas']				= $this->main_model_v2_4->get_service_areas();

		// This shows a list of the Risks for subcats to reference
		$resultsARR							= $this->main_model_v2_4->md_get_subcats();
		
//		$this->yprintARR($resultsARR, "LOGIN REC Dump", 0);    // DBG
		
		$data['subcatsARR']			= $resultsARR;
		$data['page'] = "pages_v2_4/subcat/pg_show_subcats_v2_4";
		$this->load->view('template/master', $data);
		
	}	
	
//------------------------------------------------( EO Subcat Functions Block )

	
	
	
/* 	
=============================================================================================
 signup() - Enables people to join and create new accounts using an email address.
---------------------------------------------------------------------------------------------
 Registration: Step 1 of 2
---------------------------------------------------------------------------------------------
 - validate submitted information
 - encrypt email address to generate activation code
 - check if that email adderss already exist in the system
 - add a new unactivated user to the database
 - send activation email with activation link that sends them to activation function below
 - redirect with confirmation message, otherwise redirect to  with error
=============================================================================================
*/
	public function ctr_signup()
	{
		if($data = $this->input->post(null, true))
		{
			$this->form_validation->set_rules('first_name', 'first name', 'trim|min_length[2]|required');
			$this->form_validation->set_rules('last_name', 'last name', 'trim|min_length[2]|required');
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
			// $this->form_validation->set_rules('password_confirmation', 'confirm password', 'trim|matches[password]|required');

			if($this->form_validation->run() != false)
			{
				$activation_code = $this->encrypt->encode($data['email_address']);

				// check if that email adderss already exist in the system
				if(!$this->user_model->email_address_exist($data['email_address']))
				{
					if($this->user_model->add_user($data))
					{
					    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
						$this->email->to($data['email_address']); 
					    $this->email->subject('Activate your account');
						// $this->email->message('<p>Hi '.$data['first_name'].', welcome to <strong>Help4Hire</strong>! </p><p>Click the link below to verify your email address and activate your account. </p><p><a href="'.base_url().'user/activation/'.$activation_code.'">'.base_url().'user/activation/'.$activation_code.'</a>');	
						$this->email->message('<p>Click the link below to verify your email address and activate your account. </p><p><a href="'.base_url().'user/activation/'.$activation_code.'">'.base_url().'user/activation/'.$activation_code.'</a>');	
					    // echo $this->email->print_debugger();

					    if($this->email->send())
					    {
							// $this->session->set_flashdata('confirmation', 'Please check your email to complete the registration process and activate your account. <a href="'.base_url().'user/activation/'.$activation_code.'">Activate Now</a>');	
							$this->session->set_flashdata('confirmation', 'Check your email to complete the registration process.');
							redirect('user-');
					    }
					    else 
					    {
					    	$this->session->set_flashdata('error', 'Account was created but sending activation email failed.');
							redirect('user-signup');
						}					    
					}
					else
					{
						$this->session->set_flashdata('error', 'Error creating user account.');
						redirect('user-signup');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'An account with that email address already exists - try again or <a href="'.base_url().'user/">log in</a>');
					redirect('user-signup');
				} 
			}
		}
		else
		{
			// $data['page'] = 'pages_v2_4/content/signup';
			// $this->load->view('pages_v2_4/general', $data);	
			$this->load->view('pages_v2_4/signup');
		}

	}
/* 	
=============================================================================================
 activation() - Activates new user accounts.
---------------------------------------------------------------------------------------------
 Registration: Step 2 of 2
---------------------------------------------------------------------------------------------
 - decode activation code to get the email address
 - use the email address to activate the account and retrieve user data
 - send confirmation email
 - create a session and log user in
 - send them to the profile page with confirmation message
=============================================================================================
*/
	// public function activation($encoded_email = null, $activation_code = null)
	public function activation($activation_code = false)
	{
		// if(isset($encoded_email, $activation_code))
		// if(isset($activation_code))
		if($activation_code)
		{
			// get user email by decoding activation code
			$email_address = $this->encrypt->decode($activation_code);

			// get user data (** adjust to search only non-activated accounts **)
			$data = $this->user_model->activate_user($email_address);

			if($data)
			{
			    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
				$this->email->to($data['email_address']); 
			    $this->email->subject('Welcome to Help4Hire!');
				$this->email->message('<p>Hi '.$data['first_name'].' </p><p>Your account has been activated. </p><p>You can  here: <a href="'.base_url().'user//'.$data['email_address'].'">'.base_url().'user//'.$data['email_address'].'</a> </p>');	
			    $this->email->send();
			    // echo $this->email->print_debugger();

				$this->session->set_userdata($data);
				$this->session->set_flashdata('confirmation', 'Your account has been activated. Welcome to Help4Hire.com!');
				redirect('user-profile');
			}
			else
			{
				$this->session->set_flashdata('error', 'User not found');
				redirect('user-');
			}

		}
		$this->session->set_flashdata('error', 'There was a problem activating your account. Try the activation link again or <a href="'.base_url().'contact/feedback">contact support.</a>');
		redirect('user-');
	}
/* 	
=============================================================================================
 forgot-password() - Emails a url to a user so they can reset their password.
---------------------------------------------------------------------------------------------
 Password Reset: Step 1 of 3
---------------------------------------------------------------------------------------------
 Accepts a valid email address from post. Checks if that email address exists
 in the database. If it does it's combined with the current date and system 
 encryption key then hashed with MD5 to create the reset_code. The email 
 address and reset code are then used to create a URL that's emailed to the 
 user. That URL passes the values to the reset_password() function below and 
 are used to authenticate the reset request.
=============================================================================================
*/
	public function ctr_forgot_password()
	{
		// if post data exists, capture it
		if($email_address = $this->input->post('email_address', true))
		{
			// $email_address = $this->input->post('email_address', true);
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');

			// validation failed
			if ($this->form_validation->run() == false)
			{
				// $data['page'] = 'pages_v2_4/content/forgot-password';
				// $this->load->view('pages_v2_4/general', $data);
				$this->load->view('pages_v2_4/forgot-password');
			}
			// validation succeeded
			else
			{
				// $this->load->model('user_model');
				// $data = $this->user_model->get_user_by_email($email_address);
				// if($this->user_model->email_address_exist($email_address)) 
				if($data = $this->user_model->get_user_by_email($email_address)) 
				{
					// email code generated using a combination of the users email, today's date, and the system encryption key
					$reset_code = md5($this->config->item('encryption_key').date('Y-m-d').$email_address);
					$encrypted_email_address = $this->encrypt->encode($data['email_address']);

					// set flash data to display a confirmation (temporarily display reset link in confirmation)
					// $this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$reset_code.'">'.base_url().'user/reset_password/'.$reset_code.'</a>');	
					$this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.'">'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.'</a>');	
					// $this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address.'. <br><a href="'.base_url().'user/reset_password/'.$email_address.'/'.$reset_code.'">Reset Password</a>');	
					// $this->session->set_flashdata('confirmation', 'Password reset instructions have been sent to '.$email_address);	

					// email the user a URL to the code
				    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
					$this->email->to($email_address); 
				    $this->email->subject('Reset your password');
					// $this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password at <strong>Help4Hire.com</a>. </p><p><a href="'.base_url().'user/reset_password/'.$email_address.'/'.$reset_code.'">Tap here</a> to reset your password. </p><p>For assistance contact <a href="mailto:noreply@help4hire.com">noreply@help4hire.com</a>. </p><p>Help4Hire Support</p>');	
					// $this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password. </p><p>Click the link below to reset your password now. </p><p>'.base_url().'user/reset_password/'.$reset_code.' </p>');	
					$this->email->message('<p>Hi '.$data['first_name'].' </p><p>We received a request to reset your password. </p><p>Click the link below to reset your password now. </p><p>'.base_url().'user/reset_password/'.$encrypted_email_address.'/'.$reset_code.' </p>');	
				    $this->email->send();
				    // echo $this->email->print_debugger();
				}
				else
				{
					$this->session->set_flashdata('error', 'That email address doesn\'t exist in our database. &nbsp;<a href="'.base_url().'user/join">Create an account.</a>');	
				}

				$this->session->keep_flashdata('email_address');
				redirect('user/');

			}
		}
		else
		{
			// $data['page'] = 'pages_v2_4/content/forgot-password';
			$this->load->view('pages_v2_4/forgot-password');
		}
	}
/* 	
=============================================================================================
 reset_password() - Display a form that enables a user to reset their password
---------------------------------------------------------------------------------------------
 Password Reset: Step 2 of 3
---------------------------------------------------------------------------------------------
 Requires email_address and reset_code. If the reset_code is valid it combines
 the email_address and reset_code then hashes them using MD5 to create the 
 reset_hash. All three values are then passed to the reset password view. The 
 reset_code and reset_hash are loaded in the view as hidden form fields and 
 will be used to validate the request to change password when the form is 
 submited to the change_password() function below. 
=============================================================================================
*/
	public function reset_password($encrypted_email_address = null, $reset_code = null)
	{
		if(isset($encrypted_email_address, $reset_code))
		{
			// decode email address
			$email_address = $this->encrypt->decode($encrypted_email_address);

			// verify email code - if varified display password reset from view
			if($reset_code == md5($this->config->item('encryption_key').date('Y-m-d').$email_address))
			{
				// create reset_hash from email address and reset_code to pass to the password reset form view
				$reset_hash = md5($email_address.$reset_code);

				$data['email_address']	= $email_address;
				$data['reset_code']	= $reset_code;
				$data['reset_hash']	= $reset_hash;

				// $data['page_info']['path'] 	= "sections/user/reset_password";
				// $this->load->view('template/master', $data);

				// $data['page'] = 'pages_v2_4/content/reset_password';
				$this->load->view('pages_v2_4/reset_password', $data);
			}
			else
			{
				$this->session->set_flashdata('error', 'Password reset has expired, try again.');
				redirect('user-forgot-password');
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Email address or reset code was missing, try again.');
			redirect('user-forgot-password');
		}
	}
/* 	
=============================================================================================
 change_password() - Display a form that enables a user to reset their password
---------------------------------------------------------------------------------------------
 Password Reset: Step 3 of 3
---------------------------------------------------------------------------------------------
 Requires email_address and reset_code. If the reset_code is valid it combines
 the email_address and reset_code then hashes them using MD5 to create the 
 reset_hash. All three values are then passed to the reset password view. The 
 reset_code and reset_hash are loaded into the form as hidden fields and will 
 be used to validate the reset password request when the form is submited.
=============================================================================================
*/
	public function ctr_change_password()
	{
		if($this->input->post('email_address', true) && $this->input->post('reset_code', true) && $this->input->post('reset_hash', true))
		{
			// form validation rules
			$this->form_validation->set_rules('email_address', 'email address', 'trim|min_length[6]|max_length[100]|valid_email|required');
			$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
			$this->form_validation->set_rules('password_confirmation', 'confirm password', 'trim|matches[password]|required');
			$this->form_validation->set_rules('reset_code', 'reset code', 'trim|exact_length[32]|required');
			$this->form_validation->set_rules('reset_hash', 'reset hash', 'trim|exact_length[32]|required');

			// validation
			if ($this->form_validation->run() == false)
			{
				// $data['page'] = 'pages_v2_4/content/reset_password';
				$this->load->view('pages_v2_4/reset_password');
			}
			else
			{
				$data = $this->input->post(null, true);

				// verify that reset code is correct
				if($data['reset_code'] == md5($this->config->item('encryption_key').date('Y-m-d').$data['email_address']))
				{
					// Verify that reset hash is correct
					if($data['reset_hash'] = md5($data['email_address'].$data['reset_code']))
					{
						// reset password
						// echo "Change password.";

						$result = $this->user_model->update_password($data['email_address'], $data['password']);

						// if successful, load user data into session and goto user profile
						if($result)
						{
						    $this->email->from(NOREPLY_EMAIL, NOREPLY_NAME);
							$this->email->to($result['email_address']); 
						    $this->email->subject('Your password has been changed');
							$this->email->message('<p>Hi '.$result['first_name'].' </p><p>Your password at <strong>Help4Hire.com</strong> has been changed. </p><p>
							          <a href="'.base_url().'user-/'.$result['email_address'].'">
							          Tab here</a> to . </p>
							          <p>For assistance contact <a href="mailto:noreply@help4hire.com">noreply@help4hire.com</a>. </p><p>Help4Hire Support</p>');	
						    $this->email->send();
						    // echo $this->email->print_debugger();

							$this->session->set_userdata($result);
							$this->session->set_flashdata('confirmation', 'Your password was updated.');
							redirect('user/profile');
						}
						// if failed, goto  with error
						else
						{
							$this->session->set_flashdata('error', 'There was a problem updating your password, try again. If the issue persists <a href="'.base_url().'contact/feedback">contact support</a>');
							redirect('user/reset_password/'.$data['email_address'].'/'.$data['reset_code']);
						} 

						// hash the users new password
						// update the users password in the database
						// update users last modified date in the database
						// log the user in to the website
						// send them to their profile page
						// confirm that their password was successfully changed
						// send a confirmation email
					}
					else 
					{
						$this->session->set_flashdata('error', 'Reset hash was invalid.');
						redirect('user-forgot-password');
					}
				}
				else 
				{
					$this->session->set_flashdata('error', 'Reset code was invalid.');
					redirect('user-forgot-password');
				}
			}	
		}
		// if no post data, display the  view
		else
		{
			$this->session->set_flashdata('error', 'Email address, reset code or reset hash was missing, try again.');
			redirect('user-forgot-password');
		}
	}
/* 	
=============================================================================================
 logout() - Logs the user out, destroys their session and returns them to the 
  page with a notice telling them they've been logged out.
=============================================================================================
*/
	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->sess_create();
		$this->session->set_flashdata('confirmation', 'You\'ve been logged out.');
		redirect('user-signin');
	}


/*---------------------------------------------------------
 * 	
 * 		FUNCTION:	uploadthis()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 *		This is the controller for uploading documents
 *		to the EMO Safe Storage area.
 * 
 *--------------------------------------------------------- 
 */
	
public function uploadthis()
{
$config		= array();
//$fullpath 	= 'C:/inetpub/hrm_uploads/EMO/';
//$fullpath 	= realpath('C:/inetpub/hrm_uploads/');
$imgPATH = realpath('C:/inetpub/wwwroot/hrm_uploads/EMO/');
$imgPATH = 'C:/inetpub/wwwroot/hrm_uploads/EMO/';
$imgPATH = 'C:\inetpub\hrm_uploads\EMO';
$imgPATH = 'C:\EMOuploads';

		$fullPATH = realpath('C:/inetpub/wwwroot/hrm_uploads/EMO/');

         $config['upload_path']   = $imgPATH; 
         $config['allowed_types'] = 'pdf'; 
         $config['max_size']      = 300; 
         $config['max_width']     = 1024; 
         $config['max_height']    = 768;  
         
         
		$this->yprintARR($config, "CONFIG SETTING  ", 0);    // DBG
         
         
         $this->load->library('upload', $config);
			
 if ( ! $this->upload->do_upload('userfile')) 
 			{
            $error = array('error' => $this->upload->display_errors()); 
//            $this->load->view('upload_form', $error); 
			print "NO WORKIE " ;
			$this->yprintARR($error, "Upload Errors ", 0);    	// DBG
 			}
			
         else
         	{ 
            $data = array('upload_data' => $this->upload->data()); 
 //           $this->load->view('upload_success', $data); 
			print "IT WORKS!!!";
         	} 
  	
	
	
	
}	
	
	
/*---------------------------------------------------------
 * 		FUNCTION:	yprintARR()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This is used for array debugging and can be 
 * 		easily adapted to remote testing as well.
 * 		Data will be dumped to a external log file.
 * 
 *--------------------------------------------------------- 
 */

function yprintARR($vdata = NULL, $vmsg = NULL, $xcond = NULL)
{
//		print "<pre>--------------->>>> SO [" . $vmsg . "]  Data DMP)";
//		if(!empty($vdata))
//			print_r($vdata);
//			print "HERE";
//		else 
//			print "NO CELLS IN ARRAY<br><br>";	
//		print "</pre>--------------<<<< EO [" . $vmsg . "]   Data DMP Ends)<br><br>";
}

/*---------------------------------------------------------
 * 		FUNCTION:	yprint()
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	LA
 * 
 * 		This is used for one line debugging and can be 
 * 		easily adapted to remote testing as well.
 * 
 *--------------------------------------------------------- 
 */

function yprint($msg = NULL, $cdloc = NULL, $xcond = NULL)
{
//	print '<font color="FFFFFF">';
//	PRINT "<br> MSG [" . $msg . "][" . $cdloc . "]";
//	PRINT " MSG [" . $msg . "]<br>" . "\r\n";
	
//	print "</font>";
	
}	
	

}