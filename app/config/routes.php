<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

// $route['deliverable/(:num)'] 			= 'deliverables/deliverable/$1';
// $route['(:any)/(:any)/(:any)'] 			= 'deliverables/index/$1/$2/$3';
// $route['(:any)/(:any)'] 					= 'deliverables/index/$1/$2';
// $route['(:any)'] 						= 'deliverables/index';

// Mapping Routes - PRSC

$route['user-profile'] 					= 'user_v2_4G/ctr_profile_v2_4';
$route['user-signin'] 					= 'user_v2_4G/ctr_new_login_v2_4';
//$route['user-signin'] 				= 'user_v2_4G/index';
$route['user-signup'] 					= 'user_v2_4G/ctr_signup';
$route['user-forgot-password']			= 'user_v2_4G/ctr_forgot_password_v2_4';
$route['user-logout'] 					= 'user_v2_4G/ctr_logout';

$route['new-login'] 					= 'user_v2_4G/ctr_new_login_v2_4';
$route['user-join'] 					= 'user_v2_4G/ctr_join_v2_4';

$route['user-redo-password/(:any)/(:any)'] 	= 'user_v2_4G/ctr_redo_password_v2_4/$1/$2';
$route['user-change-passwd'] 			= 'user_v2_4G/ctr_submit_new_password_v2_4';

// Functional Screens - Categories	
// 2017 May 30 PRSC
$route['show-categories'] 				= 'user_v2_4G/ctr_show_categories_v2_4';
$route['add-category'] 					= 'user_v2_4G/ctr_add_category_v2_4';
$route['display-category/(:any)'] 		= 'user_v2_4G/ctr_display_category_full_v2_4/$1';
$route['update-category'] 				= 'user_v2_4G/ctr_update_category_v2_4';
$route['change-category/(:any)'] 		= 'user_v2_4G/ctr_change_category_v2_4/$1';
$route['delete-category/(:any)'] 		= 'user_v2_4G/ctr_display_category_v2_4/$1';
$route['create-category'] 				= 'user_v2_4G/ctr_create_category_v2_4';
$route['delthis-category/(:any)'] 		= 'user_v2_4G/ctr_deletethis_category_v2_4/$1';
$route['deleteit-category'] 			= 'user_v2_4G/ctr_deleteit_category_v2_4';
$route['display-category-full/(:any)'] 	= 'user_v2_4G/ctr_display_category_full_v2_4/$1';
$route['disable-category/(:any)'] 		= 'user_v2_4G/ctr_disable_category_v2_4/$1';
$route['enable-category/(:any)'] 		= 'user_v2_4G/ctr_enable_category_v2_4/$1';
$route['flipit-category'] 				= 'user_v2_4G/ctr_flipit_category_v2_4';



// Functional Screens - Users	
// 2017 May 30 PRSC

$route['show-users'] 				= 'user_v2_4G/ctr_show_users_v2_4';
$route['add-user'] 					= 'user_v2_4G/ctr_add_user_v2_4';
//$route['display-user/(:any)'] 		= 'user_v2_4G/ctr_display_user_v2_4G/$1';
$route['update-user'] 				= 'user_v2_4G/ctr_update_user_v2_4';
$route['change-user/(:any)'] 		= 'user_v2_4G/ctr_change_user_v2_4/$1';
$route['delete-user/(:any)'] 		= 'user_v2_4G/ctr_display_user_v2_4/$1';
$route['create-user'] 				= 'user_v2_4G/ctr_create_user_v2_4';
$route['delthis-user/(:any)'] 		= 'user_v2_4G/ctr_deletethis_user_v2_4/$1';
$route['deleteit-user'] 			= 'user_v2_4G/ctr_deleteit_user_v2_4';

$route['display-user/(:any)'] 		= 'user_v2_4G/ctr_display_user_and_groups_v2_4/$1';

$route['disable-user/(:any)'] 		= 'user_v2_4G/ctr_disable_user_v2_4/$1';
$route['enable-user/(:any)'] 		= 'user_v2_4G/ctr_enable_user_v2_4/$1';
$route['flipit-user'] 				= 'user_v2_4G/ctr_flipit_user_v2_4';


// Functional Screens - Documents	
// 2017 May 30 PRSC

$route['show-documents'] 				= 'user_v2_4G/ctr_show_documents_v2_4';
$route['add-document'] 					= 'user_v2_4G/ctr_add_document_v2_4';
$route['add-doc-to-cat/(:any)'] 		= 'user_v2_4G/ctr_add_doc_to_cat_v2_4/$1';
$route['display-document/(:any)'] 		= 'user_v2_4G/ctr_display_document_UL_v2_4/$1';
$route['update-document'] 				= 'user_v2_4G/ctr_update_document_v2_4';
$route['change-document/(:any)'] 		= 'user_v2_4G/ctr_change_document_v2_4/$1';
$route['delete-document/(:any)'] 		= 'user_v2_4G/ctr_display_document_v2_4/$1';
$route['create-document'] 				= 'user_v2_4G/ctr_create_document_v2_4';
$route['delthis-document/(:any)'] 		= 'user_v2_4G/ctr_deletethis_document_v2_4/$1';
$route['deleteit-document'] 			= 'user_v2_4G/ctr_deleteit_document_v2_4';
$route['add-document-to-cat/(:any)'] 	= 'user_v2_4G/ctr_add_document_to_cat_v2_4/$1';

$route['disable-document/(:any)'] 		= 'user_v2_4G/ctr_disable_document_v2_4/$1';
$route['enable-document/(:any)'] 		= 'user_v2_4G/ctr_enable_document_v2_4/$1';
$route['flipit-document'] 				= 'user_v2_4G/ctr_flipit_document_v2_4';


// Functional Screens - Accesses	
// 2017 May 30 PRSC


$route['show-accesses'] 				= 'user_v2_4G/ctr_show_accesses_v2_4';
$route['add-access'] 					= 'user_v2_4G/ctr_add_access_v2_4';
$route['add-access-to-cat/(:any)'] 		= 'user_v2_4G/ctr_add_access_to_cat_v2_4/$1';
$route['add-access-to-user/(:any)'] 	= 'user_v2_4G/ctr_add_access_to_user_v2_4/$1';
$route['create-access-for-user'] 		= 'user_v2_4G/ctr_create_access_for_user_v2_4/$1';

$route['display-access/(:any)'] 		= 'user_v2_4G/ctr_display_access_v2_4/$1';
$route['update-access'] 				= 'user_v2_4G/ctr_update_access_v2_4';
$route['change-access/(:any)'] 			= 'user_v2_4G/ctr_change_access_v2_4/$1';
$route['delete-access/(:any)'] 			= 'user_v2_4G/ctr_display_access_v2_4/$1';
$route['create-access'] 				= 'user_v2_4G/ctr_create_access_v2_4';
$route['delthis-access/(:any)'] 		= 'user_v2_4G/ctr_deletethis_access_v2_4/$1';
$route['deleteit-access'] 				= 'user_v2_4G/ctr_deleteit_access_v2_4';
$route['disable-access/(:any)'] 		= 'user_v2_4G/ctr_disable_access_v2_4/$1';
$route['enable-access/(:any)'] 			= 'user_v2_4G/ctr_enable_access_v2_4/$1';
$route['flipit-access'] 				= 'user_v2_4G/ctr_flipit_access_v2_4';

// Functional Screens - Sub Categoriess	
// 2017 May 30 PRSC


$route['show-subcats'] 					= 'user_v2_4G/ctr_show_subcats_v2_4';
$route['add-subcat'] 					= 'user_v2_4G/ctr_add_subcat_v2_4';
$route['add-subcat-to-cat/(:any)'] 		= 'user_v2_4G/ctr_add_subcat_to_cat_v2_4/$1';
$route['display-subcat/(:any)'] 		= 'user_v2_4G/ctr_display_subcat_full_v2_4/$1';
$route['update-subcat'] 				= 'user_v2_4G/ctr_update_subcat_v2_4';
$route['change-subcat/(:any)'] 			= 'user_v2_4G/ctr_change_subcat_v2_4/$1';
$route['delete-subcat/(:any)'] 			= 'user_v2_4G/ctr_display_subcat_v2_4/$1';
$route['create-subcat'] 				= 'user_v2_4G/ctr_create_subcat_v2_4';
$route['delthis-subcat/(:any)'] 		= 'user_v2_4G/ctr_deletethis_subcat_v2_4/$1';
$route['deleteit-subcat'] 				= 'user_v2_4G/ctr_deleteit_subcat_v2_4';
$route['disable-subcat/(:any)']			= 'user_v2_4G/ctr_disable_subcat_v2_4/$1';
$route['enable-subcat/(:any)'] 			= 'user_v2_4G/ctr_enable_subcat_v2_4/$1';
$route['flipit-subcat'] 				= 'user_v2_4G/ctr_flipit_subcat_v2_4';

$route['show-fulldoc']					= 'user_v2_4G/ctr_show_fulldoc_v2_4';


$route['show-news']					= 'user_v2_4G/ctr_show_news_v2_4';

$route['default_controller'] 		= 'user_v2_4G/index';

/* End of file routes.php */
/* Location: ./application/config/routes.php */