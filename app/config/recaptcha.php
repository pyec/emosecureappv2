<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
// Recaptcha class config
// ------------------------------------------------------------------------

// The reCaptcha server keys and API locations
// Obtain your own keys from: http://www.recaptcha.net
// Available Styles/Themes:  'red' (default),'white','blackglass','clean' (plain)

$config['recaptcha'] = array(
	'public'						=> '6LdRkNoSAAAAAP5_mp8t_8KpT7MnSIiB0NHh6GI0',
	'private'						=> '6LdRkNoSAAAAAGWTJ5mU0wro999X8Ai_lnBOoLfq',
	'RECAPTCHA_API_SERVER' 			=> 'http://www.google.com/recaptcha/api',
	'RECAPTCHA_API_SECURE_SERVER'	=> 'https://www.google.com/recaptcha/api',
	'RECAPTCHA_VERIFY_SERVER' 		=> 'www.google.com',
	'RECAPTCHA_SIGNUP_URL' 			=> 'https://www.google.com/recaptcha/admin/create',
	'theme' 						=> 'clean'
	);