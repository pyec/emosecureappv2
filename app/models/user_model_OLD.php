<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 	
=============================================================================================
 The users model handles database interaction related to user accounts. It 
 supports the login controller by checking if a user exist by validating
 their crendentals. It also enables the creation of new accounts and updates
 to user profiles.
=============================================================================================
*/
class User_model extends CI_Model 
{
	// global variables
	public $user_table_name = null;
	public $user_primary_key = null;

	public function __construct()
	{
		parent::__construct();
		$this->user_table_name = 'users';
		$this->user_primary_key = 'user_id';
	}
/* 	
=============================================================================================
 authenticate($data) - Check if credentials match a record in the db.
---------------------------------------------------------------------------------------------
 Accepts an array with keys: email_address, password
 Returns user details if a match is found, otherwise returns false.
=============================================================================================
*/
	public function authenticate($data)
	{
		$data['password'] = $data['password'];
		// $data['password'] = md5($this->config->item('encryption_key').strtolower($data['email_address']).$data['password']);
		$data['email_address'] = strtolower($data['email_address']);

		$query = $this->db->get_where('users_view', $data);
		return $query->row_array();
	}
/* 	
=============================================================================================
 add_user($data) - Add user if their email address doesn't already exist.
---------------------------------------------------------------------------------------------
 Accepts an array with keys: first_name, last_name, email_address, password
 Password is encrypted using encryption key, email address, and password
 Returns the new users id on success, otherwise returns false.
=============================================================================================
*/
	public function add_user($data)
	{
		if(is_array($data))
		{
			$data = array(
			   'first_name' => ucfirst(strtolower($data['first_name'])),
			   'last_name' => ucfirst(strtolower($data['last_name'])),
			   'email_address' => strtolower($data['email_address']),
			   'password' => md5($this->config->item('encryption_key').strtolower($data['email_address']).$data['password']),
			   'date_created' => date('Y-m-d H:i:s')
			);
			if($this->db->insert('users', $data))	return $this->db->insert_id();
			else return false;
		}
		return false;
	}
/* 
=============================================================================================
 activate_user($data) - Activates a user account.
=============================================================================================
*/
	public function activate_user($email_address)
	{
		$data['activated_flag'] = 1;
		$this->db->where('email_address', $email_address);
		if($this->db->update('users', $data))	return $this->get_user_by_email($email_address);
		return false;
	}
/* 	
=============================================================================================
 update_password() - Updates the users password.
=============================================================================================
*/
	public function update_password($email_address, $password)
	{
		$data = array(
           'password' => md5($this->config->item('encryption_key').strtolower($email_address).$password)
        );

		$this->db->where('email_address', $email_address);
		// $query = $this->db->update('users', $data);

		if($this->db->update('users', $data))	return $this->get_user_by_email($email_address);

		return false;
	}
/* 	
=============================================================================================
 email_address_exist($email_address) - Check if email address exists in db.
---------------------------------------------------------------------------------------------
 Accepts a single email address.
---------------------------------------------------------------------------------------------
 Returns true if a match is found, otherwise returns false.
=============================================================================================
*/
	public function email_address_exist($email_address)
	{
		$query = $this->db->get_where('users_view', array('email_address' => $email_address));
		if($query->row_array()) return true;
		else return false;
	}
/* 	
=============================================================================================
 get_user_by_id() - Returns data for a particular user based on their ID.
=============================================================================================
*/
	public function get_user_by_id($id)
	{
		$data['id'] = $id;
		$result = $this->db->get_where('users_view', $data);
		
		if($result->row_array()) return $result->row_array();
		else return false;
	}
/* 	
=============================================================================================
 get_user_by_email() - Returns data for a particular user based on their ID.
=============================================================================================
*/
	public function get_user_by_email($email_address)
	{
		$data['email_address'] = $email_address;
		$result = $this->db->get_where('users_view', $data);
		
		if($result->row_array()) return $result->row_array();
		else return false;
	}




	/*-----------------------------------------------------------------------------------
		This will return an array of users wth offset and limit
	------------------------------------------------------------------------------------*/
	// public function get_users( $offset = 0, $limit = 0 )
	// {
	// 	$query = $this->db->get($this->user_table_name, $limit, $offset);
	// 	$rows = $query->num_rows();

	// 	if ($rows > 0)
	// 	{
	// 		if ($rows > 1)
	// 		{
	// 			return $query->row_array();
	// 		}
	// 		else
	// 		{
	// 			return $query->results_array();
	// 		}
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }


	/*-----------------------------------------------------------------------------------
		This will return a user record by id
	------------------------------------------------------------------------------------*/
	// public function get_user_by_id( $user_id = null )
	// {
	// 	if ($user_id != null)
	// 	{
	// 		$this->db->where($this->user_primary_key, $user_id);
	// 		$query = $this->db->get($this->user_table_name);
	// 		$rows = $query->num_rows();

	// 		if ($rows > 0)
	// 		{
	// 			return $query->row_array();
	// 		}
	// 		else
	// 		{
	// 			return false;
	// 		}
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }

	/*-----------------------------------------------------------------------------------
		This will add a record to the users table
	------------------------------------------------------------------------------------*/
	// public function add_user( $user_data = null )
	// {
	// 	if ($user_data != null)
	// 	{
	// 		$results = $this->db->insert($this->user_table_name, $user_data);

	// 		if ($results)
	// 		{
	// 			return true;
	// 		}
	// 		else
	// 		{
	// 			return false;
	// 		}	
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }

	/*-----------------------------------------------------------------------------------
		This will update a record from the users table by id
	------------------------------------------------------------------------------------*/
	// public function update_user( $user_id = null, $user_data = null )
	// {
	// 	if ($user_id != null || $user_data != null)
	// 	{
	// 		$this->db->where($this->user_primary_key, $user_id);
	// 		$results = $this->db->update($this->user_table_name, $user_data);
	// 		return $results;
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }

	/*-----------------------------------------------------------------------------------
		This will delete a record from the users table by id
	------------------------------------------------------------------------------------*/
	// public function delete_user( $user_id = null )
	// {
	// 	if ($user_id != null)
	// 	{
	// 		$this->db->where($this->user_primary_key, $user_id);
	// 		$results = $this->db->delete($this->user_table_name);

	// 		return $results;
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }

}

/* End of file user_model.php */
/* Location: ./app/models/user_model.php */