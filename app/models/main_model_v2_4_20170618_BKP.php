<?php

/*----------------------------------------------------------------------------
 * 
 * 		MODULE: 	main_model_v2_4
 * 		AUTHOR:		Stephen Chafe (Zen River Software)
 * 		CREATED:	2017_05_30
 * 
 * 		This is the Data MODEL functions for the EMOSecureDoc App.
 * 
 * ---------------------------------------------------------------------------
 * 
 *  MODIFICATION HISTORY
 *  2017-05-30		PRSC	Cleaned up for routes use and easier edit controls.
 * 
 *----------------------------------------------------------------------------
 */



class Main_model_v2_4 extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//$this->load->database();	// auto-loaded
	}


	public function is_user($email=FALSE, $password=FALSE)
	{

		if(!empty($email) && !empty($password)) {

			$values = array(
				'UserEmail' 	=> trim(strtolower($email)),
				'UserPassword'	=> trim(strtolower($password))
			);

			$query = $this->db->get_where('UsersView', $values); 
			return $query->row_array();
		}
	}




//-----------------------------------------------[SO Reference Calls ]>>>

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_UserTypes(Groups) PRSC
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_user_types()
	{
		$this->db->order_by("UserTypesID", "ASC");
		$query = $this->db->get('UserTypes');
		return $query->result_array();
	}
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_ActiveStates
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_active_states()
	{
		$this->db->order_by("ActiveStatesID", "ASC");
		$query = $this->db->get('ActiveStates');
		return $query->result_array();
	}
	
//<<<<<<-----------------------------------------[EO Reference Calls ]>>>
	
	
//-----------------------------------------------[SO Category Block Model ]>>>
		
/*===========================================================================
 * 
 *  BLOCK:       	md_categories
 *  AUTHOR:         R.Stephen Chafe (Zen River Software)
 *  CREATED:        2017_05_29			
 * 
 * ------------------------------------------------------------------------
 *
 *      This contains the block of model functions for accessing the 
 *      Categories Table.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160228    PRSC    Created from standard Zen shell  
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_category_by_id($id = NULL)
	{
		$this->db->where('CID', $id);
		$query = $this->db->get('Categories');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_categories
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_categories($id = null)
	{
		if(!empty($id))	$this->db->where('CID', $id);
		$this->db->order_by("CID", "ASC");
		$query = $this->db->get('Categories');
		return $query->result_array();
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_update_categories
 *--------------------------------------------------------------------------
 */  
	
	public function md_update_category($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
//			'CategoryCode'				=> $data['CategoryCode'],
			'Name'						=> $data['Name'],
			'CategoryDesc'				=> $data['CategoryDesc'],
//			'AdminID'					=> $data['AdminID'],
            'LastModBy'					=> $data['LastModBy'],
			'LastModDate' 				=> $DATETIME
		);

		$this->db->where('CID', $data['CID']);
		$result = $this->db->update('Categories',$values);

		if($result)	 	return true;
		else 			return false;
	}
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_create_category
 *--------------------------------------------------------------------------
 */  
	
	public function md_create_category($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
//		print "<pre>--------------->>>> SO )";
//		print_r($data);
//		print "</pre>--------------<<<< EO <br>";
//				
		$values = array(
//			'CategoryCode'				=> $data['CategoryCode'],
			'Name'						=> $data['Name'],
			'CategoryDesc'				=> $data['CategoryDesc'],
//			'AdminID'					=> $data['AdminID'],
//          'LastModBy'					=> $data['LastModBy'],
//			'LastModDate' 				=> $DATETIME
		    'CreatedBy'					=> $data['CreatedBy'],
			'CreateDDate' 				=> $DATETIME
		);

		$result = $this->db->insert('Categories', $values);
//        print "RESULT [ " .  $result . "]<br>"; 
		$result = $this->db->insert_id();
//        print "RESULT [ " .  $result . "]<br>"; 
        
        return $result;
	
	}

	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_delete_category
 *--------------------------------------------------------------------------
 */  
	
	public function md_delete_category($id = NULL)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;

		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->db->where("CID", $id);
			$results = $this->db->delete('Categories');
			return $results;
		}
	}											// EO Delete Function
		
		
//<<<<<<------------------------------------[EO Category Block Model ]	
	
	
//-----------------------------------------------[SO User Block Model ]>>>
		
/*===========================================================================
 * 
 *  BLOCK:       	md_users
 *  AUTHOR:         R.Stephen Chafe (Zen River Software)
 *  CREATED:        2017_05_29			
 * 
 * ------------------------------------------------------------------------
 *
 *      This contains the block of model functions for accessing the 
 *      Users Table.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160228    PRSC    Created from standard Zen shell  
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_user_by_id($id = NULL)
	{
		$this->db->where('UID', $id);
		$query = $this->db->get('Users');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
		}

	public function md_get_user_by_email($i_email = NULL)
	{
		$this->db->where('Email', $i_email);
		$query = $this->db->get('Users');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}
	
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_users
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_users($id = null)
	{
		if(!empty($id))	$this->db->where('UID', $id);
		$this->db->order_by("UID", "ASC");
		$query = $this->db->get('Users');
		return $query->result_array();
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_update_users
 *--------------------------------------------------------------------------
 */  
	
	public function md_update_user($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
//			'UserCode'				=> $data['UserCode'],
			'FirstName'				=> $data['FirstName'],
			'LastName'				=> $data['LastName'],
			'UserGroup'				=> $data['UserTypesID'],
			'Active'				=> $data['ActiveStatesID'],
			'Email'					=> $data['Email'],
			'Password'				=> $data['Password'],
			'UserDesc'				=> $data['UserDesc'],
//			'AdminID'				=> $data['AdminID'],
            'LastModBy'				=> $data['LastModBy'],
			'LastModDate' 			=> $DATETIME
		);

		$this->db->where('UID', $data['UID']);
		$result = $this->db->update('Users',$values);

		if($result)	 	return true;
		else 			return false;
	}
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_create_user
 *--------------------------------------------------------------------------
 */  
	
	public function md_create_user($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
		print "<pre>--------------->>>> SO )";
		print_r($data);
		print "</pre>--------------<<<< EO <br>";
//				
		$values = array(
//			'UserCode'				=> $data['UserCode'],
			'FirstName'				=> $data['FirstName'],
			'LastName'				=> $data['LastName'],
			'UserGroup'				=> $data['UserTypesID'],
			'Active'				=> $data['ActiveStateID'],
			'Email'					=> $data['Email'],
			'Password'				=> $data['Password'],
			'UserDesc'					=> $data['UserDesc'],
//			'AdminID'				=> $data['AdminID'],
//          'LastModBy'					=> $data['LastModBy'],
//			'LastModDate' 				=> $DATETIME
		    'CreatedBy'					=> $data['CreatedBy'],
			'CreateDDate' 				=> $DATETIME
		);

		$result = $this->db->insert('Users', $values);
//        print "RESULT [ " .  $result . "]<br>"; 
		$result = $this->db->insert_id();
//        print "RESULT [ " .  $result . "]<br>"; 
        
        return $result;
	
	}

	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_delete_user
 *--------------------------------------------------------------------------
 */  
	
	public function md_delete_user($id = NULL)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;

		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->db->where("UID", $id);
			$results = $this->db->delete('Users');
			return $results;
		}
	}											// EO Delete Function
		

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_access_users_in_category
 *  
 *  	This should be done as a view to provide constraints.
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_access_users_in_category($id = null)
	{
//		if(!empty($id))	$this->db->where('UID', $id);

		$this->db->where('CID', $id);
		$this->db->order_by("AID", "ASC");
		$query = $this->db->get('CategoryAccess');
		return $query->result_array();
	}
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_users_in_category
 *  
 *  	This should be done as a view to provide constraints.
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_access_categories_user_is_in($id = null)
	{
//		if(!empty($id))	$this->db->where('UID', $id);

		$this->db->where('UID', $id);
		$this->db->order_by("AID", "ASC");
		$query = $this->db->get('CategoryAccess');
		return $query->result_array();
	}
	
	
//<<<<<<------------------------------------[EO User Block Model ]	
	
	
//-----------------------------------------------[SO Access Block Model ]>>>
		
/*===========================================================================
 * 
 *  BLOCK:       	md_accesses
 *  AUTHOR:         R.Stephen Chafe (Zen River Software)
 *  CREATED:        2017_05_29			
 * 
 * ------------------------------------------------------------------------
 *
 *      This contains the block of model functions for accessing the 
 *      CategoryAccess Table.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160228    PRSC    Created from standard Zen shell  
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_access_by_id($id = NULL)
	{
		$this->db->where('AID', $id);
		$query = $this->db->get('CategoryAccess');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_accesses
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_accesses($id = null)
	{
		if(!empty($id))	$this->db->where('AID', $id);
//		$this->db->order_by("AID", "ASC");
		$this->db->order_by("CID", "asc");
		$query = $this->db->get('CategoryAccess');
		return $query->result_array();
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_update_accesses
 *--------------------------------------------------------------------------
 */  
	
	public function md_update_access($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
//			'AccessCode'				=> $data['AccessCode'],
			'CID'					=> $data['CID'],
			'UID'					=> $data['UID'],
			'Active'				=> $data['ActiveStateID'],
			'AccessDesc'			=> $data['AccessDesc'],
			'LastModBy'				=> $data['LastModBy'],
			'LastModDate' 			=> $DATETIME
		);

		$this->db->where('AID', $data['AID']);
		$result = $this->db->update('CategoryAccess',$values);

		if($result)	 	return true;
		else 			return false;
	}
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_create_access
 *--------------------------------------------------------------------------
 */  
	
	public function md_create_access($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
//		print "<pre>--------------->>>> SO )";
//		print_r($data);
//		print "</pre>--------------<<<< EO <br>";
//				
		$values = array(
//			'AccessCode'				=> $data['AccessCode'],
			'CID'					=> $data['CID'],
			'UID'					=> $data['UID'],
			'Active'				=> $data['ActiveStateID'],
			'AccessDesc'			=> $data['AccessDesc'],
//			'AdminID'				=> $data['AdminID'],
//          'LastModBy'					=> $data['LastModBy'],
//			'LastModDate' 				=> $DATETIME
		    'CreatedBy'					=> $data['CreatedBy'],
			'CreateDDate' 				=> $DATETIME
		);

		$result = $this->db->insert('CategoryAccess', $values);
//        print "RESULT [ " .  $result . "]<br>"; 
		$result = $this->db->insert_id();
//        print "RESULT [ " .  $result . "]<br>"; 
        
        return $result;
	
	}

	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_delete_access
 *--------------------------------------------------------------------------
 */  
	
	public function md_delete_access($id = NULL)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;

		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->db->where("AID", $id);
			$results = $this->db->delete('CategoryAccess');
			return $results;
		}
	}											// EO Delete Function
		
		
//<<<<<<------------------------------------[EO Access Block Model ]	

		
//-----------------------------------------------[SO Doc Block Model ]>>>
		
/*===========================================================================
 * 
 *  BLOCK:       	md_documents
 *  AUTHOR:         R.Stephen Chafe (Zen River Software)
 *  CREATED:        2017_05_29			
 * 
 * ------------------------------------------------------------------------
 *
 *      This contains the block of model functions for documenting the 
 *      EMODocs Table.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160228    PRSC    Created from standard Zen shell  
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_document_by_id($id = NULL)
	{
		$this->db->where('DocID', $id);
		$query = $this->db->get('EMODocs');   // was originally a view
//		return $query->result_array();
		if($query->row_array())	
				return $query->row_array();	
				
	}

	public function md_get_documents_by_category($id = NULL)
	{
		$this->db->where('CID', $id);
		$query = $this->db->get('EMODocs');   // was originally a view
		return $query->result_array();
	}
	
	
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_documents
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_documents($id = null)
	{
		if(!empty($id))	$this->db->where('DocID', $id);
		$this->db->order_by("DocID", "ASC");
		$query = $this->db->get('EMODocs');
		return $query->result_array();
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_update_documents
 *--------------------------------------------------------------------------
 */  
	
	public function md_update_document($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		if(!empty($data['SCID']))
			$SCID		= $data['SCID'];
		else 	
			$SCID		= 0;
			
		$values = array(
//			'DocID'					=> $data['DocID'],
			'CID'					=> $data['CID'],
			'SCID'					=> $data['SCID'],
//			'UID'					=> $data['UID'],
			'DocDesc'				=> $data['DocDesc'],
			'DocShortName'			=>  $data['DocShortName'],			
			'Version'				=>  $data['Version'],			
			'DocCD'					=>  $data['DocCD'],			
//			'DocLoc'				=>  $data['DocLoc'],			
//			'DocType'				=>  $data['DocType'],
//			'DocStatus'				=>  $data['DocStatus'],
//			'AdminID'				=> $data['AdminID'],
			'LastModBy'				=> $data['LastModBy'],
			'LastModDate' 			=> $DATETIME
		);

		$this->db->where('DocID', $data['DocID']);
		$result = $this->db->update('EMODocs',$values);

		if($result)	 	return true;
		else 			return false;
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_update_document_upload
 *--------------------------------------------------------------------------
 */  
	
	public function md_update_document_upload($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
//			'DocID'					=> $data['DocID'],
//			'UID'					=> $data['UID'],
			'DocLoc'				=>  $data['DocLoc'],			
			'DocType'				=>  $data['DocType'],
			'DocFileName'			=>  $data['DocFileName'],
//			'AdminID'				=> $data['AdminID'],
			'LastModBy'				=> $data['LastModBy'],
			'LastModDate' 			=> $DATETIME
		);

		$this->db->where('DocID', $data['DocID']);
		$result = $this->db->update('EMODocs',$values);

		if($result)	 	return true;
		else 			return false;
	}
	
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_create_document
 *--------------------------------------------------------------------------
 */  
	
	public function md_create_document($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
		if(!empty($data['SCID']))
			$SCID		= $data['SCID'];
		else 	
			$SCID		= 0;
		
//		print "<pre>--------------->>>> SO )";
//		print_r($data);
//		print "</pre>--------------<<<< EO <br>";
//				
		$values = array(
//			'DocID'					=> $data['DocID'],
			'CID'					=> $data['CID'],
			'SCID'					=> $data['SCID'],
//			'UID'					=> $data['UID'],
			'DocDesc'				=> $data['DocDesc'],
			'DocShortName'			=>  $data['DocShortName'],			
			'Version'				=>  $data['Version'],			
			'DocCD'					=>  $data['DocCD'],			
//			'DocLoc'				=>  $data['DocLoc'],			
//			'DocType'				=>  $data['DocType'],
//			'DocStatus'				=>  $data['DocStatus'],
//			'AdminID'				=> $data['AdminID'],
//          'LastModBy'				=> $data['LastModBy'],
//			'LastModDate' 			=> $DATETIME
		    'CreatedBy'				=> $data['CreatedBy'],
			'CreateDDate' 			=> $DATETIME
		);

		$result = $this->db->insert('EMODocs', $values);
//        print "RESULT [ " .  $result . "]<br>"; 
		$result = $this->db->insert_id();
//        print "RESULT [ " .  $result . "]<br>"; 
        
        return $result;
	
	}

	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_delete_document
 *--------------------------------------------------------------------------
 */  
	
	public function md_delete_document($id = NULL)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;

		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->db->where("DocID", $id);
			$results = $this->db->delete('EMODocs');
			return $results;
		}
	}											// EO Delete Function
		
	
//-----------------------------------------------[SO Subcat Block Model ]>>>
		
/*===========================================================================
 * 
 *  BLOCK:       	md_subcats
 *  AUTHOR:         R.Stephen Chafe (Zen River Software)
 *  CREATED:        2017_05_29			
 * 
 * ------------------------------------------------------------------------
 *
 *      This contains the block of model functions for accessing the 
 *      Subcats Table.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160228    PRSC    Created from standard Zen shell  
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_subcat_by_id($id = NULL)
	{
		$this->db->where('SCID', $id);
		$query = $this->db->get('Subcats');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}

	
	public function md_get_subcats_by_category($id = NULL)
	{
		$this->db->where('CID', $id);
		$query = $this->db->get('Subcats');   // was originally a view
		return $query->result_array();
	}
	
	
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_get_subcats
 *--------------------------------------------------------------------------
 */  
	
	public function md_get_subcats($id = null)
	{
		if(!empty($id))	$this->db->where('SCID', $id);
		$this->db->order_by("SCID", "ASC");
		$query = $this->db->get('Subcats');
		return $query->result_array();
	}

/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_update_subcats
 *--------------------------------------------------------------------------
 */  
	
	public function md_update_subcat($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
			'SubcatCD'					=> $data['SubcatCD'],
			'SubcatShortName'			=> $data['SubcatShortName'],
			'SubcatDesc'				=> $data['SubcatDesc'],
			'CID'						=> $data['CID'],
		//			'AdminID'			=> $data['AdminID'],
            'LastModBy'					=> $data['LastModBy'],
			'LastModDate' 				=> $DATETIME
		);

		$this->db->where('SCID', $data['SCID']);
		$result = $this->db->update('Subcats',$values);

		if($result)	 	return true;
		else 			return false;
	}
	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_create_subcat
 *--------------------------------------------------------------------------
 */  
	
	public function md_create_subcat($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
//		print "<pre>--------------->>>> SO )";
//		print_r($data);
//		print "</pre>--------------<<<< EO <br>";
//				
		$values = array(
			'SubcatCD'					=> $data['SubcatCD'],
			'SubcatShortName'			=> $data['SubcatShortName'],
			'SubcatDesc'				=> $data['SubcatDesc'],
			'CID'						=> $data['CID'],
//          'LastModBy'					=> $data['LastModBy'],
//			'LastModDate' 				=> $DATETIME
		    'CreatedBy'					=> $data['CreatedBy'],
			'CreateDDate' 				=> $DATETIME
		);

		$result = $this->db->insert('Subcats', $values);
//        print "RESULT [ " .  $result . "]<br>"; 
		$result = $this->db->insert_id();
//        print "RESULT [ " .  $result . "]<br>"; 
        
        return $result;
	
	}

	
/*
 * --------------------------------------------------------------------------
 *  	Function:      	md_delete_subcat
 *--------------------------------------------------------------------------
 */  
	
	public function md_delete_subcat($id = NULL)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;

		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->db->where("SCID", $id);
			$results = $this->db->delete('Subcats');
			return $results;
		}
	}											// EO Delete Function
		
		
//<<<<<<------------------------------------[EO Subcat Block Model ]	

	
	
}